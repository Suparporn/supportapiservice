﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.Main
{
    //บันทึกข้อมูลการเข้าใช้งานเมนู
    public class UserLoginSaveRequest
    {
        public string LoginPDAName { get; set; } = "";
        public string LoginBranch { get; set; } = "";
        public string LoginBranchName { get; set; } = "";
        public string LoginVersion { get; set; } = "";
        public string LoginService { get; set; } = "";
        public string LoginId { get; set; } = "";
        public string LoginName { get; set; } = "";
        public string DeviceDate { get; set; } = "";
        public string DeviceIp { get; set; } = "";
    }

    //บันทึกข้อมูลการเข้าใช้งานเครื่อง
    public class AccessMenuSaveRequest
    {
        public string LogBranch { get; set; } = "";
        public string LogBranchName { get; set; } = "";
        public string LogWhoID { get; set; } = "";
        public string LogWhoIDName { get; set; } = "";
        public double LogRow { get; set; } = 0;
        public string LogMachine { get; set; } = "";
        public string LogTmp { get; set; } = "";
    }

    public class dimensionDetail
    {
        public string NUM { get; set; } = "";
        public string DESCRIPTION { get; set; } = "";
        public string DESCRIPTION_SHOW { get; set; } = "";
    }

    //การค้นหาใน ตารางหลัก ค่า ConFig ต่างๆ โดยที่ไม่ระบุสาขา [ทุกสาขาใช้เหมือนกัน]
    public class BranchConfigDetailData
    {
        public string STA { get; set; } = "";
        public string SHOW_ID { get; set; } = "";
        public string SHOW_NAME { get; set; } = "";
        public string SHOW_DESC { get; set; } = "";
        public string REMARK { get; set; } = "";
        public string MaxImage { get; set; } = "";
    }
}