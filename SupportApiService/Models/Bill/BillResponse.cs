﻿using System;

namespace SupportApiService.Models.Bill
{

    public class BillData
    {
        public String Bill_ID { get; set; } = "";
        public String Bill_INVENTCOSTCENTERID { get; set; } = "";
        public String Bill_Date { get; set; } = "";
        public String Bill_Branch { get; set; } = "";
        public String Bill_BranchName { get; set; } = "";
        public String Bill_EmplID { get; set; } = "";
        public String Bill_EmplName { get; set; } = "";
        public String Bill_Remark { get; set; } = "";
        public String SPC_ITEMBARCODEUNIT { get; set; } = "";
        public String SPC_ITEMNAME { get; set; } = "";
        public String SPC_ITEMBARCODE { get; set; } = "";
        public double COSTAMOUNT { get; set; } = 0;
        public double SPC_QTY { get; set; } = 0;

    }

}