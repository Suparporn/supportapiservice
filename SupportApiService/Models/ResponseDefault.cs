﻿
namespace SupportApiService.Models
{
    public class ResponseDefault
    {
        public int CODE { get; set; } = 200;
        public string MESSAGE { get; set; } = "";
    }
}