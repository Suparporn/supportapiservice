﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.CNReciveBox
{
    
    //รายละเอียดของ tag ที่ส่งมา
    public class CNReciveBoxTagDetailDATA
    {
        public String TagNumber { get; set; } = "";
        public String TagBranch { get; set; } = "";
        public String TagMNPC { get; set; } = "";
        public String TagShipSta { get; set; } = "";
        public String TagReciveID { get; set; } = "";
        public String TagReciveSta { get; set; } = "";
        public String BRANCH_NAME { get; set; } = "";
        public String ShipID { get; set; } = "";
        public String ShipCarID { get; set; } = "";
        public String ShipDriverID { get; set; } = "";
        public String ShipStaDoc { get; set; } = "";
        public String SPC_NAME { get; set; } = "";
        public String StaImg { get; set; } = "";
        public String DOCDATE { get; set; } = "";
    }
  
    //รายละเอียดของที่ส่ง มาตามเอกสาร ShipDoc
    public class CNReciveBoxTagSendDATA
    {
        public String TagShipID { get; set; } = "";
        public String TagNumber { get; set; } = "";
        public String TagMNPC { get; set; } = "";
        public String TagBranch { get; set; } = "";
        public String BRANCH_NAME { get; set; } = "";
    }
    //เชคจำนวนลังที่ส่งและรับ
    public class CNReciveBoxCountDATA
    {
        public String ShipID { get; set; } = "";
        public String ShipCarID { get; set; } = "";
        public String ShipBranch { get; set; } = "";
        public String ShipBranchName { get; set; } = "";
        public String ShipDriverID { get; set; } = "";
        public String TagSend { get; set; } = "";
        public String TagRecive { get; set; } = "";
        public String TagDiff { get; set; } = "";
    }

  
}