﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.CNReciveBox
{
    //HD+DT
    public class ReciveSaveRequest
    {
        public List<ReciveSaveDTRequest> DT = new List<ReciveSaveDTRequest> ();
        public List<TagNumberRequest> dtTag = new List<TagNumberRequest>();
    }
    //รายละเอียดการ
    public class ReciveSaveDTRequest
    {        
        public String ShipReciveShipID { get; set; } = "";
        public double ShipCountBoxAll { get; set; } = 0;
        public double ShipCountBoxRecive { get; set; } = 0;
        public double ShipCountBoxDiff { get; set; } = 0;
    }
    public class TagNumberRequest
    {
        public String TagNumber { get; set; } = "";
    }
}