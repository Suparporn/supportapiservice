﻿namespace SupportApiService.Models.General
{
    public class GeneralClass  
    {
        //ค้นหารายละเอียดสินค้า ตามบาร์โค้ด
        public static string Barcode_Detail(string barcode)
        {
            return $@"[ItembarcodeClass_GetItembarcode_ALLDeatil] '{barcode}'";
        }

        //ค้นหารายละเอียดสินค้า ตามบาร์โค้ด พร้อม ระดับ ราคา
        public static string Barcode_Detail(string barcode, string price)
        {
            return $@"[ItembarcodeClass_GetItembarcode_ALLDeatil_ByPrice] '{barcode}','{price}'";
        }

        //ค้นหาบาร์โค้ดทั้งหมด ตามชุดมิติสินค้า
        public static string Barcode_AllByItemIdDim(string productId, string productDim)
        {
            return $@"[ItembarcodeClass_FindItembarcodeAll_ByItemidDim] '{productId}','{productDim}'";
        }

        //ค้นหาบาร์โค้ดทั้งหมด ตามชุดมิติสินค้า
        public static string BarcodeAllDim_ByPriceLevel(string productId, string productDim,string priceLavel)
        {
            return $@"[ItembarcodeClass_FindItembarcodeAll_ByItemidDim_ByPrice] '{productId}','{productDim}',{priceLavel}";
        }

        //รายละเอียดพนักงานทั้งหมด โดยที่เช็คข้อมูลมใน SHOP_EMPLOYEE ด้วย
        public static string Employee_GetDetailShopByAltnum(string altnum)
        {
            return $@"[EmplClass_GetEmployee] '{altnum}' ";
        }

    }
}