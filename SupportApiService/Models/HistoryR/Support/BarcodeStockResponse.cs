﻿using ApiService.Models.TableModel.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.HistoryR.Support
{
    public class BarcodeStockResponse : DefaultResponse
    {
        public IEnumerable<BarcodeStockReturn> Data { get; set; }
    }
}