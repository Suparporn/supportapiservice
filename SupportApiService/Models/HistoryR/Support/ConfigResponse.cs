﻿using ApiService.Models.TableModel.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.HistoryR.Support
{
    public class ConfigResponse : DefaultResponse
    {
        public IEnumerable<ConfigReturn> Data { get; set; }
    }
}