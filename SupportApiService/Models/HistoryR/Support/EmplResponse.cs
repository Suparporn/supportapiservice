﻿using ApiService.Models.TableModel.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.HistoryR.Support
{
    public class EmplResponse : DefaultResponse
    {
        public IEnumerable<EmplReturn> Data { get; set; }
    }
}