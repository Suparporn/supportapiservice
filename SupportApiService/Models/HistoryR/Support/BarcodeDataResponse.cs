﻿using ApiService.Models.TableModel.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.HistoryR.Support
{
    public class BarcodeDataResponse : DefaultResponse
    {
        public IEnumerable<BarcodeDataReturn> Data { get; set; }
    }
}