﻿using ApiService.Models.TableModel.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.HistoryR.Support
{
    public class MntfMaxIdDocnoResponse : DefaultResponse
    {
        public IEnumerable<MntfMaxIdDocnoReturn> Data { get; set; }
    }
}