﻿
namespace SupportApiService.Models.MNPC
{
    //รายละเอียดของบาร์โค้ดที่จะคืน
    public class MNPCBarcodeDetailData
    {
        public string ITEMBARCODE { get; set; } = "";
        public string ITEMID { get; set; } = "";
        public string INVENTDIMID { get; set; } = "";
        public string SPC_ITEMNAME { get; set; } = "";
        public double QTY { get; set; } = 1;
        public string UNITID { get; set; } = "";
        public string DIMENSION { get; set; } = "";
        public string DESCRIPTION { get; set; } = "";
        public string SPC_SalesPriceType { get; set; } = "";
        public string SPC_PRICEGROUP3 { get; set; } = "";
        public double PRICEMN { get; set; } = 0;
    }

    //ค้นหา MNPC ที่ไม่ได้อนุมัติ
    public class MNPCHDDetailData
    {
        public string BRANCHID { get; set; } = "";
        public string BRANCHNAME { get; set; } = "";
        public string DocNo { get; set; } = "";
        public string DOCDATE { get; set; } = "";
        public string DocTime { get; set; } = "";
        public string TYPEPRODUCT { get; set; } = "";
        public string TYPEPRODUCTNAME { get; set; } = "";
        public string Remark { get; set; } = "";
    }

    //ค้นหา MNPC ที่ไม่ได้อนุมัติ
    public class MNPCDTDetailData
    {
        public string Branch { get; set; } = "";
        public string BRANCH_NAME { get; set; } = "";
        public string DocNo { get; set; } = "";
        public string DOCDATE { get; set; } = "";
        public string DocTime { get; set; } = "";
        public string TYPEPRODUCT { get; set; } = "";
        public string TYPEPRODUCTNAME { get; set; } = "";
        public string Remark { get; set; } = "";
        public string StaItem { get; set; } = "";
        public string StaDoc { get; set; } = "";
        public string StaPrcDoc { get; set; } = "";
        public string ItemID { get; set; } = "";
        public string Dimid { get; set; } = "";
        public string Barcode { get; set; } = "";
        public string ItemName { get; set; } = "";
        public string Qty { get; set; } = "";
        public string UnitID { get; set; } = "";
        public string StatusShop { get; set; } = "";
        public string Price { get; set; } = "";
        public string DeptCode { get; set; } = "";
        public string DESCRIPTION { get; set; } = "";
        public string CNReason { get; set; } = "";
        public string SPC_SALESPRICETYPE { get; set; } = "";
        public string REASONNAME { get; set; } = "";
        public string STAPurchase { get; set; } = "";

    }

    //ค้นหา MNPC ที่ไม่ได้อนุมัติ
    public class MNPCTagData
    {
        public string TagNumber { get; set; } = "";
    }


}