﻿
namespace SupportApiService.Models.MNPC
{
    //HD+DT
    public class MNPCSaveRequest
    {
        public MNPCHDSaveRequest HD { get; set; } = new MNPCHDSaveRequest();
        public MNPCDTSaveRequest DT { get; set; } = new MNPCDTSaveRequest();
    }

    //HD
    public class MNPCHDSaveRequest
    {
        public string UserCode { get; set; } = "";
        public string Branch { get; set; } = "";
        public string BranchName { get; set; } = "";
        public string Grand { get; set; } = "";
        public string Net { get; set; } = "";
        public string TYPEPRODUCT { get; set; } = "";
        public string SPCName { get; set; } = "";
    }

    //DT
    public class MNPCDTSaveRequest
    {
        public int SeqNo { get; set; } = 1;
        public string ItemID { get; set; } = "";
        public string Dimid { get; set; } = "";
        public string Barcode { get; set; } = "";
        public string ItemName { get; set; } = "";
        public string DeptCode { get; set; } = "";
        public string Qty { get; set; } = "";
        public string UnitID { get; set; } = "";
        public string Factor { get; set; } = "";
        public string QtySum { get; set; } = "";
        public string Price { get; set; } = "";
        public string PriceNet { get; set; } = "";
        //public string StatusShop { get; set; } = "";
        public string SPC_SALESPRICETYPE { get; set; } = "";
        public string CNReason { get; set; } = "";


    }

    //TagNumber
    public class MNPCTagRequest
    {
        public string TagNumber { get; set; } = "";
        public string Branch { get; set; } = "";
    }

    
}