﻿using System;

namespace SupportApiService.Models.Invent
{
    //ข้อมูลคลังสินค้า
    public class InventData
    {
        public String INVENTLOCATIONID { get; set; } = "";
        public String INTNAME { get; set; } = "";
    }

    //ข้อมูลเงื่อนไขการจัดส่ง
    public class InventDLVTERMData
    {
        public String CODE { get; set; } = "";
        public String CODE_TXT { get; set; } = "";
    }

    //ข้อมูลแผนก
    public class DptData
    {
        public String Num { get; set; } = "";
        public String Description { get; set; } = "";
    }

    //ข็อมูลคลังสินค้า ของมินิมาร์ทพร้อมสายรถ
    public class BranchRouteData
    {
        public String BRANCH_ID { get; set; } = "";
        public String BRANCH_NAME { get; set; } = "";
        public String ROUTEID { get; set; } = "";
        public String ROUTENAME { get; set; } = "";
      
    }

    public class BranchData
    {
        public String BRANCH_ID { get; set; } = "";
        public String BRANCH_NAME { get; set; } = "";

    }
}