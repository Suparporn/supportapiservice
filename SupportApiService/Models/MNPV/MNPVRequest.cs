﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.MNPV
{
    //ข้อมูล HD+DT
    public class MNPVSaveRequest
    {
        public MNPVSaveHDRequest HD { get; set; } = new MNPVSaveHDRequest();
        public MNPVDTSaveRequest DT { get; set; } = new MNPVDTSaveRequest();
    }
    //ข้อมูล DT
    public class MNPVSaveHDRequest
    {
        public String WhoIDIns { get; set; } = "";
        public String WhoNameIns { get; set; } = "";
        public String VenderID { get; set; } = ""; 
        public String VenderName { get; set; } = ""; 
        public String NUM { get; set; } = ""; 
        public String DESCRIPTION { get; set; } = ""; 
        public String Remark { get; set; } = ""; 
        public String Machine { get; set; } = "";
        public String DptID { get; set; } = "";
    }

    //ข้อมูล DT
    public class MNPVDTSaveRequest
    {
        public String LineNum { get; set; } = "";
        public String ITEMID { get; set; } = "";
        public String INVENTDIMID { get; set; } = "";
        public String ITEMBARCODE { get; set; } = "";
        public String SPC_ITEMNAME { get; set; } = "";
        public double QTY { get; set; } = 0;
        public String UNITID { get; set; } = "";
        public double PRICE { get; set; } = 0;
        public double FACTOR { get; set; } = 0;
        public string SPC_SALESPRICETYPE { get; set; } = "";
        public string ResonID { get; set; } = "";
        public string ResonName { get; set; } = ""; 

    }
}