﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.MNPV
{
    //บิล HD 
    public class MNPVVenderData
    {
        public String ACCOUNTNUM { get; set; } = "";
        public String NAME { get; set; } = "";
        public String NUM { get; set; } = "";
        public String DESCRIPTION { get; set; } = "";
    }
    //บิล HD 
    public class MNPVNotApvData
    { 
        public String DocNo { get; set; } = "";
        public String Date { get; set; } = "";
        public String TimeIns { get; set; } = "";
        public String VenderID { get; set; } = "";
        public String VenderName { get; set; } = "";
        public String WhoIDIns { get; set; } = "";
        public String WhoNameIns { get; set; } = "";     

    }
    //รายละเอียดบิล
    public class MNPVDetailData
    {
        public String DOCNO { get; set; } = "";
        public String VenderID { get; set; } = "";
        public String VenderName { get; set; } = "";
        public String WhoIDIns { get; set; } = "";
        public String WhoNameIns { get; set; } = "";
        public String StaDoc { get; set; } = "";
        public String StaPrcDoc { get; set; } = "";
        public String Remark { get; set; } = "";
        public String ITEMBARCODE { get; set; } = "";
        public String SPC_ITEMNAME { get; set; } = "";
        public double QTY { get; set; } = 0;
        public double PRICE { get; set; } = 0;
        public String UNITID { get; set; } = "1";
        public String SPC_SALESPRICETYPE { get; set; } = "";
        public String ResonID { get; set; } = "";
        public String ResonName { get; set; } = "";

    }
}