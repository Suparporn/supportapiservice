﻿using System;

namespace SupportApiService.Models.MNTF
{
    //ข้อมูล HD+DT
    public class MNTFSaveRequest
    {
        public MNTFSaveHDRequest HD { get; set; } = new MNTFSaveHDRequest();
        public MNTFDTSaveRequest DT { get; set; } = new MNTFDTSaveRequest();
    }
    //ข้อมูล DT
    public class MNTFSaveHDRequest
    {
        public String MNTFUserCode { get; set; } = "";
        public String MNTFUserCodeName { get; set; } = "";
        public String MNTFTypeOrder { get; set; } = ""; //ดูแลสินค้า 1 สำหรับ จัดซื้อยา เบิกสินค้ามาหน้าร้าน = 7,คืนสินค้าโกดัง = 8 ,ส่งสินค้ามาหน้าร้าน = 9
        public String MNTFWHSoure { get; set; } = ""; //สำหรับ จัดซื้อยา เบิกสินค้ามาหน้าร้าน/ส่งสินค้ามาหน้าร้าน = WH-A,คืนสินค้าโกดัง = RETAILAREA
        public String MNTFWHDestination { get; set; } = "";//สำหรับ จัดซื้อยา เบิกสินค้ามาหน้าร้าน/ส่งสินค้ามาหน้าร้าน =  RETAILAREA,คืนสินค้าโกดัง =WH-A
        public String MNTFLock { get; set; } = "";//สำหรับ จัดซื้อยา ส่ง DRUG
        public String MNTFRemark { get; set; } = "";//สำหรับ จัดซื้อยา ส่งสินค้ามาหน้าร้าน ต้องส่งเลขที่ MNTF อ้างอิงมา
        public String MNTFSPCNAME { get; set; } = "";
        public String MNTFDepart { get; set; } = "";
        public String MNTFDepartName { get; set; } = "";
    }

    //ข้อมูล DT
    public class MNTFDTSaveRequest
    {
        public String MNTFSeqNo { get; set; } = "";
        public String MNTFItemID { get; set; } = "";
        public String MNTFDimid { get; set; } = "";
        public String MNTFBarcode { get; set; } = "";
        public String MNTFName { get; set; } = "";
        public double MNTFQtyOrder { get; set; } = 0;
        public String MNTFUnitID { get; set; } = "";
        public double MNTFPrice { get; set; } = 0;
        public double MNTFFactor { get; set; } = 0;
        public string MNTFPurchase { get; set; } = "";
        public string MNTFRemark { get; set; } = "";//สำหรับ จัดซื้อยา ส่งสินค้ามาหน้าร้าน ต้องส่งเลขที่ MNTF_LineNum อ้างอิงมา ,เบิกสินค้ามาหน้าร้าน/คืนสินค้าโกดัง ส่ง MNTF_linenum (MNTF ตัวเอง)
        public String MNTFPATHIMAGE { get; set; } = "";

    }
}