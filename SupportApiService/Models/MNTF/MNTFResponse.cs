﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.MNTF
{

    //บิล HD 
    public class MNTFNotApvData
    {
        public String MNTFDocNo { get; set; } = "";
        public String MNTFDate { get; set; } = "";
        public String MNTFTime { get; set; } = "";
        public String MNTFUserCode { get; set; } = "";
        public String MNTFWHSoure { get; set; } = "";
        public String MNTFWHDestination { get; set; } = "";
        public String MNTFLock { get; set; } = "";
        public String MNTFStaApvDoc { get; set; } = "";

    }
    //รายละเอียดบิล
    public class MNTFDetailData
    {
        public String MNTFDocNo { get; set; } = "";
        public String MNTFDate { get; set; } = "";
        public String MNTFTime { get; set; } = "";
        public String MNTFUserCode { get; set; } = "";
        public String MNTFWHSoure { get; set; } = "";
        public String MNTFWHDestination { get; set; } = "";
        public String MNTFLock { get; set; } = "";
        public String MNTFBarcode { get; set; } = "";
        public String MNTFName { get; set; } = "";
        public double MNTFQtyOrder { get; set; } = 0;
        public String MNTFUnitID { get; set; } = "";
        public String TXT { get; set; } = "1";
        public String NAMESoure { get; set; } = "";
        public String NAMEDes { get; set; } = "";
        public String MNTFStaApvDoc { get; set; } = "";

    }
    //Detail bill สำหรับการอ้างอิง การรับสินค้าและการส่งสินค้าจากโกดังมาหน้าร้าน
    public class MNTFRefTransIDData
    {
        public string MNTFWHDestination { get; set; } = "";
        public string MNTFDepart { get; set; } = "";
        public string MNTFDocNo { get; set; } = "";
        public string MNTFWHSoure { get; set; } = "";
    }
  
    //Detail ของรายการที่อ้างอิงการทำบิลเบิก
    public class MNTFRefData
    {
        public string INVENTTRANSID { get; set; } = "";
        public string SPC_Qty { get; set; } = "";
       
    }
}