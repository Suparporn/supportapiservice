﻿using System;

namespace SupportApiService.Models.MNTO
{
    //บิล HD 
    public class MNTONotApvData
    {
        public String MNTODocNo { get; set; } = "";
        public String MNTODate { get; set; } = "";
        public String MNTOTime { get; set; } = "";
        public String MNTOInventOUT { get; set; } = "";
        public String MNTOInventIN { get; set; } = "";
        public String MNTOInventINName { get; set; } = "";
        public String MNTOWhoIn { get; set; } = "";
    }

    //รายละเอียดบิล
    public class MNTODetailData
    {
        public String MNTOInvent { get; set; } = "";
        public String ITEMID { get; set; } = "";
        public String INVENTDIM { get; set; } = "";
        public String ITEMBARCODE { get; set; } = "";
        public String SPC_ITEMNAME { get; set; } = "";
        public double QTY { get; set; } = 1;
        public String UNITID { get; set; } = "";
        public String WHOINS { get; set; } = "";
        public String MNTOStaPrcDoc { get; set; } = "";
        public String MNTOInventIN { get; set; } = "";
        public String MNTOInventINName { get; set; } = "";
        public String MNTOWhoDpt { get; set; } = "1";
        public String MNTOStaDoc { get; set; } = "";
        public String MNTOStaDt { get; set; } = "";
        public String INVENTSUB { get; set; } = "";
        
    }

}