﻿using System;

namespace SupportApiService.Models.MNTO
{
    //ข้อมูล HD+DT
    public class MNTOSaveRequest
    {
        public MNTOSaveHDRequest HD { get; set; } = new MNTOSaveHDRequest();
        public MNTOSaveDTRequest DT { get; set; } = new MNTOSaveDTRequest();
    }
    //ข้อมูล HD
    public class MNTOSaveHDRequest
    {
        public String MNTOUserCode { get; set; } = "";
        public String MNTOInventIN { get; set; } = "";
        public String MNTOInventOUT { get; set; } = "";
        public String MNTOSPC { get; set; } = "";
        public String MNTOWhoIn { get; set; } = "";
        public String MNTOInventINName { get; set; } = "";
        public String MNTOWhoDpt { get; set; } = "";
    }
    //ข้อมูล DT
    public class MNTOSaveDTRequest
    {
        public int MNTOSeqNo { get; set; } = 1;
        public String MNTOItemID { get; set; } = "";
        public String MNTODimid { get; set; } = "";
        public String MNTOBarcode { get; set; } = "";
        public String MNTOName { get; set; } = "";
        public String MNTOInvent { get; set; } = "";
        public double MNTOQtyOrder { get; set; } = 1;
        public String MNTOUnitID { get; set; } = "";
        public double MNTOFactor { get; set; } = 1;
        public String MNTOUser { get; set; } = "";
        public String INVENTSUB { get; set; } = "";

    }
}