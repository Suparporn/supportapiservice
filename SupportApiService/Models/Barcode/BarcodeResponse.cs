﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.Barcode
{
    //ข้อมูลสินค้า ตามราคาที่ Set ไว้
    public class BarcodeDetailData
    {
        public string ITEMID { get; set; } = "";
        public string INVENTDIMID { get; set; } = "";
        public string ITEMBARCODE { get; set; } = "";
        public string SPC_ITEMNAME { get; set; } = "";
        public string SPC_ITEMACTIVE { get; set; } = "";
        public string DIMENSION { get; set; } = "";
        public string NUM { get; set; } = "";
        public string DESCRIPTION { get; set; } = "";
        public string PRICE { get; set; } = "";// ราคาตามแผนกที่ set ไว้
        public string SPC_PRICEGROUP3 { get; set; } = ""; //ราคาขายสาขาใหญ่
        public string UNITID { get; set; } = "";
        public string QTY { get; set; } = "";
        public string SPC_SalesPriceType { get; set; } = "";
        public string SPC_PickingLocationId { get; set; } = "";
        public string ACCOUNTNUM { get; set; } = "";
        public string NAME { get; set; } = "";
        public string INVENTSUB { get; set; } = "";
        public string PRICEMN { get; set; } = "";
        public string SPC_IMAGEPATH { get; set; } = "";
    }
    //ค้นหาบาร์โค้ดทั้งหมด ตามชุดมิติสินค้า
    public class BarcodeAllDimData
    {
        public string ITEMBARCODE { get; set; } = "";
        public string ITEMID { get; set; } = "";
        public string INVENTDIMID { get; set; } = "";
        public string QTY { get; set; }
        public string UNITID { get; set; } = "";
        public string SPC_ITEMNAME { get; set; } = "";
        public string SPC_ITEMACTIVE { get; set; } = "";
        public double PRICE { get; set; } = 0;
        public double SPC_PRICEGROUP1 { get; set; } = 0;
        public double SPC_PRICEGROUP3 { get; set; } = 0;
        public double SPC_PRICEGROUP4 { get; set; } = 0;
        public double SPC_PRICEGROUP13 { get; set; } = 0;
        public double SPC_PRICEGROUP14 { get; set; } = 0;
        public double SPC_PRICEGROUP15 { get; set; } = 0;
        public double SPC_PRICEGROUP17 { get; set; } = 0;
        public double SPC_PRICEGROUP21 { get; set; } = 0;
        public string SPC_IMAGEPATH { get; set; } = "";
        public string SPC_BurmaName { get; set; } = "";
        public string DESCRIPTION { get; set; } = "";
        public string SPC_PickingLocationId { get; set; } = "";
        public string INVENTSUB { get; set; } = "";

        public double CAPACITY { get; set; } = 0;
        public double SPC_BarCodeWeight { get; set; } = 0;
        public double SPC_RFIDItemTagging { get; set; } = 0;
    }
    //บาร์โค้ด จากชื่อสินค้า
    public class BarcodeName
    {
        public string ITEMBARCODE { get; set; } = "";
        public string SPC_ITEMNAME { get; set; } = "";
    }
}