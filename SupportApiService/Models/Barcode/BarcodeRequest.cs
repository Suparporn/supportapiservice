﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.Barcode
{
    //บันทึกการเปลี่ยนแปลงป้ายราคา
    public class BarcodeInsertPriceChangeRequest
    {
        public string CheckWho { get; set; } = "";
        public string CheckWhoName { get; set; } = "";
        public string ITEMBARCODE { get; set; } = "";
        public string SPC_ITEMNAME { get; set; } = "";
        public double CheckPrice { get; set; } = 0;
        public string UNITID { get; set; } = "";
        public string DptID { get; set; } = "";
        public string DptName { get; set; } = "";
        public string CheckPDA { get; set; } = "";
        public string CheckDepart { get; set; } = "";
        public string CheckDepartName { get; set; } = "";
    }
    //แผนกและผู้บันทึกพิมพ์
    public class BarcodePrintDateSaveHeadRequest
    {
        public string EmpID { get; set; } = "";
        public string InventID { get; set; } = "";

        public List<BarcodePrintDateInsertRequest> DATA = new List<BarcodePrintDateInsertRequest>();
    }
    //บันทึกบาร์โค้ดที่พิมพ์ไป เพื่อเช็ครายการพิมพ์ล่าสุดของแต่ละสินค้า
    public class BarcodePrintDateInsertRequest
    {
        public string ITEMBARCODE { get; set; } = "";
        public string ITEMNAME { get; set; } = "";
        public double ItemPrice { get; set; } = 0;
        public string ItemUnit { get; set; } = "";
 
    }
}