﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class PassParameter
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}