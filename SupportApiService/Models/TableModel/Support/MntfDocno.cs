﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class MntfDocno
    {
        public string pType { get; set; } = "";
        public string pPos { get; set; } = "";
        public string pFirst { get; set; } = "";
        public string pCase { get; set; } = "";
    }

    public class MntfMaxIdDocnoReturn
    {
        public string Id { get; set; } = "";
    }
}