﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class Config
    {
        public string Devicename { get; set; } = "";
    }

    public class ConfigReturn
    {
        public string Comname { get; set; } = "";
        public string Deptid { get; set; } = "";
        public string Deptname { get; set; } = "";
        public string Showid { get; set; } = "";
        public string Versionupd { get; set; } = "";
        public string Warehouse { get; set; } = "";
        public string Pricegroup { get; set; } = "";
        public string Printer { get; set; } = "";
        public string log { get; set; } = "";
        public string modifiedUser { get; set; } = "";
    }
}