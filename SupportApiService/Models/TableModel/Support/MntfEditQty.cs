﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class MntfEditQty
    {
        public string pDocno { get; set; } = "";
        public string pSeqNo { get; set; } = "";
        public string pQty { get; set; } = "";
        public string pUser { get; set; } = "";
        public string pUsername { get; set; } = "";
    }
}