﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class ChangeSign
    {
        public String Checkwho { get; set; } = "";
        public String Checkwhoname { get; set; } = "";
        public String Itembarcode { get; set; } = "";
        public String Itemname { get; set; } = "";
        public String Deptid { get; set; } = "";
        public String Deptname { get; set; } = "";
        public String Checkprice { get; set; } = "";
        public String Unitid { get; set; } = "";
        public String CheckPDA { get; set; } = "";
        public String Checkdepart { get; set; } = "";
        public String Checkdepartname { get; set; } = "";
    }
}