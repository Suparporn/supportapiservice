﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class MntfData
    {
        public string Mntfdocno { get; set; } = "";
        public Datasavehd Datasavehd { get; set; }
        public Datasavedt Datasavedt { get; set; }
        public string Type { get; set; } = "";
        public string Pdaname { get; set; } = "";
        public string pDpt { get; set; } = "";
        public string pDptname { get; set; } = "";
        public int Tag { get; set; }
    }

    public class Datasavehd
    {
        public string MNTFUsercode { get; set; } = "";
        public string MNTFUsercodename { get; set; } = "";
        public string MNTFWHSoure { get; set; } = "";
        public string MNTFWHDestination { get; set; } = "";
        public string MNTFLock { get; set; } = "";
        public string Transferid { get; set; } = "";
    }

    public class Datasavedt
    {
        public string MNTFSeqno { get; set; } = "";
        public string MNTFItemid { get; set; } = "";
        public string MNTFDimid { get; set; } = "";
        public string MNTFBarcode { get; set; } = "";
        public string MNTFName { get; set; } = "";
        public string MNTFQtyorder { get; set; } = "";
        public string MNTFUnitid { get; set; } = "";
        public string MNTFPrice { get; set; } = "";
        public string MNTFFactor { get; set; } = "";
        public string MNTFWhoin { get; set; } = "";
    }
}