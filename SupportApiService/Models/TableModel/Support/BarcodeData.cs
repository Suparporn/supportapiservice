﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class BarcodeData
    {
        public string Itembarcode { get; set; } = "";
        public string Pricelevel { get; set; } = "";
    }

    public class BarcodeDataReturn
    {
        public string Itembarcode { get; set; } = "";
        public string Itemid { get; set; } = "";
        public string Inventdimid { get; set; } = "";
        public string Qty { get; set; } = "";
        public string Unitid { get; set; } = "";
        public string Itemname { get; set; } = "";
        public int Itemactive { get; set; } = 0;
        public string Pricegroup3 { get; set; } = "";
        public string Pricegroup4 { get; set; } = "";
        public string Pricegroup13 { get; set; } = "";
        public string Pricegroup14 { get; set; } = "";
        public string Pricegroup15 { get; set; } = "";
        public string Imagepath { get; set; } = "";
        public string Primaryvendorid { get; set; } = "";
        public string Primaryvendoridname { get; set; } = "";
        public string Dimension { get; set; } = "";
        public string Description { get; set; } = "";
        public int Salespricetype { get; set; } = 0;
        public string Grpid0 { get; set; } = "";
        public string Tax0 { get; set; } = "";
        public string Grpid1 { get; set; } = "";
        public string Tax1 { get; set; } = "";
        public string Grpid2 { get; set; } = "";
        public string Tax2 { get; set; } = "";
        public int Giftpoint { get; set; } = 0;
        public string Burmaname { get; set; } = "";
        public string Salespricetypename { get; set; } = "";
        public string Pickinglocationid { get; set; } = "";
        public string Price { get; set; } = "";
        public string Nameinvent { get; set; } = "";
        public string Num { get; set; } = "";
        public string Pricemn { get; set; } = "";
        public string Accountnum { get; set; } = "";
        public string Name { get; set; } = "";
    }
}