﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class ApvBill
    {
        public String pDocno { get; set; } = "";
        public String pUser { get; set; } = "";
        public String pUsername { get; set; } = "";
        public String pType { get; set; } = "";
        public String pInvent { get; set; } = "";
    }
}