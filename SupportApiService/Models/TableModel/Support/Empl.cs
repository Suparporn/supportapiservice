﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class Empl
    {
    }

    public class EmplLogin
    {
        public String Emplid { get; set; } = "";
        public String Emplpass { get; set; } = "";
    }

    public class EmplReturn
    {
        public String Emplid { get; set; } = "";
        public String Emplpass { get; set; } = "";
        public String Name { get; set; } = "";
        public String Deptid { get; set; } = "";
        public String Deptname { get; set; } = "";
    }
}