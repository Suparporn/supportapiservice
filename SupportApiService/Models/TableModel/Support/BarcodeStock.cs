﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class BarcodeStock
    {
        public string Itembarcode { get; set; } = "";
        public string Warehouse { get; set; } = "";
    }

    public class BarcodeStockReturn
    {
        public string Stock { get; set; } = "0";
    }
}