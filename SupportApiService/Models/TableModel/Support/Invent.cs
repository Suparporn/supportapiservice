﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class Invent
    {
        public string pCase { get; set; } = "";
        public string pInventid { get; set; } = "";
    }

    public class InventReturn
    {
        public string Inventlocationid { get; set; } = "";
        public string Inventname { get; set; } = "";
        public string Code { get; set; } = "";
        public string Codetxt { get; set; } = "";
    }
}