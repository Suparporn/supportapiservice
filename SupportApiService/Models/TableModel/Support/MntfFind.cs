﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
	public class MntfFind
	{
		public string pCase { get; set; } = "";
		public string pType { get; set; } = "";
		public string pDptid { get; set; } = "";
		public string pInventid { get; set; } = "";
		public string pDocno { get; set; } = "";
		public string pBarcode { get; set; } = "";
	}

	public class MntfFindReturn
	{
		public string MntfDocno { get; set; } = "";
		public string MntfDate { get; set; } = "";
		public string MntfTime { get; set; } = "";
		public string MntfUserid { get; set; } = "";
		public string MntfWhSource { get; set; } = "";
		public string MntfWhDes { get; set; } = "";
		public string MntfLock { get; set; } = "";
		public string MntfBarcode { get; set; } = "";
		public string MntfName { get; set; } = "";
		public double MntfQtyOrder { get; set; } = 0;
		public string MntfUnitid { get; set; } = "";
		public string Txt { get; set; } = "";
		public string NameSource { get; set; } = "";
		public string NameDes { get; set; } = "";
		public string MntfStatusApv { get; set; } = "";
		public int LineNum { get; set; } = 0;
		public string MntfItemid { get; set; } = "";
		public string MntfDimid { get; set; } = "";
		public double MntfFactor { get; set; } = 0;
		public double MntfPrice { get; set; } = 0;
		public string MntfSpcName { get; set; } = "";
	}
}