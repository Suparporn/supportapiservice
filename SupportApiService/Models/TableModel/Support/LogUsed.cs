﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class LogUsed
    {
        public String LogBranch { get; set; } = "";
        public String LogBranchName { get; set; } = "";
        public String LogType { get; set; } = "";
        public String LogWhoID { get; set; } = "";
        public String LogWhoIDName { get; set; } = "";
        public String LogRow { get; set; } = "";
        public String LogMachine { get; set; } = "";
        public String LogTmp { get; set; } = "";
    }
}