﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models.TableModel.Support
{
    public class AllBarcodeData
    {
        public string Itemid { get; set; } = "";
        public string Inventdimid { get; set; } = "";
        public string Pricelevel { get; set; } = "";
    }

    public class AllBarcodeDataReturn
    {
        public string Itembarcode { get; set; } = "";
        public string Itemid { get; set; } = "";
        public string Inventdimid { get; set; } = "";
        public string Qty { get; set; } = "";
        public string Unitid { get; set; } = "";
        public string Itemname { get; set; } = "";
        public int Itemactive { get; set; } = 0;
        public string Pricegroup3 { get; set; } = "";
        public string Pricegroup13 { get; set; } = "";
        public string Pricegroup14 { get; set; } = "";
        public string Pricegroup15 { get; set; } = "";
        public string Imagepath { get; set; } = "";
        public string Burmaname { get; set; } = "";
        public string Ok { get; set; } = "";
        public string Description { get; set; } = "";
        public string Pickinglocationid { get; set; } = "";
        public int Rfiditemtagging { get; set; } = 0;
        public string Barcodeweight { get; set; } = "";
        public string Capacity { get; set; } = "";
        public string Pricemn { get; set; } = "";
        public string Price { get; set; } = "";
    }
}