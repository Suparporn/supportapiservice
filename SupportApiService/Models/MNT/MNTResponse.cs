﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.MNT
{
    //ประเภทลัง ส่งมินิมาร์ท
    public class MNTBoxGroupData
    {
        public String CODE { get; set; } = "";
        public String TXT { get; set; } = "";
        public String CAPACITY { get; set; } = "";
        public String BOXTYPE { get; set; } = "";
        public String WEIGHT { get; set; } = "";
    }
    //ค้นหาลังในบิล
    public class MNTPOSTOBOXFACESHEETData
    {
        public String REFBOXTRANS { get; set; } = "";
        public String BOXGROUP { get; set; } = "";
        public String QTY { get; set; } = "";
        public String BOXTYPE { get; set; } = "";
        public String BOXNAME { get; set; } = "";
    }
    //ค้นหารายการ
    public class MNTPOSTOLINEData
    {
        public String ROUTESHIPMENTID { get; set; } = "";
        public String RouteName { get; set; } = "";
        public String DLVTERM { get; set; } = "";
        public String TXT { get; set; } = "";
        public String INVENTLOCATIONIDTO { get; set; } = "";
        public String Branch_NAME { get; set; } = "";
        public String INVENTLOCATIONIDFROM { get; set; } = "";
        public String SHIPDATE { get; set; } = "";
        public String STAAX { get; set; } = "";
        public String STADOC { get; set; } = "";
        public int LINENUM { get; set; } = 0;
        public double QTY { get; set; } = 1;
        public String SPC_ITEMNAME { get; set; } = "";
        public String SALESUNIT { get; set; } = "";
        public String ITEMBARCODE { get; set; } = "";
        public String REFBOXTRANS { get; set; } = "";
    }

    //ระดับราคา
    public class MNTPRICEData
    {
        public String ColumnName { get; set; } = "";
        public String Label { get; set; } = "";
        public String LabelDesc { get; set; } = "";
    }

}