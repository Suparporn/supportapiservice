﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportApiService.Models.MNT
{

    //ข้อมูล HD+DT+Trans
    public class MNTSaveRequest
    {
        public MNTSaveHDRequest HD { get; set; } = new MNTSaveHDRequest();
        public MNTSaveDTRequest DT { get; set; } = new MNTSaveDTRequest();
        public MNTSaveTransRequest Trans { get; set; } = new MNTSaveTransRequest();
    }
    
    //ข้อมูล HD
    public class MNTSaveHDRequest
    {
        public String CASHIERID { get; set; } = "";
        public String PICKINGROUTEID { get; set; } = "";
        public String ROUTESHIPMENTID { get; set; } = "";
        public String INVENTLOCATIONIDTO { get; set; } = "";
        public String INVENTLOCATIONIDFROM { get; set; } = "";
        public String VOUCHERID { get; set; } = "";
        public String STADPT { get; set; } = "";
    }

    //ข้อมูล HD
    public class MNTSaveDTRequest
    {
        public int LINENUM { get; set; } = 1;
        public double QTY { get; set; } = 1;
        public double SALESPRICE { get; set; } = 1;
        public String ITEMID { get; set; } = "";
        public String NAME { get; set; } = "";
        public String SALESUNIT { get; set; } = "";
        public String ITEMBARCODE { get; set; } = "";
        public String INVENTSERIALID { get; set; } = "";
        public String INVENTDIMID { get; set; } = "";
        //public String INVENTTRANSID { get; set; } = "";
        public String REFBOXTRANS { get; set; } = "";
    }
    //Trans
    public class MNTSaveTransRequest
    {
        public int LINENUM { get; set; } = 1;
        public String REFBOXTRANS { get; set; } = "";
        public String ITEMBARCODE { get; set; } = "";
        public double LINECAPACITY { get; set; } = 0;
        public double LINEWEIGHT { get; set; } = 0;
        public String BOXQTY { get; set; } = "";
        public String BOXTYPE { get; set; } = "";
        public String NAME { get; set; } = "";
        public String BOXGROUP { get; set; } = "";
    }
}