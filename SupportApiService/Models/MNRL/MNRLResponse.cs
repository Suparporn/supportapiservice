﻿using System;

namespace SupportApiService.Models.MNRL
{
    //บิล HD 
    public class MNRLNotApvData
    {

        public String DocNo { get; set; } = "";
        public String DATEINS { get; set; } = "";
        public String TIMEINS { get; set; } = "";
        public String DeptID { get; set; } = "";
        public String DeptName { get; set; } = "";
        public String UserID { get; set; } = "";
        public String UserName { get; set; } = "";
        public String StaPrcDoc { get; set; } = "";
    }

    //รายละเอียดบิล
    public class MNRLDetailData
    {
      
        public String DocNo { get; set; } = "";
        public String StaDoc { get; set; } = "";
        public String StaPrcDoc { get; set; } = "";
        public String ItemName { get; set; } = "";
        public double Qty { get; set; } = 1;
        public String DeptID { get; set; } = "";
        public String DeptName { get; set; } = "";

    }
}