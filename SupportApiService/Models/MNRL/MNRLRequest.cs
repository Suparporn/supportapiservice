﻿using System;

namespace SupportApiService.Models.MNRL
{
    //ข้อมูล HD+DT
    public class MNRLSaveRequest
    {
        public MNRLSaveHDRequest HD { get; set; } = new MNRLSaveHDRequest();
        public MNRLSaveDTRequest DT { get; set; } = new MNRLSaveDTRequest();
    }
    //ข้อมูล HD
    public class MNRLSaveHDRequest
    {
        public String DeptID { get; set; } = "";
        public String DeptName { get; set; } = "";
        public String UserID { get; set; } = "";
        public String UserNAME { get; set; } = "";

    }
    //ข้อมูล DT
    public class MNRLSaveDTRequest
    {
        public int SeqNo { get; set; } = 1;
        public String ItemName { get; set; } = "";
        public double Qty { get; set; } = 0;
        public String REMARK { get; set; } = "";
    }
}