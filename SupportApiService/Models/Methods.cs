﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace SupportApiService.Models
{
    public class Methods
    {
        public static string conShop24Hrs = "Data Source=192.168.100.24;Initial Catalog=SHOP24HRS;User Id=shop;Password=Ca999999;Connection Timeout = 60;";
        public static string con708AX = "Data Source=192.168.100.58;Initial Catalog=AX50SP1_SPC;User Id=shop24;Password=Ca999999;Connection Timeout = 60;";
        public static string con803 = "Data Source=192.168.100.156;Initial Catalog= CARTRACKING;User ID= sa;Password= Ca999999;Connection Timeout = 60;";

        public static string imagepath_ClaimInSRV = @"\\192.168.100.60\ImageMinimark\AssetClaimLeave\";
        public static string imagepath_BillInSRV = @"\\192.168.100.60\ImageMinimark\BillSupc\";
        public static string imagepath_IDMInSRV = @"\\192.168.100.60\ImageMinimark\IDM\";

        //For Return List
        public class GlobalClass<T> : ResponseDefault
        {
            public List<T> DATA { get; set; } = new List<T>();
        }

        //SendDataSelect For List
        public static GlobalClass<T> SelectDataSend<T>(string query, string connection, string function)
        {
            GlobalClass<T> response = new GlobalClass<T>();
            try
            {
                DataTable dt = SelectDataTable(query, connection);
                response.DATA = ConvertDataTableToList<T>(dt);
            }
            catch (Exception ex)
            {
                response.CODE = 417;
                response.MESSAGE = function + " : " + ex.ToString();
            }
            return response;
        }

        //Select Data For DT
        public static DataTable SelectDataTable(string query, string connection)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(connection))
                {
                    sqlcon.Open();
                    using (SqlCommand command = new SqlCommand()
                    {
                        Connection = sqlcon,
                        CommandType = CommandType.Text,
                        CommandText = query
                    })
                    {
                        using (var reader = command.ExecuteReader())
                            if (reader.HasRows)
                            {
                                dt.Load(reader);
                            }
                    }
                    if (sqlcon.State == ConnectionState.Open) sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return dt;
        }

        //Convert Datatable To List
        public static List<T> ConvertDataTableToList<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                Type temp = typeof(T);
                T obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in row.Table.Columns)
                {
                    foreach (PropertyInfo pro in temp.GetProperties())
                    {
                        if (pro.Name == column.ColumnName)
                        {
                            try
                            {
                                pro.SetValue(obj, row[column.ColumnName], null);
                            }
                            catch (Exception)
                            {
                                pro.SetValue(obj, Convert.ChangeType(row[column.ColumnName], pro.PropertyType), null);
                                continue;
                            }
                        }
                        else
                            continue;
                    }
                }
                T item = obj;
                data.Add(item);
            }
            return data;
        }

        //Insert/Update/Delete Data String
        public static ResponseDefault InsertSql(string query, string connection, string function)
        {
            ResponseDefault response = new ResponseDefault();
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(connection))
                {
                    sqlcon.Open();

                    using (SqlCommand command = new SqlCommand()
                    {
                        Connection = sqlcon,
                        CommandType = CommandType.Text,
                    })
                    {
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                    }
                    if (sqlcon.State == ConnectionState.Open) sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                response.CODE = 417;
                response.MESSAGE = function + " : " + ex.ToString();
            }
            return response;
        }

        //Insert/Update/Delete Data List
        public static ResponseDefault InsertSql(List<string> listSql, string connection, string function)
        {
            ResponseDefault response = new ResponseDefault();
            SqlConnection con = new SqlConnection();
            SqlTransaction tr = null;
            try
            {
                con.ConnectionString = connection;
                if (con.State == ConnectionState.Closed) con.Open();
                SqlCommand cmd = new SqlCommand();
                tr = con.BeginTransaction("SqlArrayTransaction");
                cmd.Connection = con;
                cmd.Transaction = tr;
                foreach (var sql in listSql)
                {
                    cmd.CommandText = sql;
                    cmd.CommandTimeout = 60;
                    cmd.ExecuteNonQuery();
                }
                tr.Commit();
                con.Close();
            }
            catch (Exception ex)
            {
                if (tr != null) tr.Rollback();
                con.Close();
                response.CODE = 417;
                response.MESSAGE = function + " : " + ex.ToString();
            }
            return response;
        }

        //Insert/Update/Delete Data String 2 Server
        public static ResponseDefault InsertSqlTwoServer(string query1, string connection1, string query2, string connection2, string function)
        {
            ResponseDefault response = new ResponseDefault();
            SqlConnection connectionSql1 = new SqlConnection();
            SqlConnection connectionSql2 = new SqlConnection();
            SqlTransaction tr1 = null;
            SqlTransaction tr2 = null;
            try
            {
                connectionSql1.ConnectionString = connection1;
                if (connectionSql1.State == ConnectionState.Closed) connectionSql1.Open();
                SqlCommand cmd1 = new SqlCommand();
                tr1 = connectionSql1.BeginTransaction("SqlTransaction1");
                cmd1.Connection = connectionSql1;
                cmd1.Transaction = tr1;

                connectionSql2.ConnectionString = connection2;
                if (connectionSql2.State == ConnectionState.Closed) connectionSql2.Open();
                SqlCommand cmd2 = new SqlCommand();
                tr2 = connectionSql2.BeginTransaction("SqlTransaction2");
                cmd2.Connection = connectionSql2;
                cmd2.Transaction = tr2;


                cmd1.CommandText = query1;
                cmd1.CommandTimeout = 60;
                cmd1.ExecuteNonQuery();

                cmd2.CommandText = query2;
                cmd2.CommandTimeout = 60;
                cmd2.ExecuteNonQuery();

                tr1.Commit();
                tr2.Commit();
            }
            catch (Exception ex)
            {
                if (tr1 != null) tr1.Rollback();
                if (tr2 != null) tr2.Rollback();
                response.CODE = 417;
                response.MESSAGE = function + " : " + ex.ToString();
            }
            connectionSql1.Close();
            connectionSql2.Close();
            return response;
        }

        //Insert/Update/Delete Data List 2 Server
        public static ResponseDefault InsertSqlTwoServer(List<string> listQuery1, string connection1, List<string> listQuery2, string connection2, string function)
        {
            ResponseDefault response = new ResponseDefault();
            SqlConnection connectionSql1 = new SqlConnection();
            SqlConnection connectionSql2 = new SqlConnection();
            SqlTransaction tr1 = null;
            SqlTransaction tr2 = null;
            try
            {
                connectionSql1.ConnectionString = connection1;
                if (connectionSql1.State == ConnectionState.Closed) connectionSql1.Open();
                SqlCommand cmd1 = new SqlCommand();
                tr1 = connectionSql1.BeginTransaction("SqlTransaction1");
                cmd1.Connection = connectionSql1;
                cmd1.Transaction = tr1;

                connectionSql2.ConnectionString = connection2;
                if (connectionSql2.State == ConnectionState.Closed) connectionSql2.Open();
                SqlCommand cmd2 = new SqlCommand();
                tr2 = connectionSql2.BeginTransaction("SqlTransaction2");
                cmd2.Connection = connectionSql2;
                cmd2.Transaction = tr2;

                foreach (var query1 in listQuery1)
                {
                    cmd1.CommandText = query1;
                    cmd1.CommandTimeout = 60;
                    cmd1.ExecuteNonQuery();
                }

                foreach (var query2 in listQuery2)
                {
                    cmd2.CommandText = query2;
                    cmd2.CommandTimeout = 60;
                    cmd2.ExecuteNonQuery();
                }

                tr1.Commit();
                tr2.Commit();
            }
            catch (Exception ex)
            {
                if (tr1 != null) tr1.Rollback();
                if (tr2 != null) tr2.Rollback();
                response.CODE = 417;
                response.MESSAGE = function + " : " + ex.ToString();
            }
            connectionSql1.Close();
            connectionSql2.Close();
            return response;
        }

        //สำหรับ MaxDocno ที่ดึงใช้งานภายในฟังก์ชั่น
        public static string GetMaxINVOICEIDString(string type, string pos, string frist, string cases)
        {
            try
            {
                DataTable dt = SelectDataTable($@"[config_SHOP_GENARATE_BILLAUTO] '{type}','{pos}','{frist}','{cases}'", conShop24Hrs);
                return dt.Rows[0]["ID"].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("[SHOP24HRS_GetMaxINVOICEID]  " + ex.ToString());
            }
        }

        //สำหรับการ Return MaxDocno 
        public static ResponseDefault GetMaxINVOICEID(string type, string pos, string frist, string cases)
        {
            ResponseDefault res = new ResponseDefault();
            try
            {
                DataTable dt = SelectDataTable($@"[config_SHOP_GENARATE_BILLAUTO] '{type}','{pos}','{frist}','{cases}'", conShop24Hrs);
                res.MESSAGE = dt.Rows[0]["ID"].ToString();
            }
            catch (Exception ex)
            {
                res.CODE = 417;
                res.MESSAGE = "ไม่สามารถดึงเลขที่บิลได้" + " : " + ex.ToString();
            }
            return res;
        }
    }
}