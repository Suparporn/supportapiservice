﻿using System;
namespace SupportApiService.Models.Employee
{
    //ข้อมูลพนักงาน
    public class EmpLoginData
    {
        public String EMP_ID { get; set; } = "";
        public String SPC_NAME { get; set; } = "";
        public String NUM { get; set; } = "";
        public String DESCRIPTION { get; set; } = "";
        public String DateToDay { get; set; } = "";
        public String EMP_PASSWORD { get; set; } = "";
    
    }
}