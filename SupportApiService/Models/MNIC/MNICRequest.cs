﻿using System;

namespace SupportApiService.Models.MNIC
{

    public class MNICSaveRequest
    {
        public MNICHDSaveRequest HD { get; set; } = new MNICHDSaveRequest();
        public MNICDTSaveRequest DT { get; set; } = new MNICDTSaveRequest();
    }

    //ข้อมูล HD
    public class MNICHDSaveRequest
    {
        public String MNICInvent { get; set; } = "";
        public String MNICDptWho { get; set; } = "";
        public String MNICSPC { get; set; } = "";
        public String MNICWhoIn { get; set; } = "";
    }
    //ข้อมูล DT
    public class MNICDTSaveRequest
    {
        public String MNICSeqNo { get; set; } = "";
        public String MNICItemID { get; set; } = "";
        public String MNICDimid { get; set; } = "";
        public String MNICBarcode { get; set; } = "";
        public String MNICName { get; set; } = "";
        public String MNICQty { get; set; } = "";
        public String MNICPrice { get; set; } = "";
        public String MNICUnitID { get; set; } = "";
        public String MNICFactor { get; set; } = "";
        public String MNICVender { get; set; } = "";
        public String MNICVenderName { get; set; } = "";
        public String MNICReson { get; set; } = "";//reasonID
        public String MNICRemark { get; set; } = "";// reasonName
        public String MNICWhoIn { get; set; } = "";
    }
}