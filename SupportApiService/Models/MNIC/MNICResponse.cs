﻿using System;

namespace SupportApiService.Models.MNIC
{
    //รายละเอียดบิล
    public class MNICDetailData
    {
        public String MNICDocNo { get; set; } = "";
        public String MNICBarcode { get; set; } = "";
        public String MNICName { get; set; } = "";
        public double MNICQty { get; set; } = 0;
        public String MNICUnitID { get; set; } = "";
        public double MNICPrice { get; set; } = 0;
        public String MNICVender { get; set; } = "";
        public String MNICVenderName { get; set; } = "";
        public String MNICReson { get; set; } = "";
        public String Remark { get; set; } = "";
        public String MNICStaPrcDoc { get; set; } = "0";
        public String MNICStaDoc { get; set; } = "1";
        public String MNICRemark { get; set; } = "";
    }

    //เหตุผลการคืน
    public class MNICReason
    {
        public String REASONID { get; set; } = "";
        public String REASONNAME { get; set; } = "";
    }

    //บิลที่ค้างอนุมัติรายการทั้งหมด ตามแผนก
    public class MNICNotApv
    {
        public String MNICDocNo { get; set; } = "";
        public String MNICDate { get; set; } = "";
        public String MNICTime { get; set; } = "";
        public String MNICWhoIn { get; set; } = "";
        public String MNICInvent { get; set; } = "";
    }
}