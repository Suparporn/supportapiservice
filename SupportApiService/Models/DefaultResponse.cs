﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiService.Models
{
    public class DefaultResponse
    {
        /// <summary>
        /// Code Status Number
        /// </summary>
        public int Code { get; set; } = 0;
        /// <summary>
        /// Message Description 
        /// </summary>
        public string Message { get; set; } = "";
        /// <summary>
        /// Message Error
        /// </summary>
        public string ErrorMassage { get; set; } = "";
    }
}