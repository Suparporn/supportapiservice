﻿using ApiService.Models.TableModel.Support;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiService.ExternalDatabase
{
    public class Support_ClassConnectDb
    {
        public static string con119 = "Data Source= 192.168.100.24 ;Initial Catalog= SHOP24HRS;User ID= sa;Password= Ca999999;Connection Timeout = 60;";
        public static string con708 = "Data Source= 192.168.100.58 ;Initial Catalog= AX50SP1_SPC;User ID= sa;Password= Ca999999;Connection Timeout = 60;";

        public static DataTable GetDataFromSQL(string constr, string sql, CommandType commandtype, List<PassParameter> param)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(constr))
            {
                try
                {
                    con.Open();

                    using (SqlCommand command = new SqlCommand()
                    {
                        Connection = con,
                        CommandType = commandtype,
                        CommandText = sql,
                        CommandTimeout = 999
                    })
                    {
                        for (int i = 0; i < param.Count; i++)
                        {
                            command.Parameters.AddWithValue(param[i].name, param[i].value);
                        }

                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.Fill(ds);
                    }

                    con.Close();
                    return ds.Tables[0];
                }
                catch (Exception ex)
                {
                    con.Close();
                    throw new Exception(ex.Message);
                }
            }
        }

        public static void PostDataToSQL(string constr, string sql, CommandType commandtype)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();

                using (SqlTransaction transaction = con.BeginTransaction("QueueAdd"))
                {
                    try
                    {
                        using (SqlCommand command = new SqlCommand()
                        {
                            Connection = con,
                            CommandType = commandtype,
                            CommandText = sql,
                            CommandTimeout = 999,
                            Transaction = transaction
                        })
                        {
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }
        }


        public static string DecodePassword(string pass)
        {
            string res = "";
            for (int i = 0; i <= pass.Length - 1; i++)
            {
                if (i % 2 == 1)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) + 4);
                else if (i % 2 == 0)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(pass, i) - 4);
            }
            return res;
        }

        internal static IEnumerable<EmplReturn> GetEmployee(EmplLogin Data)
        {
            List<PassParameter> param = new List<PassParameter>();
            param.Add(new PassParameter { name = "@StrEmpALTNUM", value = Data.Emplid });

            DataTable BAR = GetDataFromSQL(con119,
                String.Format(@"EmplClass_GetEmployee"),
                CommandType.StoredProcedure,
                param);

            IList<EmplReturn> items = BAR.AsEnumerable().Select(row =>
            new EmplReturn
            {
                Emplid = row.Field<string>("EMP_ID"),
                Emplpass = row.Field<string>("EMP_PASSWORD"),
                Name = row.Field<string>("SPC_NAME"),
                Deptid = row.Field<string>("NUM"),
                Deptname = row.Field<string>("DESCRIPTION")
            }).ToList();

            return items.ToList();
        }

        internal static IEnumerable<ConfigReturn> GetConfig(Config Data)
        {
            List<PassParameter> param = new List<PassParameter>();

            DataTable BAR = GetDataFromSQL(con119,
                String.Format(@"  SELECT [COM_NAME]
                                        ,[DEPT_ID]
                                        ,[DIMENSIONS].[DESCRIPTION] AS [DEPT_NAME]
                                        ,[SHOW_ID]
                                        ,[VERSIONUPD]
		                                ,[CHANNEL] AS [WAREHOUSE]
		                                ,[PRICEGROUP]
		                                ,[REMARK] AS [PRINTTER]
                                  FROM [SHOP24HRS].[dbo].[SHOP_BRANCH_PERMISSION] WITH (NOLOCK)

                                  LEFT JOIN [SHOP2013TMP].[dbo].[DIMENSIONS] WITH (NOLOCK)
                                  ON [DATAAREAID] = 'SPC'
	                                 AND [DIMENSIONCODE] = 0
	                                 AND [NUM] = [DEPT_ID]

                                  WHERE [COM_NAME] = '{0}'
                                        AND [SHOW_ID] = 'A04'"

                                , Data.Devicename),
                CommandType.Text,
                param);

            IList<ConfigReturn> items = BAR.AsEnumerable().Select(row =>
            new ConfigReturn
            {
                Comname = row.Field<string>("COM_NAME"),
                Deptid = row.Field<string>("DEPT_ID"),
                Deptname = row.Field<string>("DEPT_NAME"),
                Showid = row.Field<string>("SHOW_ID"),
                Versionupd = row.Field<string>("VERSIONUPD"),
                Warehouse = row.Field<string>("WAREHOUSE"),
                Pricegroup = row.Field<string>("PRICEGROUP"),
                Printer = row.Field<string>("PRINTTER")
            }).ToList();

            return items.ToList();
        }

        internal static IEnumerable<AllBarcodeDataReturn> GetAllBarcodeData(AllBarcodeData Data)
        {
            List<PassParameter> param = new List<PassParameter>();
            param.Add(new PassParameter { name = "@ITEMID", value = Data.Itemid });
            param.Add(new PassParameter { name = "@INVENTDIMID", value = Data.Inventdimid });
            param.Add(new PassParameter { name = "@Price", value = Data.Pricelevel });

            DataTable BAR = GetDataFromSQL(con119,
                String.Format(@"ItembarcodeClass_FindItembarcodeAll_ByItemidDim_ByPrice"),
                CommandType.StoredProcedure,
                param);

            IList<AllBarcodeDataReturn> items = BAR.AsEnumerable().Select(row =>
            new AllBarcodeDataReturn
            {
                Itembarcode = row.Field<string>("ITEMBARCODE"),
                Itemid = row.Field<string>("ITEMID"),
                Inventdimid = row.Field<string>("INVENTDIMID"),
                Qty = row.Field<Decimal>("QTY").ToString("0.##"),
                Unitid = row.Field<string>("UNITID"),
                Itemname = row.Field<string>("SPC_ITEMNAME"),
                Itemactive = row.Field<int>("SPC_ITEMACTIVE"),
                Pricegroup3 = row.Field<Decimal>("SPC_PRICEGROUP3").ToString("0.##"),
                Pricegroup13 = row.Field<Decimal>("SPC_PRICEGROUP13").ToString("0.##"),
                Pricegroup14 = row.Field<Decimal>("SPC_PRICEGROUP14").ToString("0.##"),
                Pricegroup15 = row.Field<Decimal>("SPC_PRICEGROUP15").ToString("0.##"),
                Imagepath = row.Field<string>("SPC_IMAGEPATH"),
                Burmaname = row.Field<string>("SPC_BurmaName"),
                Ok = row.Field<string>("OK"),
                Description = row.Field<string>("DESCRIPTION"),
                Pickinglocationid = row.Field<string>("SPC_PickingLocationId"),
                Rfiditemtagging = row.Field<int>("SPC_RFIDItemTagging"),
                Barcodeweight = row.Field<Decimal>("SPC_BarCodeWeight").ToString("0.##"),
                Capacity = row.Field<Decimal>("CAPACITY").ToString("0.##"),
                Pricemn = row.Field<Decimal>("PRICEMN").ToString("0.##"),
                Price = row.Field<Decimal>("PRICE").ToString("0.##")
            }).ToList();

            return items.ToList();
        }

        internal static IEnumerable<BarcodeDataReturn> GetBarcodeData(BarcodeData Data)
        {
            List<PassParameter> param = new List<PassParameter>();
            param.Add(new PassParameter { name = "@Itembarcode", value = Data.Itembarcode });
            param.Add(new PassParameter { name = "@Price", value = Data.Pricelevel });

            DataTable BAR = GetDataFromSQL(con119,
                String.Format(@"ItembarcodeClass_GetItembarcode_ALLDeatil_ByPrice"),
                CommandType.StoredProcedure,
                param);

            IList<BarcodeDataReturn> items = BAR.AsEnumerable().Select(row =>
            new BarcodeDataReturn
            {
                Itembarcode = row.Field<string>("ITEMBARCODE"),
                Itemid = row.Field<string>("ITEMID"),
                Inventdimid = row.Field<string>("INVENTDIMID"),
                Qty = row.Field<Decimal>("QTY").ToString("0.##"),
                Unitid = row.Field<string>("UNITID"),
                Itemname = row.Field<string>("SPC_ITEMNAME"),
                Itemactive = row.Field<int>("SPC_ITEMACTIVE"),
                Pricegroup3 = row.Field<Decimal>("SPC_PRICEGROUP3").ToString("0.##"),
                Pricegroup4 = row.Field<Decimal>("SPC_PRICEGROUP4").ToString("0.##"),
                Pricegroup13 = row.Field<Decimal>("SPC_PRICEGROUP13").ToString("0.##"),
                Pricegroup14 = row.Field<Decimal>("SPC_PRICEGROUP14").ToString("0.##"),
                Pricegroup15 = row.Field<Decimal>("SPC_PRICEGROUP15").ToString("0.##"),
                Imagepath = row.Field<string>("SPC_IMAGEPATH"),
                Primaryvendorid = row.Field<string>("PRIMARYVENDORID"),
                Primaryvendoridname = row.Field<string>("PRIMARYVENDORIDNAME"),
                Dimension = row.Field<string>("DIMENSION"),
                Description = row.Field<string>("DESCRIPTION"),
                Salespricetype = row.Field<int>("SPC_SalesPriceType"),
                Grpid0 = row.Field<string>("GRPID0"),
                Tax0 = row.Field<string>("TAX0"),
                Grpid1 = row.Field<string>("GRPID1"),
                Tax1 = row.Field<string>("TAX1"),
                Grpid2 = row.Field<string>("GRPID2"),
                Tax2 = row.Field<string>("TAX2"),
                Giftpoint = row.Field<int>("SPC_GiftPoint"),
                Burmaname = row.Field<string>("SPC_BurmaName"),
                Salespricetypename = row.Field<string>("SPC_SalesPriceTypeNAME"),
                Pickinglocationid = row.Field<string>("SPC_PickingLocationId"),
                Price = row.Field<Decimal>("PRICE").ToString("0.##"),
                Nameinvent = row.Field<string>("NameInvent"),
                Num = row.Field<string>("NUM"),
                Pricemn = row.Field<Decimal>("PRICEMN").ToString("0.##"),
                Accountnum = row.Field<string>("ACCOUNTNUM"),
                Name = row.Field<string>("NAME")
            }).ToList();

            return items.ToList();
        }

        internal static IEnumerable<BarcodeStockReturn> GetBarcodeStock(BarcodeStock Data)
        {
            List<PassParameter> param_dim = new List<PassParameter>();
            param_dim.Add(new PassParameter { name = "@pBarcode", value = Data.Itembarcode });

            DataTable dt_dim = GetDataFromSQL(con119,
                String.Format(@"ItembarcodeClass_FindDIM_ByBarcode"),
                CommandType.StoredProcedure,
                param_dim);

            if (dt_dim.Rows.Count > 0)
            {
                List<PassParameter> param_stock = new List<PassParameter>();
                param_stock.Add(new PassParameter { name = "@pBchID", value = Data.Warehouse });
                param_stock.Add(new PassParameter { name = "@ITEMID", value = dt_dim.Rows[0]["ITEMID"].ToString() });
                param_stock.Add(new PassParameter { name = "@CONFIGID", value = dt_dim.Rows[0]["CONFIGID"].ToString() });
                param_stock.Add(new PassParameter { name = "@INVENTSIZEID", value = dt_dim.Rows[0]["INVENTSIZEID"].ToString() });
                param_stock.Add(new PassParameter { name = "@INVENTCOLORID", value = dt_dim.Rows[0]["INVENTCOLORID"].ToString() });
                param_stock.Add(new PassParameter { name = "@BatchNumGroupId", value = dt_dim.Rows[0]["BatchNumGroupId"].ToString() });

                DataTable dt_stock = GetDataFromSQL(con119,
                String.Format(@"ItembarcodeClass_FindStockByDIM"),
                CommandType.StoredProcedure,
                param_stock);

                if (dt_stock.Rows.Count > 0)
                {
                    IList<BarcodeStockReturn> items = dt_stock.AsEnumerable().Select(row =>
                    new BarcodeStockReturn
                    {
                        Stock = row.Field<Decimal>("AVAILPHYSICAL").ToString("0.##")
                    }).ToList();

                    return items.ToList();
                }
            }

            IList<BarcodeStockReturn> stockdefault = new List<BarcodeStockReturn>();
            BarcodeStockReturn defaultvalue = new BarcodeStockReturn();
            defaultvalue.Stock = "0";
            stockdefault.Add(defaultvalue);

            return stockdefault;
        }

        internal static void SaveLogUsed(LogUsed Data)
        {
            PostDataToSQL(con119,
                String.Format(@"Insert into [SHOP24HRS].[dbo].[SHOP_LOGSHOP24HRS](LogBranch
                                                                                 ,LogBranchName
                                                                                 ,LogType
                                                                                 ,LogWhoID
                                                                                 ,LogWhoIDName
                                                                                 ,LogRow
                                                                                 ,LogMachine
                                                                                 ,LogTmp)
                                values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')"

                               , Data.LogBranch
                               , Data.LogBranchName
                               , Data.LogType
                               , Data.LogWhoID
                               , Data.LogWhoIDName
                               , Data.LogRow
                               , Data.LogMachine
                               , Data.LogTmp),
                CommandType.Text);
        }

        internal static void SaveChangeSign(ChangeSign Data)
        {
            PostDataToSQL(con119,
                String.Format(@" Insert into [SHOP24HRS].[dbo].[SHOP_CHECKPRICEFOREXPORT]( [CheckWho]
                                                                                          ,[CheckWhoName]
                                                                                          ,[ITEMBARCODE]
                                                                                          ,[SPC_ITEMNAME]
                                                                                          ,[DptID]
                                                                                          ,[DptName]
                                                                                          ,[CheckPrice]
                                                                                          ,[UNITID]
                                                                                          ,[CheckPDA]
                                                                                          ,[CheckDepart]
                                                                                          ,[CheckDepartName] )
                                values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')"

                               , Data.Checkwho
                               , Data.Checkwhoname
                               , Data.Itembarcode
                               , Data.Itemname
                               , Data.Deptid
                               , Data.Deptname
                               , Data.Checkprice
                               , Data.Unitid
                               , Data.CheckPDA
                               , Data.Checkdepart
                               , Data.Checkdepartname),
                CommandType.Text);
        }

        internal static IEnumerable<InventReturn> GetInvent(Invent Data)
        {
            List<PassParameter> param = new List<PassParameter>();
            param.Add(new PassParameter { name = "@pcase", value = Data.pCase });
            param.Add(new PassParameter { name = "@pInventID", value = Data.pInventid });

            DataTable BAR = GetDataFromSQL(con119,
                String.Format(@"SupcSupport_Invent"),
                CommandType.StoredProcedure,
                param);

            IList<InventReturn> items = new List<InventReturn>();

            if (Data.pCase.Equals("0"))
            {
                items = BAR.AsEnumerable().Select(row =>
                new InventReturn
                {
                    Inventlocationid = row.Field<string>("INVENTLOCATIONID"),
                    Inventname = row.Field<string>("INTNAME")
                }).ToList();
            }
            else if (Data.pCase.Equals("3"))
            {
                items = BAR.AsEnumerable().Select(row =>
                new InventReturn
                {
                    Code = row.Field<string>("CODE"),
                    Codetxt = row.Field<string>("CODE_TXT")
                }).ToList();
            }

            return items.ToList();
        }

        internal static IEnumerable<MntfMaxIdDocnoReturn> GetMntfMaxIdDocno(MntfDocno Data)
        {
            List<PassParameter> param = new List<PassParameter>();
            param.Add(new PassParameter { name = "@TYPE", value = Data.pType });
            param.Add(new PassParameter { name = "@POS", value = Data.pPos });
            param.Add(new PassParameter { name = "@FIRST", value = Data.pFirst });
            param.Add(new PassParameter { name = "@pCase", value = Data.pCase });

            DataTable BAR = GetDataFromSQL(con119,
                String.Format(@"config_SHOP_GENARATE_BILLAUTO"),
                CommandType.StoredProcedure,
                param);

            IList<MntfMaxIdDocnoReturn> items = new List<MntfMaxIdDocnoReturn>();

            if (Data.pCase.Equals("1"))
            {
                items = BAR.AsEnumerable().Select(row =>
                new MntfMaxIdDocnoReturn
                {
                    Id = row.Field<string>("ID")
                }).ToList();
            }

            return items.ToList();
        }

        internal static void SaveMntfData(MntfData Data)
        {
            string queryrun = "";

            switch (Data.Tag)
            {
                case 1:
                    queryrun = String.Format(@"   Insert Into [SHOP24HRS].[dbo].[SHOP_MNTF_DT] ( MNTFDocNo
												                                                ,MNTFSeqNo
												                                                ,MNTFItemID
												                                                ,MNTFDimid
												                                                ,MNTFBarcode
												                                                ,MNTFName
												                                                ,MNTFQtyOrder
												                                                ,MNTFUnitID
												                                                ,MNTFPrice
												                                                ,MNTFFactor
												                                                ,MNTFWhoIn
												                                                ,MNTFWhoInName )
                                                  values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}')"

                                                , Data.Mntfdocno
                                                , Data.Datasavedt.MNTFSeqno
                                                , Data.Datasavedt.MNTFItemid
                                                , Data.Datasavedt.MNTFDimid
                                                , Data.Datasavedt.MNTFBarcode
                                                , Data.Datasavedt.MNTFName
                                                , Data.Datasavedt.MNTFQtyorder
                                                , Data.Datasavedt.MNTFUnitid
                                                , Data.Datasavedt.MNTFPrice
                                                , Data.Datasavedt.MNTFFactor
                                                , Data.Datasavedt.MNTFWhoin
                                                , Data.Datasavehd.MNTFUsercodename);
                    break;

                case 2:
                    queryrun = String.Format(@"   Insert Into [SHOP24HRS].[dbo].[SHOP_MNTF_HD] ( MNTFDocNo
											                                                    ,MNTFUserCode
											                                                    ,MNTFUserCodeName
											                                                    ,MNTFTypeOrder
											                                                    ,MNTFWHSoure
											                                                    ,MNTFWHDestination
											                                                    ,MNTFLock
											                                                    ,MNTFRemark
											                                                    ,MNTFSPCNAME
											                                                    ,MNTFDepart
											                                                    ,MNTFDepartName ) 
                                                  values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}')

                                                  Insert Into [SHOP24HRS].[dbo].[SHOP_MNTF_DT] ( MNTFDocNo
												                                                ,MNTFSeqNo
												                                                ,MNTFItemID
												                                                ,MNTFDimid
												                                                ,MNTFBarcode
												                                                ,MNTFName
												                                                ,MNTFQtyOrder
												                                                ,MNTFUnitID
												                                                ,MNTFPrice
												                                                ,MNTFFactor
												                                                ,MNTFWhoIn
												                                                ,MNTFWhoInName )
                                                  values ('{0}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{2}')"

                                                , Data.Mntfdocno
                                                , Data.Datasavehd.MNTFUsercode
                                                , Data.Datasavehd.MNTFUsercodename
                                                , Data.Type
                                                , Data.Datasavehd.MNTFWHSoure
                                                , Data.Datasavehd.MNTFWHDestination
                                                , Data.Datasavehd.MNTFLock
                                                , Data.Datasavehd.Transferid
                                                , Data.Pdaname
                                                , Data.pDpt
                                                , Data.pDptname
                                                , Data.Datasavedt.MNTFSeqno
                                                , Data.Datasavedt.MNTFItemid
                                                , Data.Datasavedt.MNTFDimid
                                                , Data.Datasavedt.MNTFBarcode
                                                , Data.Datasavedt.MNTFName
                                                , Data.Datasavedt.MNTFQtyorder
                                                , Data.Datasavedt.MNTFUnitid
                                                , Data.Datasavedt.MNTFPrice
                                                , Data.Datasavedt.MNTFFactor
                                                , Data.Datasavedt.MNTFWhoin);
                    break;

                default:
                    break;
            }

            PostDataToSQL(con119, queryrun, CommandType.Text);
        }

        internal static void CancelMntfBill(CancelMntf Data)
        {
            string queryrun = "";
            queryrun = String.Format(@" Update SHOP_MNTF_HD 
                                        SET MNTFStaDoc = 0
                                          , MNTFDateUp = convert(varchar,getdate(),25)
                                          , MNTFWhoUp = '{0}'
                                          , MNTFWhoUpName = '{1}'
                                        WHERE MNTFDocNo = '{2}' "

                                       , Data.pUser
                                       , Data.pUsername
                                       , Data.pDocno);

            PostDataToSQL(con119, queryrun, CommandType.Text);
        }

        internal static IEnumerable<MntfFindReturn> MntfFinddata(MntfFind Data)
        {
            List<PassParameter> param = new List<PassParameter>();
            param.Add(new PassParameter { name = "@pcase", value = Data.pCase });
            param.Add(new PassParameter { name = "@pType", value = Data.pType });
            param.Add(new PassParameter { name = "@pDptID", value = Data.pDptid });
            param.Add(new PassParameter { name = "@pInventID", value = Data.pInventid });
            param.Add(new PassParameter { name = "@pDocno", value = Data.pDocno });
            param.Add(new PassParameter { name = "@pBarcode", value = Data.pBarcode });

            DataTable BAR = GetDataFromSQL(con119,
                String.Format(@"SupcSupport_MNTF_FindData"),
                CommandType.StoredProcedure,
                param);

            IList<MntfFindReturn> items = new List<MntfFindReturn>();

            if (Data.pCase.Equals("1"))
            {
                items = BAR.AsEnumerable().Select(row =>
                new MntfFindReturn
                {
                    MntfDocno = row.Field<string>("MNTFDocNo"),
                    MntfDate = row.Field<string>("MNTFDate"),
                    MntfTime = row.Field<string>("MNTFTime"),
                    MntfUserid = row.Field<string>("MNTFUserCode"),
                    MntfWhSource = row.Field<string>("MNTFWHSoure"),
                    MntfWhDes = row.Field<string>("MNTFWHDestination"),
                    MntfLock = row.Field<string>("MNTFLock")
                }).ToList();
            } 
            else if (Data.pCase.Equals("3"))
            {
                items = BAR.AsEnumerable().Select(row =>
                new MntfFindReturn
                {
                    MntfDocno = row.Field<string>("MNTFDocNo"),
                    MntfDate = row.Field<string>("MNTFDate"),
                    MntfTime = row.Field<string>("MNTFTime"),
                    MntfUserid = row.Field<string>("MNTFUserCode"),
                    MntfWhSource = row.Field<string>("MNTFWHSoure"),
                    MntfWhDes = row.Field<string>("MNTFWHDestination"),
                    MntfLock = row.Field<string>("MNTFLock"),
                    MntfBarcode = row.Field<string>("MNTFBarcode"),
                    MntfName = row.Field<string>("MNTFName"),
                    MntfQtyOrder = row.Field<double>("MNTFQtyOrder"),
                    MntfUnitid = row.Field<string>("MNTFUnitID"),
                    Txt = row.Field<string>("TXT"),
                    NameSource = row.Field<string>("NAMESoure"),
                    NameDes = row.Field<string>("NAMEDes"),
                    MntfStatusApv = row.Field<string>("MNTFStaApvDoc")
                }).ToList();
            }
            else if (Data.pCase.Equals("6"))
            {
                items = BAR.AsEnumerable().Select(row =>
                new MntfFindReturn
                {
                    LineNum = row.Field<int>("LineNum"),
                    MntfDocno = row.Field<string>("DOCNO"),
                    MntfWhSource = row.Field<string>("WHSoure"),
                    MntfWhDes = row.Field<string>("WHDestination"),
                    MntfLock = row.Field<string>("Lock"),
                    MntfItemid = row.Field<string>("ITEMID"),
                    MntfDimid = row.Field<string>("INVENTDIM"),
                    MntfBarcode = row.Field<string>("ITEMBARCODE"),
                    MntfName = row.Field<string>("SPC_ITEMNAME"),
                    MntfQtyOrder = row.Field<double>("QtyOrder"),
                    MntfUnitid = row.Field<string>("UnitID"),
                    MntfFactor = row.Field<double>("QTY"),
                    MntfUserid = row.Field<string>("MNTFUserCode"),
                    MntfPrice = row.Field<double>("PRICE"),
                    MntfSpcName = row.Field<string>("RMK"),
                    MntfDate = row.Field<string>("DateIns")
                }).ToList();
            }

            return items.ToList();
        }

        internal static void ApvBill(ApvBill Data, IEnumerable<MntfFindReturn> DataForAX)
        {
            string queryrun_update24 = "";
            string queryrun_insertAXhd = "";
            string queryrun_insertAXdt = "";

            queryrun_update24 = String.Format(@" Update [SHOP24HRS].[dbo].[SHOP_MNTF_HD]
                                                 SET MNTFStaApvDoc = '1'
	                                               , MNTFStaPrcDoc = '1'
                                                   , MNTFDateApv = convert(varchar,getdate(),25)
	                                               , MNTFWhoApv = '{0}'
                                                   , MNTFWhoApvName = '{1}'
                                                 WHERE MNTFDocNo = '{2}' "

                                               , Data.pUser
                                               , Data.pUsername
                                               , Data.pDocno);

            queryrun_insertAXhd = String.Format(@" insert into SPC_POSTOTABLE20 (  FREIGHTSLIPTYPE
							                                                     , CASHIERID
							                                                     , ALLPRINTSTICKER
							                                                     , DATAAREAID
							                                                     , RECVERSION
							                                                     , RECID
							                                                     , TRANSFERSTATUS
							                                                     , INVENTLOCATIONIDTO
							                                                     , INVENTLOCATIONIDFROM
							                                                     , TRANSFERID
							                                                     , SHIPDATE
							                                                     , VOUCHERID
							                                                     , DLVTERM
							                                                     , REMARKS)
                                                    values('0', '{0}', '0', 'SPC', '1', '1', '{1}', '{2}', '{3}', '{4}', '{5}', '{4}', '{6}', '{7}' )"

                                                  , DataForAX.ElementAt(0).MntfUserid
                                                  , Data.pType
                                                  , DataForAX.ElementAt(0).MntfWhDes
                                                  , DataForAX.ElementAt(0).MntfWhSource
                                                  , DataForAX.ElementAt(0).MntfDocno
                                                  , DataForAX.ElementAt(0).MntfDate
                                                  , DataForAX.ElementAt(0).MntfLock
                                                  , DataForAX.ElementAt(0).MntfSpcName);

            using (SqlConnection con = new SqlConnection(con708))
            {
                con.Open();

                using (SqlTransaction transaction = con.BeginTransaction("QueueAdd"))
                {
                    try
                    {
                        using (SqlCommand commandAXhd = new SqlCommand()
                        {
                            Connection = con,
                            CommandType = CommandType.Text,
                            CommandText = queryrun_insertAXhd,
                            CommandTimeout = 999,
                            Transaction = transaction
                        })
                        {
                            commandAXhd.ExecuteNonQuery();
                        }

                        foreach (var item in DataForAX.Select((value, index) => new { Value = value, Index = index }))
                        {
                            queryrun_insertAXdt = String.Format(@"  insert into SPC_POSTOLINE20 ( RECID
							                                                                    , REFBOXTRANS
							                                                                    , DATAAREAID
							                                                                    , VOUCHERID
							                                                                    , SHIPDATE
							                                                                    , LINENUM
							                                                                    , QTY
							                                                                    , SALESPRICE 
							                                                                    , ITEMID
							                                                                    , NAME
							                                                                    , SALESUNIT
							                                                                    , ITEMBARCODE
							                                                                    , INVENTDIMID
							                                                                    , INVENTTRANSID
							                                                                    , RECVERSION )
                                                                    values('1', '0', 'SPC', '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '1')"

                                                                  , item.Value.MntfDocno
                                                                  , item.Value.MntfDate
                                                                  , item.Index + 1
                                                                  , item.Value.MntfQtyOrder
                                                                  , item.Value.MntfPrice
                                                                  , item.Value.MntfItemid
                                                                  , item.Value.MntfName.Replace("'", "")
                                                                  , item.Value.MntfUnitid
                                                                  , item.Value.MntfBarcode
                                                                  , item.Value.MntfDimid
                                                                  , item.Value.MntfDocno + "-" + item.Value.LineNum);

                            using (SqlCommand commandAXdt = new SqlCommand()
                            {
                                Connection = con,
                                CommandType = CommandType.Text,
                                CommandText = queryrun_insertAXdt,
                                CommandTimeout = 999,
                                Transaction = transaction
                            })
                            {
                                commandAXdt.ExecuteNonQuery();
                            }
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            PostDataToSQL(con119, queryrun_update24, CommandType.Text);
        }

        internal static void MNTF_EditQtyLineBill(MntfEditQty Data)
        {
            string queryrun = "";

            if (Data.pQty.Equals(0))
            {
                queryrun = String.Format(@" Update SHOP_MNTF_DT 
                                            SET MNTFQtyOrder = '{0}' 
                                              , MNTFStaDT = '0'
                                              , MNTFDateUp = convert(varchar, getdate(), 25)
                                              , MNTFWhoUp = '{1}'
                                              , MNTFWhoUpName = '{2}'
                                            WHERE MNTFDocNo = '{3}'
                                                  AND MNTFSeqNo = '{4}'"

                                          , Data.pQty
                                          , Data.pUser
                                          , Data.pUsername
                                          , Data.pDocno
                                          , Data.pSeqNo);
            } 
            else
            {
                queryrun = String.Format(@" Update SHOP_MNTF_DT 
                                            SET MNTFQtyOrder = '{0}'
                                              , MNTFDateUp = convert(varchar,getdate(),25)
                                              , MNTFWhoUp = '{1}'
                                              , MNTFWhoUpName = '{2}'
                                            WHERE MNTFDocNo = '{3}' 
                                                  AND MNTFSeqNo = '{4}'"

                                          , Data.pQty
                                          , Data.pUser
                                          , Data.pUsername
                                          , Data.pDocno
                                          , Data.pSeqNo);
            }

            PostDataToSQL(con119, queryrun, CommandType.Text);
        }
    }
}