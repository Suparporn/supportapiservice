﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using static SupportApiService.Models.Methods;
using SupportApiService.Models.CNReciveBox;
using SupportApiService.Models;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/CNReciveBox")]
    public class CNReciveBoxController : ApiController
    {

        //ค้นหา [MNPC_FindTagMNPC]
        [HttpGet, Route("CNReciveBox_FindTag")]
        public GlobalClass<CNReciveBoxTagDetailDATA> CNReciveBox_FindTag(string tagNumber)
        {
            //มี 4 หมวด MNPC-MNRB-MNRG-MNRH
            return SelectDataSend<CNReciveBoxTagDetailDATA>($@" SupcSupport_CN_RECIVEBOX '0','{tagNumber}' ", Methods.conShop24Hrs, "CNReciveBox_FindTag");
        }

        //Update ข้อมูลการรับ [MNPC_Recive]
        [HttpGet, Route("CNReciveBox_UpdateRecive")]
        public ResponseDefault CNReciveBox_UpdateRecive(string docNo, string tagNumber, string whoId, string whoName)
        {
            List<string> sql = new List<string>
            {
                $@"
                UPDATE  SHOP_MNPC_TAG
                SET     TagReciveSta = '1',TagReciveDate = convert(varchar,getdate(),23),
                        TagReciveTime = convert(varchar,getdate(),24),TagReciveWho = '{whoId}' 
                WHERE   TagNumber = '{tagNumber}' "
                ,
                $@"
                UPDATE  SHOP_MNRB
                SET     [Bill_TypeCNRecive] = '1',[Bill_TypeCNDate] = convert(varchar,getdate(),23),
                        [Bill_TypeCNTime] = convert(varchar,getdate(),24),[Bill_TypeCNWhoID] = '{whoId}',Bill_TypeCNWhoName = '{whoName}'
                WHERE  DocNo = '{docNo}' "
                ,
                $@"
                UPDATE  SHOP_MNRG
                SET     [Bill_TypeCNRecive] = '1',[Bill_TypeCNDate] = convert(varchar,getdate(),23),
                        [Bill_TypeCNTime] = convert(varchar,getdate(),24),[Bill_TypeCNWhoID] = '{whoId}',Bill_TypeCNWhoName = '{whoName}'
                WHERE  DocNo = '{docNo}' "
                ,
                $@"
                UPDATE  SHOP_MNRH_HD
                SET     Bill_TypeCNStatus = '1',[Bill_TypeCNRecive] = '1',[Bill_TypeCNDate] = convert(varchar,getdate(),23),
                        [Bill_TypeCNTime] = convert(varchar,getdate(),24),[Bill_TypeCNWhoID] = '{whoId}',Bill_TypeCNWhoName = '{whoName}'
                WHERE  DocNo = '{docNo}' "
            };

            return InsertSql(sql, conShop24Hrs, "CNReciveBox_UpdateRecive");
        }

        //ค้นหา [MNPC_CheckBoxDiff]
        [HttpGet, Route("CNReciveBox_CheckBox")]
        public GlobalClass<CNReciveBoxTagSendDATA> CNReciveBox_CheckBox(string shipDoc)
        {
            return SelectDataSend<CNReciveBoxTagSendDATA>($@" SupcSupport_CN_RECIVEBOX '1','{shipDoc}' ", Methods.conShop24Hrs, "CNReciveBox_CheckBox");
        }


        //ค้นหาจำนวนลังใน SH
        [HttpGet, Route("MNPC_CountBoxSH")]
        public GlobalClass<CNReciveBoxCountDATA> CNReciveBox_CountBox(string shID)
        {
            string sql = $@"
                 SELECT	ShipID,ShipCarID,ShipBranch,ShipBranchName,ShipDriverID,
		                ISNULL(ISNULL(TagSend,0)+ISNULL(TagRecive,0),0) as TagSend,
		                ISNULL(TagRecive,0) as TagRecive,ISNULL(ISNULL(TagSend,0)+ISNULL(TagRecive,0)-ISNULL(TagRecive,0),0) as TagDiff 
                FROM	SHOP_SHIPDOC WITH (NOLOCK) 
		                    LEFT OUTER JOIN  
			                    (SELECT	TagShipID,COUNT(TagNumber) AS TagSend 
			                    from 	SHOP_MNPC_TAG WITH (NOLOCK)  
			                    WHERE	TagShipID IN ( {shID} ) 
			                    AND TagReciveSta = 0 
			                    GROUP BY TagShipID)TmpSend ON SHOP_ShipDOC.ShipID = TmpSend.TagShipID 
		                    LEFT OUTER JOIN  
			                    (SELECT	TagShipID,COUNT(TagNumber) AS TagRecive 
			                    from 	SHOP_MNPC_TAG WITH (NOLOCK) 
			                    WHERE	TagShipID IN ( {shID} ) 
			                    AND TagReciveSta = 1 
			                    GROUP BY TagShipID)TmpRecive  ON SHOP_ShipDOC.ShipID = TmpRecive.TagShipID 
                WHERE	ShipID IN ( {shID} ) 
                ORDER BY ShipID,ShipCarID,ShipDriverID,ShipBranch,ShipBranchName
            ";
            return SelectDataSend<CNReciveBoxCountDATA>(sql, Methods.conShop24Hrs, "CNReciveBox_CountBox");
        }

        //บันทึกการรับลังทั้งหมด [MNPC_ReciveApv]
        [HttpPost, Route("CNReciveBox_ReciveBox")]
        public ResponseDefault CNReciveBox_ReciveBox([FromBody] ReciveSaveRequest data, string whoID)
        {
            ResponseDefault valuesDocno = new ResponseDefault();
            string maxDocno = GetMaxINVOICEIDString("MNSR", "-", "MNSR", "1");
            //HD RECIVE
            List<string> sql = new List<string>
            {
                $@"
                INSERT INTO SHOP_SHIPRECIVEHD
                            (ShipID,ShipWho) 
                VALUES      ('{maxDocno}','{whoID}')"
            };
            //DT RECIVE
            int lineNum = 1;
            foreach (var item in data.DT)
            {
                sql.Add($@"
                INSERT INTO SHOP_SHIPRECIVEDT
                            (ShipID,SeqNo,ShipReciveShipID,ShipCountBoxAll,ShipCountBoxRecive,ShipCountBoxDiff) 
                VALUES      ('{maxDocno}','{lineNum}','{item.ShipReciveShipID}','{item.ShipCountBoxAll}','{item.ShipCountBoxRecive}','{item.ShipCountBoxDiff}') ");
                lineNum++;
            };
            //UPDATE TagNumber ใส่เลขที่การรับ
            foreach (var tag in data.dtTag)
            {
                sql.Add($@"
                UPDATE  SHOP_MNPC_TAG
                SET     TagReciveID = '{maxDocno}'
                WHERE   TagNumber = '{tag.TagNumber}' ");
            };

            ResponseDefault saveSta = InsertSql(sql, conShop24Hrs, "CNReciveBox_ReciveBox");
            if (saveSta.CODE == 200)
            {
                valuesDocno.MESSAGE = maxDocno;
                return valuesDocno;
            }
            else
            {
                return saveSta;
            }
        }
    }
}