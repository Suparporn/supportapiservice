﻿using ApiService.Models.TableModel.Support;
using SupportApiService.Models;
using System.Web.Http;
using static SupportApiService.Models.Methods;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/Settings")]
    public class SettingsController : ApiController
    {
        [HttpPost, Route("PostEditToolsUsing")]
        public ResponseDefault PostEditToolsUsing([FromBody] ConfigReturn data)
        {
            string sql;
            string[] splituser = data.modifiedUser.Split('/');

            if (!data.log.Equals("-1"))
            {
                //Edit
                sql =
                $@"
                        UPDATE [SHOP24HRS].[dbo].[SHOP_BRANCH_PERMISSION]
                        SET	[COM_NAME] = '{data.Comname}'
                            ,[DEPT_ID] = '{data.Deptid}'
                            ,[PRICEGROUP] = '{data.Pricegroup}'
                            ,[CHANNEL] = '{data.Warehouse}'
                            ,[VERSIONUPD] = '{data.Versionupd}'
                            ,[DATEUPD] = GETDATE()
                            ,[WHOIDUPD] = '{splituser[0]}'
                            ,[WHONAMEUPD] = '{splituser[1]}'
                        WHERE [COM_NAME] = '{data.Comname}'
		                        AND [SHOW_ID] = 'A04'
                ";
            }
            else
            {
                //Add
                sql =
                $@"
                        INSERT INTO	[SHOP24HRS].[dbo].[SHOP_BRANCH_PERMISSION] ([COM_NAME]
														                        ,[COM_ALLBRANCH]
														                        ,[DEPT_ID]
														                        ,[SHOW_ID]
														                        ,[PRICEGROUP]
														                        ,[CHANNEL]
														                        ,[WHOINS]
														                        ,[VERSIONUPD])
                        VALUES ('{data.Comname}', '0', '{data.Deptid}', 'A04',
                                '{data.Pricegroup}', '{data.Warehouse}', '{splituser[0]}',
                                '{data.Versionupd}')
                ";
            }

            return InsertSql(sql, conShop24Hrs, "PostEditToolsUsing");
        }

        [HttpGet, Route("DeleteToolsUsing")]
        public ResponseDefault DeleteToolsUsing(string Comname)
        {
            string sql =
            $@"
                    DELETE FROM [SHOP24HRS].[dbo].[SHOP_BRANCH_PERMISSION]
                    WHERE [COM_NAME] = '{Comname}'
                    AND [SHOW_ID] = 'A04'
            ";

            return InsertSql(sql, conShop24Hrs, "DeleteToolsUsing");
        }

        [HttpPost, Route("PostUpgradeVerAll")]
        public ResponseDefault PostUpgradeVerAll(string Deptid, string Versionupd, string ModifiedUser)
        {
            string[] splituser = ModifiedUser.Split('/');

            string sql =
            $@"
                    UPDATE [SHOP24HRS].[dbo].[SHOP_BRANCH_PERMISSION]
                    SET	[VERSIONUPD] = '{Versionupd}'
                        ,[DATEUPD] = GETDATE()
                        ,[WHOIDUPD] = '{splituser[0]}'
                        ,[WHONAMEUPD] = '{splituser[1]}'
                    WHERE [DEPT_ID] = '{Deptid}'
		                  AND [SHOW_ID] = 'A04'
            ";

            return InsertSql(sql, conShop24Hrs, "PostUpgradeVerAll");
        }
    }
}
