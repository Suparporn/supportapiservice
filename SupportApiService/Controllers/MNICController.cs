﻿using SupportApiService.Models;
using System.Collections.Generic;
using System.Web.Http;
using static SupportApiService.Models.Methods;
using SupportApiService.Models.MNIC;
using System.Data;
using System;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/MNIC")]
    public class MNICController : ApiController
    {
        //test ok สำหรับ Select ทั้งหมด -- ยกเว้นเกี่ยวกับ Insert

        //'ค้นหารายละเอียดบิล [MNIC_FindMNICDetail]/[MNIC_FindMNICDetailAX]
        [HttpGet, Route("MNIC_FindDetail")]
        public GlobalClass<MNICDetailData> MNIC_FindDetail(string docno)
        {
            return SelectDataSend<MNICDetailData>($@"[SupcSupport_MNIC_FindData] '1', '{docno}', ''", conShop24Hrs, "MNIC_FindDetail");
        }

        //'ค้นหาเหตุผลการคืน [MNIC_FindReason]
        [HttpGet, Route("MNIC_FindReason")]
        public GlobalClass<MNICReason> MNIC_FindReason()
        {
            return SelectDataSend<MNICReason>($@"[SupcSupport_MNIC_FindData] '2', '', ''", conShop24Hrs, "MNIC_FindReason");
        }

        //'ค้นหา MNIC ที่ไม่ได้อนุมัติ SHOP24HRS [MNIC_FindMNICStaPrcDoc]
        [HttpGet, Route("MNIC_FindBillNotApv")]
        public GlobalClass<MNICNotApv> MNIC_FindBillNotApv(string dptId)
        {
            return SelectDataSend<MNICNotApv>($@"[SupcSupport_MNIC_FindData] '3', '', '{dptId}'", conShop24Hrs, "MNIC_FindBillNotApv");
        }

        //'ยกเลิกบิล [MNIC_Cancle]
        [HttpPost, Route("MNIC_BillCancel")]
        public ResponseDefault MNIC_BillCancel(string docNo, string whoId)
        {
            string sqlUp = $@"
                UPDATE  SHOP_MNIC_HD
                SET     MNICStaDoc='3', MNICStaPrcDoc = '0',MNICDateUp = convert(varchar,getdate(),25),MNICWhoUp = '{whoId}' 
                WHERE   MNICDocNo = '{docNo}'
            ";
            return InsertSql(sqlUp, conShop24Hrs, "MNIC_BillCancel");
        }

        //'แก้ไขจำนวน [MNIC_EditQty]/[MNIC_EditQty]
        [HttpPost, Route("MNIC_EditQty")]
        public ResponseDefault MNIC_EditQty(string docNo, string whoId, int iRows, double qty,string reasonID, string reasonName)
        {
            string SetMNICStaDt = ""; //ในกรณีที่ ลบจำนวนหรือจำนวนเท่ากับ0 จะต้องปรับฟิลด์นี้ด้วย
            if (qty == 0)
            {
                SetMNICStaDt = $@" ,MNICStaDt = '0' ";
            }
            string sqlUp = $@"
                UPDATE  SHOP_MNIC_DT
                SET     MNICQty = '{qty}',MNICDateUp = convert(varchar,getdate(),25),MNICWhoUp = '{whoId}',
                        MNICReson = COALESCE(NULLIF('{reasonID}', ''), MNICReson), MNICRemark = COALESCE(NULLIF('{reasonName}', ''), MNICRemark)
                        {SetMNICStaDt}
                WHERE   MNICDocNo = '{docNo}' AND MNICSeqNo = '{iRows}'
            ";
            return InsertSql(sqlUp, conShop24Hrs, "MNIC_EditQty");
        }

        //บันทึกบิล HD + DT //เมื่อบันทึกเรียบร้อย จะส่งค่า docno กลับไปแสดงผลหน้าจอโปรแกรม [MNIC_SaveHD]
        [HttpPost, Route("MNIC_BillSaveHD")]
        public ResponseDefault MNIC_BillSaveHD([FromBody] MNICSaveRequest data)
        {
            ResponseDefault valuesDocno = new ResponseDefault();
            string maxDocno = GetMaxINVOICEIDString("MNIC", "-", "MNIC", "1");
            List<string> sql = new List<string>
            {
                $@"
                INSERT INTO SHOP_MNIC_HD
                            (MNICDocNo,MNICInvent,MNICSPC,MNICDptWho, MNICWhoIn) 
                VALUES      ('{maxDocno}','{data.HD.MNICInvent}','{data.HD.MNICSPC}','{data.HD.MNICDptWho}','{data.HD.MNICWhoIn}')
            ",

                $@"
                INSERT INTO SHOP_MNIC_DT
                            (MNICDocNo,MNICSeqNo,MNICItemID,MNICDimid,MNICBarcode,
                            MNICName,MNICQty,MNICPrice,
                            MNICUnitID,MNICFactor,MNICVender,MNICVenderName,
                            MNICReson,MNICRemark,MNICWhoIn)
                VALUES      ('{maxDocno}','{data.DT.MNICSeqNo}','{data.DT.MNICItemID}','{data.DT.MNICDimid}','{data.DT.MNICBarcode}',
                            '{data.DT.MNICName.Replace("'"," ")}','{data.DT.MNICQty}','{data.DT.MNICPrice}',
                            '{data.DT.MNICUnitID}','{data.DT.MNICFactor}','{data.DT.MNICVender}','{data.DT.MNICVenderName}',
                            '{data.DT.MNICReson}','{data.DT.MNICRemark}','{data.DT.MNICWhoIn}')
            "
            };
            ResponseDefault saveSta = InsertSql(sql, conShop24Hrs, "MNIC_BillSaveHD");
            if (saveSta.CODE == 200)
            {
                valuesDocno.MESSAGE = maxDocno;
                return valuesDocno;
            }
            else
            {
                return saveSta;
            }
        }

        //บันทึกบิล DT [MNIC_SaveDT]
        [HttpPost, Route("MNIC_BillSaveDT")]
        public ResponseDefault MNIC_BillSaveDT(string docno, [FromBody] MNICDTSaveRequest data)
        {
            List<string> sql = new List<string>
            {
                $@"
                INSERT INTO SHOP_MNIC_DT
                            (MNICDocNo,MNICSeqNo,MNICItemID,MNICDimid,MNICBarcode,
                            MNICName,MNICQty,MNICPrice,
                            MNICUnitID,MNICFactor,MNICVender,MNICVenderName,
                            MNICReson,MNICRemark,MNICWhoIn)
                VALUES      ('{docno}','{data.MNICSeqNo}','{data.MNICItemID}','{data.MNICDimid}','{data.MNICBarcode}',
                            '{data.MNICName.Replace("'","")}','{data.MNICQty}','{data.MNICPrice}',
                            '{data.MNICUnitID}','{data.MNICFactor}','{data.MNICVender}','{data.MNICVenderName}',
                            '{data.MNICReson}','{data.MNICRemark}','{data.MNICWhoIn}')
            "
            };
            return InsertSql(sql, conShop24Hrs, "MNIC_BillSaveDT"); ;
        }

        //'ยืนยันเอกสารและส่งข้อมูลเข้า AX [MNIC_SendAXNew]/[MNIC_BillApv]
        [HttpPost, Route("MNIC_BillApvSendAX")]
        public ResponseDefault MNIC_BillApvSendAX(string docNo, string whoId)
        {
            //TRANSFERSTATUS 0 สร้างแล้ว 1 จัดส่งแล้ว 3 รับสินค้าแล้ว
            DataTable data = SelectDataTable($@"[SupcSupport_MNIC_FindData] '0', '{docNo}', ''", conShop24Hrs);
            List<string> sqlAX = new List<string>
            {
                $@"
                INSERT INTO SPC_POSTOTABLE20 
                            (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                            INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, 
                            SHIPDATE, VOUCHERID) 
                VALUES      ('98','{whoId}','0','SPC','1','1','1',
                            'RETURN','{data.Rows[0]["MNICInvent"]}','{docNo}',
                            convert(varchar,getdate(),23),'{docNo}')
            "
            };

            int iRows = 1;
            foreach (DataRow item in data.Rows)
            {
                if (Convert.ToDouble(item["MNICQty"]) > 0)
                {
                    sqlAX.Add($@"
                        INSERT INTO SPC_POSTOLINE20 
                                    (RECID,REFBOXTRANS,DATAAREAID,VOUCHERID,SHIPDATE,LINENUM,QTY,
                                    ITEMID,NAME,SALESUNIT,ITEMBARCODE,
                                    INVENTDIMID,INVENTTRANSID,RECVERSION)
                        VALUES      ('1','0','SPC','{docNo}',convert(varchar,getdate(),23),'{iRows}','{item["MNICQty"]}',
                                    '{item["MNICItemID"]}','{item["MNICName"]}','{item["MNICUnitID"]}','{item["MNICBarcode"]}',
                                    '{item["MNICDimid"]}','{docNo + "-" + item["MNICSeqNo"]}','1')
                    ");
                    iRows++;
                }
            }

            List<string> sql24 = new List<string>
            {
                $@"
                UPDATE  SHOP_MNIC_HD
                SET     MNICStaDoc='1',MNICStaPrcDoc = '1',MNICDateUp = convert(varchar,getdate(),25),MNICWhoUp = '{whoId}'
                WHERE   MNICDocNo  = '{docNo}'
                "
            };

            return InsertSqlTwoServer(sql24, conShop24Hrs, sqlAX, con708AX, "MNIC_BillApvSendAX");
        }

    }
}
