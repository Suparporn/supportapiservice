﻿using SupportApiService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/Image")]
    public class ImageController : ApiController
    {
        [HttpGet, Route("Image_FindImage")]
        public int Image_FindImage(string billtype, string billdate, string billid, string title, string branchid)
        {
            string PathI = "";

            switch (billtype)
            {
                case "IMNCM":
                    PathI = Methods.imagepath_ClaimInSRV + billdate + @"\" + branchid;
                    break;
                case "IBill":
                    PathI = Methods.imagepath_BillInSRV + billdate + @"\" + branchid;
                    break;
                case "IDMCN":
                    PathI = Methods.imagepath_IDMInSRV + billdate + @"\" + branchid;
                    break;
            }
            
            DirectoryInfo DirInfo = new DirectoryInfo(PathI);
            if (!Directory.Exists(PathI)) return 0;
            FileInfo[] Files = DirInfo.GetFiles("*" + billid + "*" + title + ".JPG", SearchOption.AllDirectories);
            return Files.Length;
        }

        //[HttpGet, Route("Image_FindImageClaimInSRV")]
        //public int Image_FindImageClaimInSRV(string billdate, string billid, string title, string branchid)
        //{
        //    string PathI = Methods.imagepath_ClaimInRRV + billdate + @"\" + branchid;

        //    DirectoryInfo DirInfo = new DirectoryInfo(PathI);
        //    if (!Directory.Exists(PathI)) return 0;
        //    FileInfo[] Files = DirInfo.GetFiles("*" + billid + "*" + title + ".JPG", SearchOption.AllDirectories);
        //    return Files.Length;
        //}

        //[HttpGet, Route("Image_FindImageBillInSRV")]
        //public int Image_FindImageBillInSRV(string billdate, string billid, string title, string branchid)
        //{
        //    string PathI = Methods.imagepath_BillInSRV + billdate + @"\" + branchid;

        //    DirectoryInfo DirInfo = new DirectoryInfo(PathI);
        //    if (!Directory.Exists(PathI)) return 0;
        //    FileInfo[] Files = DirInfo.GetFiles("*" + billid + "*" + title + ".JPG", SearchOption.AllDirectories);
        //    return Files.Length;
        //}

        [HttpPost, Route("Image_UploadImage")]
        public async Task<ResponseDefault> Image_UploadImage(string billtype, string billdate, string branchId)
        {
            ResponseDefault response = new ResponseDefault();
            try
            {
                string root = $@"";

                switch (billtype)
                {
                    case "IMNCM":
                        root = $@"{Methods.imagepath_ClaimInSRV}{billdate}\{branchId}\";
                        break;
                    case "IBill":
                        root = $@"{Methods.imagepath_BillInSRV}{billdate}\{branchId}\";
                        break;
                    case "IDMCN":
                        root = $@"{Methods.imagepath_IDMInSRV}{billdate}\{branchId}\";
                        break;
                }

                if (!Directory.Exists(root)) Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);

                await Request.Content.ReadAsMultipartAsync(provider);

                foreach (MultipartFileData file in provider.FileData)
                {
                    var name = file.Headers.ContentDisposition.FileName;
                    name = name.Trim('"');
                    var localFileName = file.LocalFileName;
                    var filePath = Path.Combine(root, name);

                    if (!File.Exists(filePath))
                    {
                        File.Move(localFileName, filePath);
                    }
                }

                response.MESSAGE = $@"บันทึกรูปภาพสำเร็จ";
            }
            catch (Exception ex)
            {
                response.CODE = 417;
                response.MESSAGE = ex.ToString();
            }
            return response;
        }

        //[HttpGet, Route("findImageAllDay")]
        //public int FindImageAllDay(string date, string deptID, string deptName, string branchId)
        //{
        //    string PathI = Methods.Path_Image_AllDay + deptName + @"\" + deptID + @"\" + date + "" + @"\" + branchId;

        //    DirectoryInfo DirInfo = new DirectoryInfo(PathI);

        //    if (!System.IO.Directory.Exists(PathI)) return 0;

        //    FileInfo[] Files = DirInfo.GetFiles("*_" + deptID + ".JPG", SearchOption.AllDirectories);
        //    return Files.Length;
        //}

        //[HttpPost, Route("uploadImage")]
        //public async Task<ResponseDefault> UploadImage(string typeImage, string date, string branchId)
        //{
        //    ResponseDefault response = new ResponseDefault();
        //    try
        //    {
        //        //string description = "";
        //        string root = $@"";
        //        if (typeImage == "All")
        //        {
        //            root = $@"{Methods.Path_Image_AllDay}{deptName}\{deptID}\{dateImage}\{branchId}\";
        //        }
        //        else
        //        {
        //            response.STATUS = response.STATUS = Methods.EXCEPTION;
        //            response.MESSAGE = " ไม่พบประเภท รูปภาพ (" + typeImage + ") ในระบบ ติดต่อผู้ดูแลระบบ";
        //            return response;
        //        }
        //        if (!Directory.Exists(root))
        //        {
        //            Directory.CreateDirectory(root);
        //        }
        //        var provider = new MultipartFormDataStreamProvider(root);

        //        await Request.Content.ReadAsMultipartAsync(provider);


        //        if (!Directory.Exists(root))
        //        {
        //            Directory.CreateDirectory(root);
        //        }
        //        foreach (MultipartFileData file in provider.FileData)
        //        {
        //            var name = file.Headers.ContentDisposition.FileName;
        //            name = name.Trim('"');
        //            var localFileName = file.LocalFileName;
        //            var filePath = Path.Combine(root, name);

        //            if (!File.Exists(filePath))
        //            {
        //                File.Move(localFileName, filePath);
        //            }
        //        }


        //        response.STATUS = Methods.SUCCESS;
        //        response.MESSAGE = $@"บันทึกรูปภาพสำเร็จ";
        //    }
        //    catch (Exception ex)
        //    {
        //        response.STATUS = Methods.EXCEPTION;
        //        response.MESSAGE = ex.ToString();
        //    }
        //    return response;
        //}

        //[HttpGet, Route("Invent_FindAll")]
        //public GlobalClass<InventData> Invent_FindAll()
        //{
        //    return SelectDataSend<InventData>($@"  SupcSupport_Invent '0','' ", conShop24Hrs, "Invent_FindAll");
        //}
    }
}
