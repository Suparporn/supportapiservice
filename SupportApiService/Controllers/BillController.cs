﻿using System.Web.Http;
using static SupportApiService.Models.Methods;
using SupportApiService.Models.Bill;
using SupportApiService.Models;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/Bill")]
    public class BillController : ApiController
    {
        //ค้นหา [MNCM_FindBill]/[MNRF_FindBill]/[IDM_FindBill]/[MNRB_FindBill]
        [HttpGet, Route("Bill_Detail")]
        public GlobalClass<BillData> Bill_Detail(string typeBill, string docNo)
        {
            // typeBill = MNCM/IDM/MNRB/MNRG/MNIO
            return SelectDataSend<BillData>($@" PDA_BillOut_FindData '{typeBill}','{docNo}' ", Methods.conShop24Hrs, "Bill_Detail");
        }
    }
}