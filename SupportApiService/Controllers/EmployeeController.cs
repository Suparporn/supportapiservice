﻿using static SupportApiService.Models.Methods;
using SupportApiService.Models.Employee;
using System.Web.Http;
using SupportApiService.Models.General;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/Employee")]
    public class EmployeeController : ApiController
    {
        //ถอดรหัสผ่าน [Emp_DecodePassword]
        [HttpGet, Route("Emp_DecodePassword")] 
        public string Employee_DecodePassword(string empPassword)
        {
            string res = "";
            for (int i = 0; i <= empPassword.Length - 1; i++)
            {
                if (i % 2 == 1)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(empPassword, i) + 4);
                else if (i % 2 == 0)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(empPassword, i) - 4);
            }
            return res;
        }

        //เข้ารหัสผ่าน [Emp_EncodePassword]
        [HttpPost, Route("Emp_EncodePassword")]
        public string Employee_EncodePassword(string empPassword)
        {
            string res = "";
            for (int i = 0; i <= empPassword.Length - 1; i++)
            {
                if (i % 2 == 1)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(empPassword, i) - 4);
                else if (i % 2 == 0)
                    res += char.ConvertFromUtf32(char.ConvertToUtf32(empPassword, i) + 4);
            }
            return res;
        }

        //รายละเอียดพนักงานทั้งหมด โดยที่เช็คข้อมูลมใน SHOP_EMPLOYEE ด้วย [Emp_FindUSer]
        [HttpGet, Route("Emp_FindName")]
        public GlobalClass<EmpLoginData> Employee_FindName(string altnum)
        {
            return SelectDataSend<EmpLoginData>(GeneralClass.Employee_GetDetailShopByAltnum(altnum), conShop24Hrs, "Emp_FindName");
        }

    }
}