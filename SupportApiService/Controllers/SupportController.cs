﻿using ApiService.ExternalDatabase;
using ApiService.Models;
using ApiService.Models.HistoryR.Support;
using ApiService.Models.TableModel.Support;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/Support")]

    public class SupportController : ApiController
    {
        [Route("GetEmployee")]
        [HttpPost]
        public EmplResponse GetEmployee(EmplLogin Data)
        {
            IEnumerable<EmplReturn> Item = new List<EmplReturn>();
            try
            {
                Item = Support_ClassConnectDb.GetEmployee(Data);
                if (Item.Count() > 0)
                {
                    string decodepass = Support_ClassConnectDb.DecodePassword(Item.ElementAt(0).Emplpass);
                    Item.ElementAt(0).Emplpass = decodepass;
                }
            }
            catch (Exception ex)
            {
                return new EmplResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error โหลดข้อมูล Employee",
                    ErrorMassage = "GetEmployee : " + ex.Message,
                    Data = Item
                };
            }
            return new EmplResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = "โหลดข้อมูล Employee สำเร็จ",
                Data = Item
            };
        }

        [Route("GetConfig")]
        [HttpPost]
        public ConfigResponse GetConfig(Config Data)
        {
            IEnumerable<ConfigReturn> Item = new List<ConfigReturn>();
            try
            {
                Item = Support_ClassConnectDb.GetConfig(Data);
            }
            catch (Exception ex)
            {
                return new ConfigResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error โหลดข้อมูล Config",
                    ErrorMassage = "GetConfig : " + ex.Message,
                    Data = Item
                };
            }
            return new ConfigResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = "โหลดข้อมูล Config สำเร็จ",
                Data = Item
            };
        }

        [Route("GetAllBarcodeData")]
        [HttpPost]
        public AllBarcodeDataResponse GetAllBarcodeData(AllBarcodeData Data)
        {
            IEnumerable<AllBarcodeDataReturn> Item = new List<AllBarcodeDataReturn>();
            try
            {
                Item = Support_ClassConnectDb.GetAllBarcodeData(Data);
            }
            catch (Exception ex)
            {
                return new AllBarcodeDataResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error โหลดข้อมูล All Barcode Data",
                    ErrorMassage = "GetAllBarcodeData : " + ex.Message,
                    Data = Item
                };
            }
            return new AllBarcodeDataResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = "โหลดข้อมูล All Barcode Data สำเร็จ",
                Data = Item
            };
        }

        [Route("GetBarcodeData")]
        [HttpPost]
        public BarcodeDataResponse GetBarcodeData(BarcodeData Data)
        {
            IEnumerable<BarcodeDataReturn> Item = new List<BarcodeDataReturn>();
            try
            {
                Item = Support_ClassConnectDb.GetBarcodeData(Data);
            }
            catch (Exception ex)
            {
                return new BarcodeDataResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error โหลดข้อมูล Barcode Data",
                    ErrorMassage = "GetBarcodeData : " + ex.Message,
                    Data = Item
                };
            }
            return new BarcodeDataResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = "โหลดข้อมูล Barcode Data สำเร็จ",
                Data = Item
            };
        }

        [Route("GetBarcodeStock")]
        [HttpPost]
        public BarcodeStockResponse GetBarcodeStock(BarcodeStock Data)
        {
            IEnumerable<BarcodeStockReturn> Item = new List<BarcodeStockReturn>();
            try
            {
                Item = Support_ClassConnectDb.GetBarcodeStock(Data);
            }
            catch (Exception ex)
            {
                return new BarcodeStockResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error โหลดข้อมูล Barcode Stock",
                    ErrorMassage = "GetBarcodeStock : " + ex.Message,
                    Data = Item
                };
            }
            return new BarcodeStockResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = "โหลดข้อมูล Barcode Stock สำเร็จ",
                Data = Item
            };
        }

        [Route("SaveLogUsed")]
        [HttpPost]
        public LogUsedResponse SaveLogUsed(LogUsed Data)
        {
            try
            {
                Support_ClassConnectDb.SaveLogUsed(Data);
                return new LogUsedResponse
                {
                    Code = (int)HttpStatusCode.OK,
                    Message = "บันทึกการใช้งานสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                return new LogUsedResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error บันทึกการใช้งาน",
                    ErrorMassage = ("SaveLogUsed : " + ex.Message).Replace(System.Environment.NewLine, string.Empty)
                };
            }
        }

        [Route("SaveChangeSign")]
        [HttpPost]
        public ChangeSignResponse SaveChangeSign(ChangeSign Data)
        {
            try
            {
                Support_ClassConnectDb.SaveChangeSign(Data);
                return new ChangeSignResponse
                {
                    Code = (int)HttpStatusCode.OK,
                    Message = "บันทึกการเปลี่ยนป้ายสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                return new ChangeSignResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error บันทึกการเปลี่ยนป้าย",
                    ErrorMassage = ("SaveChangeSign : " + ex.Message).Replace(System.Environment.NewLine, string.Empty)
                };
            }
        }

        [Route("GetInvent")]
        [HttpPost]
        public InventResponse GetInvent(Invent Data)
        {
            IEnumerable<InventReturn> Item = new List<InventReturn>();
            try
            {
                Item = Support_ClassConnectDb.GetInvent(Data);
            }
            catch (Exception ex)
            {
                return new InventResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error โหลดข้อมูลคลังสินค้าทั้งหมด",
                    ErrorMassage = "GetInvent : " + ex.Message,
                    Data = Item
                };
            }
            return new InventResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = "โหลดข้อมูล คลังสินค้าทั้งหมด สำเร็จ",
                Data = Item
            };
        }

        [Route("GetMntfMaxIdDocno")]
        [HttpPost]
        public MntfMaxIdDocnoResponse GetMntfMaxIdDocno(MntfDocno Data)
        {
            IEnumerable<MntfMaxIdDocnoReturn> Item = new List<MntfMaxIdDocnoReturn>();
            try
            {
                Item = Support_ClassConnectDb.GetMntfMaxIdDocno(Data);
            }
            catch (Exception ex)
            {
                return new MntfMaxIdDocnoResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error ค้นหาเลขที่เอกสารสูงสุดใน MNTF",
                    ErrorMassage = "GetMntfMaxIdDocno : " + ex.Message,
                    Data = Item
                };
            }
            return new MntfMaxIdDocnoResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = "ค้นหาเลขที่เอกสารสูงสุดใน MNTF สำเร็จ",
                Data = Item
            };
        }

        [Route("SaveMntfData")]
        [HttpPost]
        public MntfDataResponse SaveMntfData(MntfData Data)
        {
            try
            {
                if (String.IsNullOrEmpty(Data.Datasavehd.Transferid))
                {
                    Data.Datasavehd.Transferid = "PDA";
                }

                Support_ClassConnectDb.SaveMntfData(Data);
                return new MntfDataResponse
                {
                    Code = (int)HttpStatusCode.OK,
                    Message = "บันทึก MntfData สำเร็จ"
                };
            }
            catch (Exception ex)
            {
                return new MntfDataResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error บันทึก MntfData",
                    ErrorMassage = ("SaveMntfData [" + Data.Tag + "] : " + ex.Message).Replace(System.Environment.NewLine, string.Empty)
                };
            }
        }

        [Route("CancelMntfBill")]
        [HttpPost]
        public CancelMntfResponse CancelMntfBill(CancelMntf Data)
        {
            try
            {
                Support_ClassConnectDb.CancelMntfBill(Data);
                return new CancelMntfResponse
                {
                    Code = (int)HttpStatusCode.OK,
                    Message = "ยกเลิก MntfBill สำเร็จ"
                };
            }
            catch (Exception ex)
            {
                return new CancelMntfResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error ยกเลิก MntfBill",
                    ErrorMassage = ("CancelMntfBill : " + ex.Message).Replace(System.Environment.NewLine, string.Empty)
                };
            }
        }

        [Route("MntfFinddata")]
        [HttpPost]
        public MntfFindResponse MntfFinddata(MntfFind Data)
        {
            IEnumerable<MntfFindReturn> Item = new List<MntfFindReturn>();
            try
            {
                Item = Support_ClassConnectDb.MntfFinddata(Data);
            }
            catch (Exception ex)
            {
                return new MntfFindResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error MntfFinddata",
                    ErrorMassage = "MntfFinddata : " + ex.Message,
                    Data = Item
                };
            }
            return new MntfFindResponse
            {
                Code = (int)HttpStatusCode.OK,
                Message = "MntfFinddata สำเร็จ",
                Data = Item
            };
        }

        [Route("ApvBill")]
        [HttpPost]
        public ApvBillResponse ApvBill(ApvBill Data)
        {
            IEnumerable<MntfFindReturn> Item = new List<MntfFindReturn>();
            try
            {
                MntfFind mntffind = new MntfFind();
                mntffind.pCase = "6";
                mntffind.pDocno = Data.pDocno;

                Item = Support_ClassConnectDb.MntfFinddata(mntffind);
                Support_ClassConnectDb.ApvBill(Data, Item);
                return new ApvBillResponse
                {
                    Code = (int)HttpStatusCode.OK,
                    Message = "ปล่อยบิลสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                return new ApvBillResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error ปล่อยบิล",
                    ErrorMassage = ("ApvBill : " + ex.Message).Replace(System.Environment.NewLine, string.Empty)
                };
            }
        }

        [Route("MNTF_EditQtyLineBill")]
        [HttpPost]
        public DefaultResponse MNTF_EditQtyLineBill(MntfEditQty Data)
        {
            try
            {
                Support_ClassConnectDb.MNTF_EditQtyLineBill(Data); 
                return new DefaultResponse
                {
                    Code = (int)HttpStatusCode.OK,
                    Message = "แก้ไขจำนวนขอเบิกสินค้าสำเร็จ"
                };
            }
            catch (Exception ex)
            {
                return new DefaultResponse
                {
                    Code = (int)HttpStatusCode.ExpectationFailed,
                    Message = "Error แก้ไขจำนวนขอเบิกสินค้า",
                    ErrorMassage = ("MNTF_EditQtyLineBill : " + ex.Message).Replace(System.Environment.NewLine, string.Empty)
                };
            }
        }
    }
}
