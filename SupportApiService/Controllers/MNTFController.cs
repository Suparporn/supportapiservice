﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;
using SupportApiService.Models;
using SupportApiService.Models.MNTF;
using static SupportApiService.Models.Methods;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/MNTF")]
    public class MNTFController : ApiController
    {

        //test ok สำหรับ Select ทั้งหมด -- ยกเว้นเกี่ยวกับ Insert

        //'ค้นหาแผนกที่ระบุว่าต้องจัดบิลเองหรือไม่ (เพื่อให้ปล่อยบิลลง Shop24Hrs. ได้)
        [HttpGet, Route("MNTF_FindDptForPacking")]
        public int MNTF_FindDptForPacking(string dptID)
        {
            string sql = $@"SELECT	*	FROM	SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK)
                            WHERE	DIMENSION = '{dptID}' AND ISACTIVE = '1' ";
            DataTable dt = SelectDataTable(sql, conShop24Hrs);
            return dt.Rows.Count;
        }

        //'บิลค้างปล่อยทั้งหมด [MNTF_FindNotApv]
        [HttpGet, Route("MNTF_FindBillNotApv")]
        public GlobalClass<MNTFNotApvData> MNTF_FindBillNotApv(string typeBill, string dptID)
        {
            return SelectDataSend<MNTFNotApvData>($@" SupcSupport_MNTF_FindData '0','{typeBill}','{dptID}','','','' ", conShop24Hrs, "MNTF_FindBillNotApv");
        }

        //'บิลค้างปล่อย ตามคลัง [MNTF_FindNotApvWH]
        [HttpGet, Route("MNTF_FindBillNotApvByInvent")]
        public GlobalClass<MNTFNotApvData> MNTF_FindBillNotApvByInvent(string typeBill, string dptID, string inventID)
        {
            return SelectDataSend<MNTFNotApvData>($@" SupcSupport_MNTF_FindData '1','{typeBill}','{dptID}','{inventID}','','' ", conShop24Hrs, "MNTF_FindBillNotApvByInvent");
        }

        //'บิลทั้งหมด ย้อนหลัง 5 วัน [MNTF_FindBillLast5]
        [HttpGet, Route("MNTF_FindBillAll")]
        public GlobalClass<MNTFNotApvData> MNTF_FindBillAll(string typeBill, string dptID)
        {
            return SelectDataSend<MNTFNotApvData>($@" SupcSupport_MNTF_FindData '2','{typeBill}','{dptID}','','','' ", conShop24Hrs, "MNTF_FindBillAll");
        }

        //'ค้นหารายละเอียดบิล [MNTF_FindBillDetail]
        [HttpGet, Route("MNTF_FindDetail")]
        public GlobalClass<MNTFDetailData> MNTF_FindDetail(string docno)
        {
            return SelectDataSend<MNTFDetailData>($@" SupcSupport_MNTF_FindData '3','','','','{docno}','' ", conShop24Hrs, "MNTF_FindDetail");
        }

        //บันทึกเอกสาร HD-DT [MNTF_SaveHD]
        [HttpPost, Route("MNTF_BillSaveHD")]
        public ResponseDefault MNTF_BillSaveHD([FromBody] MNTFSaveRequest data)
        {
            ResponseDefault valuesDocno = new ResponseDefault();
            if (String.IsNullOrEmpty(data.DT.MNTFBarcode))
            {
                valuesDocno.CODE = 417;
                valuesDocno.MESSAGE = "ไม่พบบาร์โค้ด";
                return valuesDocno;
            }

            string maxDocno = GetMaxINVOICEIDString("MNTF", "-", "MNTF", "1");
          
            if (data.HD.MNTFDepart.Equals("D044") && data.HD.MNTFRemark.Equals("PDA"))
            {
                data.DT.MNTFRemark = maxDocno + "_" + data.DT.MNTFSeqNo;
            }

            List<string> sql = new List<string>() {
                    $@"
                    INSERT INTO SHOP_MNTF_HD 
                                (MNTFDocNo,MNTFUserCode,MNTFUserCodeName,MNTFTypeOrder,
                                MNTFWHSoure,MNTFWHDestination,MNTFLock,MNTFRemark,
                                MNTFSPCNAME,MNTFDepart,MNTFDepartName )
                    VALUES      ('{maxDocno}','{data.HD.MNTFUserCode}','{data.HD.MNTFUserCodeName}','{data.HD.MNTFTypeOrder}',
                                '{data.HD.MNTFWHSoure}','{data.HD.MNTFWHDestination}','{data.HD.MNTFLock}','{data.HD.MNTFRemark}',
                                '{data.HD.MNTFSPCNAME}','{data.HD.MNTFDepart}','{data.HD.MNTFDepartName}')
                    ",
                    $@"
                    INSERT INTO SHOP_MNTF_DT
                                (MNTFDocNo,MNTFSeqNo,MNTFItemID,MNTFDimid, MNTFBarcode,
                                MNTFName,MNTFQtyOrder,MNTFUnitID,MNTFPrice,MNTFFactor,
                                MNTFWhoIn,MNTFWhoInName,MNTFPurchase,MNTFRemark,MNTFPATHIMAGE )
                    VALUES      ('{maxDocno}','{data.DT.MNTFSeqNo}','{data.DT.MNTFItemID}','{data.DT.MNTFDimid}','{data.DT.MNTFBarcode}',
                                '{data.DT.MNTFName.Replace("'"," ")}','{data.DT.MNTFQtyOrder}','{data.DT.MNTFUnitID}','{data.DT.MNTFPrice}','{data.DT.MNTFFactor}',
                                '{data.HD.MNTFUserCode}','{data.HD.MNTFUserCodeName}','{data.DT.MNTFPurchase}','{data.DT.MNTFRemark}','{data.DT.MNTFPATHIMAGE}')
                    "
            };
            ResponseDefault saveSta = InsertSql(sql, conShop24Hrs, "MNTF_BillSaveHD");
            if (saveSta.CODE == 200)
            {
                valuesDocno.MESSAGE = maxDocno;
                return valuesDocno;
            }
            else
            {
                return saveSta;
            }
        }

        //บันทึกเอกสาร DT [MNTF_SaveDT]
        [HttpPost, Route("MNTF_BillSaveDT")]
        public ResponseDefault MNTF_BillSaveDT(string docNo, string whoIns, string whoInsName, [FromBody] MNTFDTSaveRequest data)
        {
            ResponseDefault valuesDocno = new ResponseDefault();

            if (String.IsNullOrEmpty(data.MNTFBarcode))
            {
                valuesDocno.CODE = 417;
                valuesDocno.MESSAGE = "ไม่พบบาร์โค้ด";
                return valuesDocno;
            }

            string sql = $@"
                    INSERT INTO SHOP_MNTF_DT
                                (MNTFDocNo,MNTFSeqNo,MNTFItemID,MNTFDimid, MNTFBarcode,
                                MNTFName,MNTFQtyOrder,MNTFUnitID,MNTFPrice,MNTFFactor,
                                MNTFWhoIn,MNTFWhoInName,MNTFPurchase,MNTFRemark,MNTFPATHIMAGE )
                    VALUES      ('{docNo}','{data.MNTFSeqNo}','{data.MNTFItemID}','{data.MNTFDimid}','{data.MNTFBarcode}',
                                '{data.MNTFName.Replace("'", " ")}','{data.MNTFQtyOrder}','{data.MNTFUnitID}','{data.MNTFPrice}','{data.MNTFFactor}',
                                '{whoIns}','{whoInsName}','{data.MNTFPurchase}','{data.MNTFRemark}','{data.MNTFPATHIMAGE}')
            ";
            return InsertSql(sql, conShop24Hrs, "MNTF_BillSaveDT");
        }

        //ยกเลิกเอกสาร  [MNTF_CancleBill]
        [HttpPost, Route("MNTF_BillCancel")]
        public ResponseDefault MNTF_BillCancel(string docNo, string whoIns, string whoInsName)
        {
            string sql = $@"
                UPDATE  SHOP_MNTF_HD
                SET     MNTFStaDoc = '0',MNTFDateUp = convert(varchar,getdate(),25),MNTFWhoUp = '{whoIns}',MNTFWhoUpName = '{whoInsName}' 
                WHERE   MNTFDocNo = '{docNo}'
            ";
            return InsertSql(sql, conShop24Hrs, "MNTF_BillCancel");
        }

        //แก้ไขจำนวน  [MNTF_EditQtyLineBill]
        [HttpPost, Route("MNTF_BillEditQty")]
        public ResponseDefault MNTF_BillEditQty(string docNo, int iRows, double qty, string whoIns, string whoInsName)
        {
            string SetMNTFStaDt = $@" ,MNTFStaDT = '1' "; //ในกรณีที่ ลบจำนวนหรือจำนวนเท่ากับ0 จะต้องปรับฟิลด์นี้ด้วย
            if (qty == 0)
            {
                SetMNTFStaDt = $@" ,MNTFStaDT = '0' ";
            }

            string sql = $@"
                UPDATE  SHOP_MNTF_DT
                SET     MNTFQtyOrder = '{qty}',
                        MNTFDateUp = convert(varchar,getdate(),25),MNTFWhoUp = '{whoIns}',MNTFWhoUpName = '{whoInsName}'  {SetMNTFStaDt}
                WHERE   MNTFDocNo = '{docNo}' AND MNTFSeqNo = '{iRows}'
            ";
            return InsertSql(sql, conShop24Hrs, "MNTF_BillEditQty");
        }

        //'ยืนยันเอกสารและส่งข้อมูลเข้า AX [MNTF_ApvBill]/[MNTF_SendToAX]
        [HttpPost, Route("MNTF_BillApvSendAX")]
        public ResponseDefault MNTF_BillApvSendAX(string docNo, string whoId, string whoInsName, string typeUpdateAX) //typeUpdateAX 0 = สร้างแล้ว 1 = จัดส่ง 2 = รับ
        {
            List<string> sql24 = new List<string>
            {
                $@"
                UPDATE  SHOP_MNTF_HD
                SET     MNTFStaApvDoc='1',MNTFStaPrcDoc = '1',MNTFDateApv = convert(varchar,getdate(),25),
                        MNTFWhoApv = '{whoId}',MNTFWhoApvName = '{whoInsName}'
                WHERE   MNTFDocNo  = '{docNo}'
                "
            };

            DataTable data = SelectDataTable($@" SupcSupport_MNTF_FindData '6','','','','{docNo}','' ", conShop24Hrs);
            string TRANSFERID = docNo;
            if (data.Rows[0]["REF"].ToString().Contains("MNTF")) TRANSFERID = data.Rows[0]["REF"].ToString();
            List<string> sqlAX = new List<string>
            {
                $@"
                INSERT INTO SPC_POSTOTABLE20 
                            (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                            INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, 
                            SHIPDATE, VOUCHERID,DLVTERM,REMARKS) 
                VALUES      ('0','{data.Rows[0]["MNTFUserCode"]}','0','SPC','1','1','{typeUpdateAX}',
                            '{data.Rows[0]["WHDestination"]}','{data.Rows[0]["WHSoure"]}','{TRANSFERID}',
                            '{data.Rows[0]["DateIns"]}','{docNo}','{data.Rows[0]["Lock"]}','{data.Rows[0]["RMK"]}')
            "
            };

            int iRows = 1;
            foreach (DataRow item in data.Rows)
            {
                if (Convert.ToDouble(item["QtyOrder"]) > 0)
                {
                    string tranID = item["INVENTTRANSID"].ToString();
                    if (item["INVENTTRANSID"].ToString() == "") tranID = docNo + "-" + item["LineNum"];

                    sqlAX.Add($@"
                        INSERT INTO SPC_POSTOLINE20 
                                    (RECID,REFBOXTRANS,DATAAREAID,VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,
                                    ITEMID,NAME,SALESUNIT,ITEMBARCODE,
                                    INVENTDIMID,INVENTTRANSID,RECVERSION)
                        VALUES      ('1','0','SPC','{docNo}','{item["DateIns"]}','{iRows}','{item["QtyOrder"]}','{item["PRICE"]}',
                                    '{item["ITEMID"]}','{item["SPC_ITEMNAME"]}','{item["UnitID"]}','{item["ITEMBARCODE"]}',
                                    '{item["INVENTDIM"]}','{tranID}','1')
                    ");
                    iRows++;
                }
            }

            return InsertSqlTwoServer(sql24, conShop24Hrs, sqlAX, con708AX, "MNTF_BillApvSendAX");
        }
        //'ยืนยันเอกสารและส่งข้อมูลเข้า 24 [MNTF_ApvBill]/[MNTF_SendToAX]
        [HttpPost, Route("MNTF_BillApvSend24")]
        public ResponseDefault MNTF_BillApvSend24(string docNo, string whoId, string whoInsName)
        {
            List<string> sql24 = new List<string>
            {
                $@"
                UPDATE  SHOP_MNTF_HD
                SET     MNTFStaApvDoc='1',MNTFStaPrcDoc = '1',MNTFDateApv = convert(varchar,getdate(),25),
                        MNTFWhoApv = '{whoId}',MNTFWhoApvName = '{whoInsName}'
                WHERE   MNTFDocNo  = '{docNo}'
                "
            };

            DataTable data = SelectDataTable($@" SupcSupport_MNTF_FindData '10','','','','{docNo}','' ", conShop24Hrs);
            sql24.Add(data.Rows[0]["STRINSERT"].ToString());

            return InsertSql(sql24, conShop24Hrs, "MNTF_BillApvSend24");
        }
        // รับบิลจากการส่งสินค้าจากโกดังมาหน้าร้าน [MNTF_Insert708]
        [HttpGet, Route("MNTF_ReciveBillByRetail")]
        public ResponseDefault MNTF_ReciveBillByRetail(string docNo, string inventID, string whoID)
        {
            string sql = $@"
                INSERT INTO SPC_TRANSFERUPDRECEIVE ( 
                            RECVERSION,RECID,DATAAREAID,VOUCHERID, BOXID, EMPLID,RECEIVEDATE, REMARKS, INVENTLOCATIONIDTO) 
                VALUES      ('1','{docNo.Substring(4, 12)}','SPC','{docNo}','','{whoID}',convert(varchar,getdate(),23),'รับสินค้าทั้งบิล','{inventID}')
            ";
            return InsertSql(sql, con708AX, "MNTF_ReciveBillByRetail");
        }

        //'บิล detail SHOP24HRs สำหรับห้องยา [MNTF_FindBillHDByDocno] Check ใน 24
        [HttpGet, Route("MNTF_FindDetailForRefTransID")]
        public GlobalClass<MNTFRefTransIDData> MNTF_FindDetailForRefTransID(string docno)
        {
            return SelectDataSend<MNTFRefTransIDData>($@" SupcSupport_MNTF_FindData '7','','','','{docno}','' ", conShop24Hrs, "MNTF_FindDetailForRefTransID");
        }

        //'ค้นหารายการอ้างอิง สำหรับห้องยา [MNTF_FindINVENTTRANSID]
        [HttpGet, Route("MNTF_FindINVENTTRANSID")]
        public GlobalClass<MNTFRefData> MNTF_FindINVENTTRANSID(string docno, string barcode)
        {
            return SelectDataSend<MNTFRefData>($@" SupcSupport_MNTF_FindData '8','','','','{docno}','{barcode}' ", conShop24Hrs, "MNTF_FindINVENTTRANSID");
        }

        //'บิล detail SHOP24HRs สำหรับห้องยา [MNTF_FindBillHDByDocno] Check ใน AX
        [HttpGet, Route("MNTF_FindBillRefTransID")]
        public GlobalClass<MNTFRefTransIDData> MNTF_FindBillRefTransID(string docno)
        {
            return SelectDataSend<MNTFRefTransIDData>($@" SupcSupport_MNTF_FindData '9','','','','{docno}','' ", conShop24Hrs, "MNTF_FindBillRefTransID");
        }
    }
}
