﻿using SupportApiService.Models;
using System.Web.Http;
using static SupportApiService.Models.Methods;
using SupportApiService.Models.Main;
using System.Data;
using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using ApiService.Models.TableModel.Support;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/Main")]
    public class MainController : ApiController
    {
        //ค้นหาเวอร์ชั่น ล่าสุด เพื่อ update  [PDA_VersionLastNew]
        [HttpGet, Route("VersionLastUpdate")]
        public ResponseDefault VersionLastUpdate()
        {
            ResponseDefault GetVersionLast = new ResponseDefault();
            try
            {
                DataTable dt = Methods.SelectDataTable($@" 
                    SELECT	SERVERIP	FROM	SHOP_CONFIGDB WITH (NOLOCK)
                    WHERE	VARIABLENAME = 'SystemVersionSupcSupport' ", Methods.conShop24Hrs);
                GetVersionLast.MESSAGE = dt.Rows[0]["SERVERIP"].ToString();
            }
            catch (Exception ex)
            {
                GetVersionLast.MESSAGE = "VersionLastUpdate : " + ex.ToString();
            }
            return GetVersionLast;
        }

        //บันทึกการเข้าใช้งานเครื่อง [PDA_SaveUseLogin]
        [HttpPost, Route("SaveLog_UserLogin")]
        public ResponseDefault SaveLog_UserLogin([FromBody] UserLoginSaveRequest data)
        {
            string sqlIn = $@"
                INSERT INTO SHOP_COMPDANAMESHOP24HRS
                            (LoginPDAName,LoginBranch,LoginBranchName,
                            LoginVersion,LoginService,
                            LoginId,LoginName,DATEPDA,IP,BranchConfig ) 
                VALUES      ('{data.LoginPDAName}','{data.LoginBranch}','{data.LoginBranchName}',
                            '{data.LoginVersion}','{data.LoginService}',
                            '{data.LoginId}','{data.LoginName}','{data.DeviceDate}','{data.DeviceIp}','SupcSupport'
                            )
            ";
            return InsertSql(sqlIn, conShop24Hrs, "SaveLog_UserLogin");

        }

        // บันทึกการใช้งานเมนูต่าง [PDA_LogUseMenu]
        [HttpPost, Route("SaveLog_AccessMenu")]
        public ResponseDefault SaveLog_AccessMenu([FromBody] AccessMenuSaveRequest data, string MenuName)
        {
            string sqlIn = $@"
                INSERT INTO SHOP_LOGSHOP24HRS
                            (LogBranch,LogBranchName,LogType,
                            LogWhoID,LogWhoIDName,LogRow,LogMachine,LogTmp) 
                VALUES      ('{data.LogBranch}','{data.LogBranchName}','{MenuName}',
                            '{data.LogWhoID}','{data.LogWhoIDName}','{data.LogRow}','{data.LogMachine}','{data.LogTmp}'
                            )
            ";
            return Methods.InsertSql(sqlIn, conShop24Hrs, "SaveLog_AccessMenu");
        }

        [HttpPost, Route("UpdateConfigPrinter")]
        public String UpdateConfigPrinter(string bluetoothaddress, string devicename, string emplid, string emplname)
        {
            string sqlIn = $@"
                  UPDATE [SHOP24HRS].[dbo].[SHOP_BRANCH_PERMISSION]
                  SET [REMARK] = '{bluetoothaddress}'
	                 ,[DATEUPD] = GETDATE()
	                 ,[WHOIDUPD] = '{emplid}'
	                 ,[WHONAMEUPD] = '{emplname}'
                  WHERE [COM_NAME] = '{devicename}'
		                AND [SHOW_ID] = 'A04'
            ";
            ResponseDefault saveSta = InsertSql(sqlIn, conShop24Hrs, "UpdateConfigPrinter");
            if (saveSta.CODE == 200)
            {
                DataTable dt = SelectDataTable($@"  SELECT *
                                                    FROM [SHOP24HRS].[dbo].[SHOP_BRANCH_PERMISSION] WITH (NOLOCK) 
                                                    WHERE [COM_NAME] = '{devicename}'
		                                                  AND [SHOW_ID] = 'A04' ", conShop24Hrs);
                return dt.Rows[0]["REMARK"].ToString();
            }
            else
            {
                return "Error";
            }
        }

        [HttpGet, Route("GetPermission")]
        public GlobalClass<ConfigReturn> GetPermission()
        {
            string sql = $@"
                                SELECT  [COM_NAME] as Comname
                                        ,[DEPT_ID] as Deptid
                                        ,[DIMENSIONS].[DESCRIPTION] as Deptname
                                        ,[SHOW_ID] as Showid
                                        ,[VERSIONUPD] as Versionupd
		                                ,[CHANNEL] as Warehouse
		                                ,[PRICEGROUP] as Pricegroup
		                                ,[REMARK] as Printer
                                FROM    [SHOP24HRS].[dbo].[SHOP_BRANCH_PERMISSION] WITH (NOLOCK)

                                LEFT JOIN [SHOP2013TMP].[dbo].[DIMENSIONS] WITH (NOLOCK)
                                ON [DATAAREAID] = 'SPC'
	                                AND [DIMENSIONCODE] = 0
	                                AND [NUM] = [DEPT_ID]

                                WHERE [SHOW_ID] = 'A04'
            ";
            return SelectDataSend<ConfigReturn>(sql, conShop24Hrs, "GetAllPermission");
        }

        [HttpGet, Route("GetDimension")]
        public GlobalClass<dimensionDetail> GetDimension(string dimension)
        {
            return SelectDataSend<dimensionDetail>($@"DptClass_GetDptDetail_ByDptID '{dimension}' ", Methods.conShop24Hrs, "GetDimension");
        }

        //'การค้นหาใน ตารางหลัก ค่า ConFig ต่างๆ โดยที่ไม่ระบุสาขา [ทุกสาขาใช้เหมือนกัน]
        [HttpGet, Route("A24_CONFIGBRANCH_GenaralDetail_ByTypeID")]
        public GlobalClass<BranchConfigDetailData> A24_CONFIGBRANCH_GenaralDetail_ByTypeID(string typeConfig)
        {
            string strCondition = "";
            if (typeConfig == "13") strCondition = " AND STA = '1' ";

            return SelectDataSend<BranchConfigDetailData>(
                $@" SELECT	[STA],[SHOW_ID],[SHOW_NAME],[SHOW_DESC],[REMARK],[MaxImage]
					FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
					WHERE	TYPE_CONFIG = '{typeConfig}'   {strCondition}
                    ORDER BY SHOW_ID,SHOW_NAME", conShop24Hrs, "A24_CONFIGBRANCH_GenaralDetail_ByTypeID");
        }
    }
}