﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using static SupportApiService.Models.Methods;
using SupportApiService.Models;
using SupportApiService.Models.MNPC;
using System.Data;

namespace AndroidShop24HrsService.Controllers
{
    [RoutePrefix("api/MNPC")]
    public class MNPCController : ApiController
    {
        //รายละเอียดบาร์โค้ดที่ต้องการคืน
        [HttpGet, Route("MNPC_FindBarcode")]
        public GlobalClass<MNPCBarcodeDetailData> MNPC_FindBarcode(string barcode)
        {
            return SelectDataSend<MNPCBarcodeDetailData>($@" PDA_MNPC_FindData '0','{barcode}','' ", conShop24Hrs, "MNPC_FindBarcode");
        }

        // ค้นหา MNPC ที่ไม่ได้อนุมัติ แตกต่างกันที่ตรงตัวนี้ส่งสาขาไปก็จริงแต่ไม่ได้เอาสาขาใส่ใน Where
        [HttpGet, Route("MNPC_FindMNPCStaPrcDoc")]
        public GlobalClass<MNPCHDDetailData> MNPC_FindMNPCStaPrcDoc(string branchID)
        {
            return SelectDataSend<MNPCHDDetailData>($@" PDA_MNPC_FindData '5','','{branchID}' ", conShop24Hrs, "MNPC_FindMNPCStaPrcDoc");
        }

        // ค้นหา MNPC รายละเอียด
        [HttpGet, Route("MNPC_FindMNPCDetail")]
        public GlobalClass<MNPCDTDetailData> MNPC_FindMNPCDetail(string docNO)
        {
            return SelectDataSend<MNPCDTDetailData>($@" PDA_MNPC_FindData '2','{docNO}','' ", conShop24Hrs, "MNPC_FindMNPCDetail");
        }

        // ค้นหา Tag MNPC 
        [HttpGet, Route("MNPC_FindTagByDocno")]
        public GlobalClass<MNPCTagData> MNPC_FindTagByDocno(string docNO)
        {
            return SelectDataSend<MNPCTagData>($@" PDA_MNPC_FindData '3','{docNO}','' ", conShop24Hrs, "MNPC_FindTagByDocno");
        }

        //Insert MNPC HD ...
        [HttpPost, Route("MNPC_SaveHD")]
        public ResponseDefault MNPC_SaveHD([FromBody] MNPCSaveRequest data, string whoID, string whoName)
        {
            ResponseDefault valuesDocno = new ResponseDefault();
            string maxDocno = GetMaxINVOICEIDString("MNPC", "-", "MNPC", "1");
            List<string> sqlIn = new List<string>
            {
                $@"
                INSERT INTO SHOP_MNPC_HD 
                            (DocNo,UserCode,Branch,BranchName,
                            Grand,Discount,Net,Remark,WhoIn,WhoNameIn,TYPEPRODUCT,SPCName,ProgramUse  )
                VALUES      ('{maxDocno}','{whoID}','{data.HD.Branch}','{data.HD.BranchName}',
                            '{data.HD.Grand}','0','0','CN สร้างเอกสาร','{data.HD.UserCode}','{whoName}','{data.HD.TYPEPRODUCT}','{data.HD.SPCName}','AndroidSupport')
                            ",
                $@"
                INSERT INTO SHOP_MNPC_DT 
                            (DocNo,SeqNo,ItemID,Dimid,Barcode,ItemName,StaItem,
                            DeptCode,Qty,UnitID,Factor,QtySum,Price,PriceNet,
                            StatusShop,WhoShop,WhoIn,SPC_SALESPRICETYPE,CNReason,WhoNameShop,WhoNameIN,ProgramUse ) 
                VALUES      ('{maxDocno}','1','{data.DT.ItemID}','{data.DT.Dimid}','{data.DT.Barcode}','{data.DT.ItemName}','1',
                            '{data.DT.DeptCode}','{data.DT.Qty}','{data.DT.UnitID}','{data.DT.Factor}','{data.DT.QtySum}','{data.DT.Price}','{data.DT.PriceNet}',
                            '0','{whoID}','{whoID}','{data.DT.SPC_SALESPRICETYPE}','{data.DT.CNReason}','{whoName}','{whoName}','Android')
                            "
            };

            ResponseDefault saveSta = InsertSql(sqlIn, conShop24Hrs, "MNPCSaveRequest");
            if (saveSta.CODE == 200)
            {
                valuesDocno.MESSAGE = maxDocno;
                return valuesDocno;
            }
            else
            {
                return saveSta;
            }
        }
        //Insert MNPC DT
        [HttpPost, Route("MNPC_SaveDT")]
        public ResponseDefault MNPC_SaveDT([FromBody] MNPCDTSaveRequest data, string docNo, string whoID, string whoName)
        {

            List<string> sqlIn = new List<string>
            {
                $@"
                INSERT INTO SHOP_MNPC_DT 
                            (DocNo,SeqNo,ItemID,Dimid,Barcode,ItemName,StaItem,
                            DeptCode,Qty,UnitID,Factor,QtySum,Price,PriceNet,
                            StatusShop,WhoShop,WhoIn,SPC_SALESPRICETYPE,CNReason,WhoNameShop,WhoNameIN,ProgramUse ) 
                VALUES      ('{docNo}','{data.SeqNo}','{data.ItemID}','{data.Dimid}','{data.Barcode}','{data.ItemName}','1',
                            '{data.DeptCode}','{data.Qty}','{data.UnitID}','{data.Factor}','{data.QtySum}','{data.Price}','{data.PriceNet}',
                            '0','{whoID}','{whoID}','{data.SPC_SALESPRICETYPE}','{data.CNReason}','{whoName}','{whoName}','Android')
                            ",
                $@"
                UPDATE  SHOP_MNPC_HD
                SET     Grand = Grand + '{data.PriceNet}'
                WHERE   DocNo = '{docNo}'
                "
            };

            return InsertSql(sqlIn, conShop24Hrs, "MNPC_SaveDT");
        }

        //ยกเลิกบิล
        [HttpGet, Route("MNPC_Cancle")]
        public ResponseDefault MNPC_Cancle(string docNO, string whoId, string whoName)
        {
            return InsertSql($@"
            UPDATE  SHOP_MNPC_HD
            SET     Remark = Remark + ' [ยกเลิกเอกสารผ่าน PDA]',StaDoc='3',StaPrcDoc = '0',
                    DateUp = convert(varchar,getdate(),23),TimeUp=convert(varchar,getdate(),24),WhoUp = '{whoId}',WhoNameUp = '{whoName}'
            WHERE    DocNo = '{docNO}'
            ", conShop24Hrs, "MNPC_Cancle");
        }

        //แก้ไขหมายเหตุและยืนยันคืน ...
        [HttpPost, Route("MNPC_EditRemark")]
        public ResponseDefault MNPC_EditRemark(string bchID, string docNO, string whoId, string whoName, int NumBox, string rmk)
        {
            double sumDT = 0;
            DataTable dt = SelectDataTable($@" PDA_MNPC_FindData '4','{docNO}','' ", conShop24Hrs);
            if (dt.Rows.Count > 0)
            {
                sumDT = Convert.ToDouble(dt.Rows[0]["PriceNet"].ToString());
            }

            List<string> sqlIn = new List<string>
            {
                $@"
                UPDATE  SHOP_MNPC_HD 
                SET     Remark = Remark + '{rmk}',StaDoc='1',StaPrcDoc = '1',DateUp = convert(varchar,getdate(),23),TimeUp=convert(varchar,getdate(),24),
                        WhoUp = '{whoId}',TagNumber = '{NumBox}',WhoNameUP = '{whoName}',Grand = '{sumDT}'
                WHERE   DocNo = '{docNO}'
                "
            };

            string tagNumber = docNO.Substring(4, 12);
            for (int i = 0; i < NumBox ; i++)
            {
                sqlIn.Add($@"
                INSERT  SHOP_MNPC_TAG
                        (TagNumber,TagMNPC,TagBranch,TagCreateDate,TagCreateTime,TagCreateWho,ProgramUse)
                VALUES  ('{tagNumber + (i + 1).ToString()}','{docNO}','{bchID}',convert(varchar,getdate(),23),convert(varchar,getdate(),24),'{whoId}','Android')
                ");
            }
            return InsertSql(sqlIn, conShop24Hrs, "MNPC_EditRemark");
        }

        //แก้ไขจำนวน
        [HttpGet, Route("MNPC_EditQty")]
        public ResponseDefault MNPC_EditQty(string docNO, int seqNo, double qty, string whoId, string whoName, string reason) // กรณีไม่เปลี่ยนเหตุผล ให้ส่ง 0 มา
        {
            string sReason = "";
            if (reason != "0")
            {
                sReason = $@" ,CNReason = '{reason}' ";
            }
            string sqlUp = $@"
            UPDATE  SHOP_MNPC_DT
            SET     Qty = '{qty}',PriceNet = Price * {qty},QtySum = Factor * {qty},
                    DateUp = convert(varchar,getdate(),23),TimeUp=convert(varchar,getdate(),24),WhoUp = '{whoId}',WhoNameUP = '{whoName}'
                    {sReason}
            WHERE   DOCNO = '{docNO}' AND SeqNo = '{seqNo}'
            ";
            return InsertSql(sqlUp, conShop24Hrs, "MNPC_EditQty");
        }

    }
}