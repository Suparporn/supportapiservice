﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using static SupportApiService.Models.Methods;
using SupportApiService.Models.MNPV;
using SupportApiService.Models;
using System.Data;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/MNPV")]
    public class MNPVController : ApiController
    {

        //'ค้นหาผู้จำหน่าย [MNPD_FindVender]
        [HttpGet, Route("MNPV_FindVender")]
        public GlobalClass<MNPVVenderData> MNPV_FindVender(string textSend) //textSend ไม่ต้องส่งค่ามาถ้าค้นหาทั้งหมด service จะแยกก่รค้นหาจากชื่อหรือรหัส
        {
            int y;
            string acc = "";
            if (textSend.Length > 0)
            {
                try
                { y = Convert.ToInt32(textSend); acc = $@" AND ACCOUNTNUM LIKE '%{textSend}%'  "; }
                catch (Exception)
                { acc = $@" AND NAME LIKE '%{textSend}%'  "; }
            }

            string sql = $@"
                SELECT	ACCOUNTNUM,NAME,NUM,DESCRIPTION 
                FROM	SHOP2013TMP.dbo.VENDTABLE WITH (NOLOCK) 
                        LEFT OUTER JOIN SHOP2013TMP.dbo.DIMENSIONS  ON VENDTABLE.DIMENSION = DIMENSIONS.NUM 
                WHERE	VENDTABLE.DATAAREAID = 'SPC' AND DIMENSIONS.DATAAREAID = 'SPC'	{acc}
                ORDER BY NUM 
            ";
            return SelectDataSend<MNPVVenderData>(sql, conShop24Hrs, "MNPV_FindVender");
        }

        //บันทึกเอกสาร HD-DT [MNPD_SaveHD]...
        [HttpPost, Route("MNPV_BillSaveHD")]
        public ResponseDefault MNPV_BillSaveHD([FromBody] MNPVSaveRequest data)
        {
            string maxDocno = GetMaxINVOICEIDString("MNPV", "-", "MNPV", "1");
            ResponseDefault valuesDocno = new ResponseDefault();

            List<string> sql = new List<string>() {
                    $@"
                    INSERT INTO SHOP_MNPV_HD 
                                (DOCNO,WhoIDIns,WhoNameIns,VenderID,VenderName,
                                NUM,DESCRIPTION,Remark,Machine,DptID)
                    VALUES      ('{maxDocno}','{data.HD.WhoIDIns}','{data.HD.WhoNameIns}','{data.HD.VenderID}','{data.HD.VenderName}',
                                '{data.HD.NUM}','{data.HD.DESCRIPTION}','{data.HD.Remark}','{data.HD.Machine}','{data.HD.DptID}')
                    ",
                    $@"
                    INSERT INTO SHOP_MNPV_DT
                                (DOCNO,LineNum,ITEMID,INVENTDIMID,ITEMBARCODE,SPC_ITEMNAME,
                                QTY,PRICE,UNITID,FACTOR,SPC_SALESPRICETYPE,ResonID,
                                ResonName,WhoIns )
                    VALUES      ('{maxDocno}','{data.DT.LineNum}','{data.DT.ITEMID}','{data.DT.INVENTDIMID}','{data.DT.ITEMBARCODE}','{data.DT.SPC_ITEMNAME}',
                                '{data.DT.QTY}','{data.DT.PRICE}','{data.DT.UNITID}','{data.DT.FACTOR}','{data.DT.SPC_SALESPRICETYPE}','{data.DT.ResonID}',
                                '{data.DT.ResonName}','{data.HD.WhoIDIns}')
                    "
            };

            ResponseDefault saveSta = InsertSql(sql, conShop24Hrs, "MNPV_BillSaveHD");
            if (saveSta.CODE == 200)
            {
                valuesDocno.MESSAGE = maxDocno;
                return valuesDocno;
            }
            else
            {
                return saveSta;
            }
        }

        //บันทึกเอกสาร DT [MNPD_SaveDT]...
        [HttpPost, Route("MNPV_BillSaveDT")]
        public ResponseDefault MNPV_BillSaveDT(string docNo, string whoIns, [FromBody] MNPVDTSaveRequest data)
        {
            List<string> sql = new List<string>() {
                    $@"
                    INSERT INTO SHOP_MNPV_DT
                                (DOCNO,LineNum,ITEMID,INVENTDIMID,ITEMBARCODE,SPC_ITEMNAME,
                                QTY,PRICE,UNITID,FACTOR,SPC_SALESPRICETYPE,ResonID,
                                ResonName,WhoIns )
                    VALUES      ('{docNo}','{data.LineNum}','{data.ITEMID}','{data.INVENTDIMID}','{data.ITEMBARCODE}','{data.SPC_ITEMNAME}',
                                '{data.QTY}','{data.PRICE}','{data.UNITID}','{data.FACTOR}','{data.SPC_SALESPRICETYPE}','{data.ResonID}',
                                '{data.ResonName}','{whoIns}')
                    "
            };
            return InsertSql(sql, conShop24Hrs, "MNPV_BillSaveDT");

        }

        //ยกเลิกเอกสาร  [MNPD_Cancle]...
        [HttpGet, Route("MNPV_BillCancel")]
        public ResponseDefault MNPV_BillCancel(string docNo, string whoIns, string whoNameIns)
        {
            string sql = $@"
                UPDATE  SHOP_MNPV_HD
                SET     StaDoc = '3',DateUpd = convert(varchar,getdate(),25),
                        WhoIDUpd = '{whoIns}',WhoNameUpd = '{whoNameIns}'
                WHERE   DOCNO = '{docNo}'
            ";
            return InsertSql(sql, conShop24Hrs, "MNPV_BillCancel");
        }

        //แก้ไขจำนวน  [MNPD_EditQtyAdd] / [MNPD_EditQty]...
        [HttpGet, Route("MNPV_BillEditQty")]
        public ResponseDefault MNPV_BillEditQty(string docNo, int iRows, double qty, string reasonID, string reasonName, string whoIns, string whoNameIns)
        {
            string SetMNTOStaDt = $@" ,StaDT = '1' "; //ในกรณีที่ ลบจำนวนหรือจำนวนเท่ากับ0 จะต้องปรับฟิลด์นี้ด้วย
            if (qty == 0)
            {
                SetMNTOStaDt = $@" ,StaDT = '0' ";
            }

            string sql = $@"
                UPDATE  SHOP_MNPV_DT
                SET     QTY = '{qty}',
                        DateTimeUpd = convert(varchar,getdate(),25), 
                        ResonID = COALESCE(NULLIF('{reasonID}', ''), ResonID),ResonName = COALESCE(NULLIF('{reasonName}', ''), ResonName),
                        WhoUpd = '{whoIns}'  {SetMNTOStaDt}
                WHERE   DOCNO = '{docNo}' AND LineNum = '{iRows}'
            ";
            return InsertSql(sql, conShop24Hrs, "MNPV_BillEditQty");
        }

        //'บิลทั้งหมดยังไม่อนุมัติ [MNPD_FindMNPDStaPrcDoc]
        [HttpGet, Route("MNPV_FindBillAll")]
        public GlobalClass<MNPVNotApvData> MNPV_FindBillAll(string dptID)
        {
            return SelectDataSend<MNPVNotApvData>($@" SupcSupport_MNPV_FindData '0','{dptID}','' ", conShop24Hrs, "MNPV_FindBillAll");
        }

        //'ค้นหารายละเอียดบิล [MNPD_FindMNPDDetail]
        [HttpGet, Route("MNPV_FindDetail")]
        public GlobalClass<MNPVDetailData> MNPV_FindDetail(string docno)
        {
            return SelectDataSend<MNPVDetailData>($@" SupcSupport_MNPV_FindData '1','','{docno}' ", conShop24Hrs, "MNPV_FindDetail");
        }

        //'ยืนยันเอกสารและส่งข้อมูลเข้า AX [MNPD_BillApv]/[MNPD_SendAX]
        [HttpGet, Route("MNPV_BillApvSendAX")]
        public ResponseDefault MNPV_BillApvSendAX(string docNo, string whoId, string whoInsName, string inventID) 
        {
            List<string> sql24 = new List<string>
            {
                $@"
                UPDATE  SHOP_MNPV_HD
                SET     StaDoc='1',StaPrcDoc = '1',DateUpd = convert(varchar,getdate(),25),
                        WhoIDUpd = '{whoId}',WhoNameUpd = '{whoInsName}'
                WHERE   DocNo  = '{docNo}'
                "
            };

            DataTable data = SelectDataTable($@" SupcSupport_MNPV_FindData '2','','{docNo}' ", conShop24Hrs);
            List<string> sqlAX = new List<string>
            {
                $@"
                INSERT INTO SPC_PURCHTABLE00 
                            (PURCHID,PURCHDATE,PURCHASETYPE,TAXINVOICE,INCLTAX,INVENTSITEID,INVENTLOCATIONID,
                            PURCHPLACER, REMARK, DIMENSION, DATAAREAID, PURCHSTATUS, TRANSDATE,RECID,ORDERACCOUNT) 
                VALUES      ('{docNo}',CONVERT(VARCHAR,GETDATE(),23),'4','0','0','SPC','{inventID}',
                            '{whoId}','ทำคืนสินค้าจากเครื่อง PDA','{data.Rows[0]["DptID"]}','SPC','1',CONVERT(VARCHAR,GETDATE(),23),'1',
                            '{data.Rows[0]["VenderID"]}')
            "
            };

            int iRows = 1;
            foreach (DataRow item in data.Rows)
            {
                double qty = Convert.ToDouble(item["QTY"]) * (-1);
                sqlAX.Add($@"
                        INSERT INTO SPC_PURCHLINE00 
                                    (PURCHID,PURCHDATE,LINENUM,PURCHQTY,
                                    DATAAREAID,RECVERSION,RECID,BARCODE,NAME,
                                    REMARK,PURCHPRICE)
                        VALUES      ('{docNo}',CONVERT(VARCHAR,GETDATE(),23),'{iRows}','{qty}',
                                    'SPC','1','1','{item["ITEMBARCODE"]}','{item["SPC_ITEMNAME"]}',
                                    '{item["ResonName"]}','{Convert.ToDouble(item["PRICE"])}')
                    ");
                iRows++;
            }

            return InsertSqlTwoServer(sql24, conShop24Hrs, sqlAX, con708AX, "MNPV_BillApvSendAX");
        }
    }
}