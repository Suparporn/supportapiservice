﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using static SupportApiService.Models.Methods;
using SupportApiService.Models.MNT;
using System.Data;
using SupportApiService.Models;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/MNT")]
    public class MNTController : ApiController
    {
        //''ค้นหาประเภทลัง'  [MNTOTag_BoxGroup]
        [HttpGet, Route("MNT_BoxGroup")]
        public GlobalClass<MNTBoxGroupData> MNT_BoxGroup()
        {
            return SelectDataSend<MNTBoxGroupData>($@" SupcSupport_MNT '0','','',''  ", conShop24Hrs, "MNT_BoxGroup");
        }

        //ค้นหาลังในบิล [MNTOTag_FindTagBOXFACESHEET]
        [HttpGet, Route("MNT_POSTOBOXFACESHEET")]
        public GlobalClass<MNTPOSTOBOXFACESHEETData> MNT_POSTOBOXFACESHEET(string docNo)
        {
            return SelectDataSend<MNTPOSTOBOXFACESHEETData>($@" SupcSupport_MNT '1','{docNo}','',''  ", conShop24Hrs, "MNT_POSTOBOXFACESHEET");
        }

        // เช็คบิลที่ระบุ พบข้อมูลหรือไม่ [MNTOTag_FindPOSTOTABLE]
        [HttpGet, Route("MNT_POSTOTABLE")]
        public int MNT_POSTOTABLE(string docNo, string invent)
        {
            return SelectDataTable($@" SupcSupport_MNT '2','{docNo}','','{invent}'  ", conShop24Hrs).Rows.Count;
        }

        // ค้นหารายละเอียดบิล [MNTOTag_FindPOSTOLINE]
        [HttpGet, Route("MNT_POSTOLINE")]
        public GlobalClass<MNTPOSTOLINEData> MNT_POSTOLINE(string docNo)
        {
            return SelectDataSend<MNTPOSTOLINEData>($@" SupcSupport_MNT '3','{docNo}','',''  ", conShop24Hrs, "MNT_POSTOLINE");
        }

        // save HD
        //บันทึกเอกสาร HD-DT [MNTOTag_SaveHD]...
        [HttpPost, Route("MNT_BillSaveHD")]
        public ResponseDefault MNT_BillSaveHD([FromBody] MNTSaveRequest data, string dptID)
        {
            string maxDocno = GetMaxINVOICEIDString("MNT", $@"{dptID}", "MNT", "11");
            ResponseDefault valuesDocno = new ResponseDefault();
            string transID = maxDocno + "-1";
            List<string> sql = new List<string>() {
                    $@"
                    INSERT INTO SHOP_POSTOTABLE 
                                (CASHIERID,PICKINGROUTEID,EMPLPICK,EMPLCHECKER,
                                    ROUTESHIPMENTID,INVENTLOCATIONIDTO,
                                    INVENTLOCATIONIDFROM,TRANSFERID,VOUCHERID,STADPT)
                    VALUES      ('{data.HD.CASHIERID}','{data.HD.PICKINGROUTEID}','{data.HD.CASHIERID}','{data.HD.CASHIERID}',
                                '{data.HD.ROUTESHIPMENTID}','{data.HD.INVENTLOCATIONIDTO}',
                                '{data.HD.INVENTLOCATIONIDFROM}','{maxDocno}','{maxDocno}','{data.HD.STADPT}')
                    ",
                    $@"
                    INSERT INTO SHOP_POSTOLINE
                                (VOUCHERID,LINENUM,QTY,SALESPRICE,ITEMID,
                                NAME,SALESUNIT,ITEMBARCODE, 
                                INVENTSERIALID, INVENTDIMID, INVENTTRANSID, REFBOXTRANS,RECVERSION)
                    VALUES      ('{maxDocno}','{data.DT.LINENUM}','{data.DT.QTY}','{data.DT.SALESPRICE}','{data.DT.ITEMID}',
                                '{data.DT.NAME.Replace("'"," ")}','{data.DT.SALESUNIT}','{data.DT.ITEMBARCODE}',
                                '{data.DT.INVENTSERIALID}','{data.DT.INVENTDIMID}','{transID}','{data.DT.REFBOXTRANS}','1')
                    "
                    ,
                    $@"
                     INSERT INTO SHOP_POSTOBOXTRANS
                            (REFBOXTRANS,ITEMBARCODE,LINECAPACITY,LINEWEIGHT,
                            BOXQTY, BOXTYPE, NAME, BOXGROUP, VOUCHERID,RECVERSION)
                     VALUES  ('{data.Trans.REFBOXTRANS}','{data.Trans.ITEMBARCODE}','{data.Trans.LINECAPACITY}','{data.Trans.LINEWEIGHT}',
                            '{data.Trans.BOXQTY}','{data.Trans.BOXTYPE}','{data.Trans.NAME}', '{data.Trans.BOXGROUP}','{maxDocno}','1')
                    "
            };

            ResponseDefault saveSta = InsertSql(sql, conShop24Hrs, "MNT_BillSaveHD");
            if (saveSta.CODE == 200)
            {
                valuesDocno.MESSAGE = maxDocno;
                return valuesDocno;
            }
            else
            {
                return saveSta;
            }
        }

        // save DT ให้เช็ค BoxTrans ด้วย ถ้ามีรายการก็ต้องบันทึก ที่ MNT_BillSaveTrans อีก 1 ที่
        //บันทึกเอกสาร HD-DT [MNTOTag_SaveDT] ...
        [HttpPost, Route("MNT_BillSaveDT")]
        public ResponseDefault MNT_BillSaveDT(string docno, [FromBody] MNTSaveDTRequest data)
        {
            string transID = docno + "-" + data.LINENUM;
            List<string> sql = new List<string>() {
                    $@"
                    INSERT INTO SHOP_POSTOLINE
                                (VOUCHERID,LINENUM,QTY,SALESPRICE,ITEMID,
                                NAME,SALESUNIT,ITEMBARCODE, 
                                INVENTSERIALID, INVENTDIMID, INVENTTRANSID, REFBOXTRANS,RECVERSION)
                    VALUES      ('{docno}','{data.LINENUM}','{data.QTY}','{data.SALESPRICE}','{data.ITEMID}',
                                '{data.NAME.Replace("'"," ")}','{data.SALESUNIT}','{data.ITEMBARCODE}',
                                '{data.INVENTSERIALID}','{data.INVENTDIMID}','{transID}','{data.REFBOXTRANS}','{data.LINENUM+1}')
                    "
            };
            return InsertSql(sql, conShop24Hrs, "MNT_BillSaveDT");
        }

        // save trans
        //บันทึกเอกสาร HD-DT [MNTOTag_SaveDT]...
        [HttpPost, Route("MNT_BillSaveTrans")]
        public ResponseDefault MNT_BillSaveTrans(string docno, [FromBody] MNTSaveTransRequest data)
        {
            List<string> sql = new List<string>() {
                     $@"
                     INSERT INTO SHOP_POSTOBOXTRANS
                            (REFBOXTRANS,ITEMBARCODE,LINECAPACITY,LINEWEIGHT,
                            BOXQTY, BOXTYPE, NAME, BOXGROUP, VOUCHERID,RECVERSION)
                     VALUES  ('{data.REFBOXTRANS}','{data.ITEMBARCODE}','{data.LINECAPACITY}','{data.LINEWEIGHT}',
                            '{data.BOXQTY}','{data.BOXTYPE}','{data.NAME}', '{data.BOXGROUP}','{docno}','{data.LINENUM+1}')
                    "
            };
            return InsertSql(sql, conShop24Hrs, "MNT_BillSaveTrans");

        }

        //แก้ไขจำนวน  [MNTOTag_EditQtyLineBill] / [MNTOTag_DeleteQtyLineBill]..
        [HttpGet, Route("MNT_BillEditQty")]
        public ResponseDefault MNT_BillEditQty(string docNo, int iRows, double qty, string refBoxTrans)
        {
            List<string> sql = new List<string>() {
            $@"
                UPDATE  Shop_POSTOLINE
                SET     QTY = '{qty}' 
                WHERE   VOUCHERID = '{docNo}' AND LINENUM = '{iRows}'
            " };

            if (refBoxTrans.Substring(0, 3) == "999")
            {
                sql.Add($@"
                    DELETE  SHOP_POSTOBOXTRANS
                    WHERE   VOUCHERID = '{docNo}' AND REFBOXTRANS = '{refBoxTrans}'
                ");
            }


            return InsertSql(sql, conShop24Hrs, "MNT_BillEditQty");
        }

        //ยกเลิกเอกสาร  [MNTOTag_CancleBill]...
        [HttpGet, Route("MNT_BillCancel")]
        public ResponseDefault MNT_BillCancel(string docNo, string whoName)
        {
            string sql = $@"
                UPDATE  Shop_POSTOTABLE
                SET     STADOC = '3',REMARKS = 'เอกสารถูกยกเลิก โดย' + '{whoName}'
                WHERE   VOUCHERID = '{docNo}'
            ";
            return InsertSql(sql, conShop24Hrs, "MNT_BillCancel");
        }

        //ยกเลิกเอกสาร  [MNTOTag_SaveTag]...
        [HttpGet, Route("MNT_BillApvSendAX")]
        public ResponseDefault MNT_BillApvSendAX(string docNo, string routeName, string bchID, string bchName)
        {

            DataTable dtTag = SelectDataTable($@" 
                SELECT	*,CONVERT(VARCHAR,TRANSDATE,23) AS SDATE 
                FROM  	Shop_POSTOBOXTRANS   with (nolock)  
                WHERE	VOUCHERID =  '{docNo}'
                ORDER BY RECVERSION
            ", conShop24Hrs);

            DataTable dtSheet = SelectDataTable($@"
                SELECT	*,CONVERT(VARCHAR,SHIPDATE,23) AS SDATE 
                FROM  	Shop_POSTOBOXFACESHEET   with (nolock)  
                WHERE	VOUCHERID =  '{docNo}'
                ", conShop24Hrs);

            if (dtSheet.Rows.Count == 0)
            {
                string[] refA = docNo.Split('-');
                string sRef = refA[0].Replace("MNT", "") + refA[1].Substring(1, 3);
                List<string> sqlUpBox = new List<string>();
                //string sqlUpBox = "";

                for (int i = 0; i < dtTag.Rows.Count; i++)
                {
                    string boxGrp = sRef + (i + 1).ToString();
                    string NUMBERLABEL = (i + 1).ToString() + "/" + dtTag.Rows.Count.ToString();
                    sqlUpBox.Add($@"
                    INSERT INTO  Shop_POSTOBOXFACESHEET 
                                (REFBOXTRANS,BOXGROUP,QTY,
                                NUMBERLABEL,ROUTENAME,BOXNAME,
                                BOXTYPE,LOCATIONNAMETO,INVENTLOCATIONIDTO,
                                VOUCHERID,RECVERSION )
                    VALUES      ('{dtTag.Rows[i]["REFBOXTRANS"]}','{boxGrp}','{dtTag.Rows[i]["BOXQTY"]}',
                                '{NUMBERLABEL}','{routeName}','{dtTag.Rows[i]["NAME"]}',
                                '{dtTag.Rows[i]["BOXTYPE"]}','{bchName}','{bchID}',
                                '{docNo}','{i + 1}')
                ");
                }

                ResponseDefault saveSta = InsertSql(sqlUpBox, conShop24Hrs, "MNT_BillApvSendAX/Shop_POSTOBOXFACESHEET");

                if (saveSta.CODE != 200)
                {
                    return saveSta;
                }
                else
                {
                    dtSheet = SelectDataTable($@"
                        SELECT	*,CONVERT(VARCHAR,SHIPDATE,23) AS SDATE 
                        FROM  	Shop_POSTOBOXFACESHEET   with (nolock)  
                        WHERE	VOUCHERID =  '{docNo}'
                        ", conShop24Hrs);
                }
            }


            DataTable dtTable = SelectDataTable($@" SELECT	*,CONVERT(VARCHAR,SHIPDATE,23) as SDATE 
                FROM  	Shop_POSTOTABLE   WITH (NOLOCK)   
                where	VOUCHERID = '{docNo}' ", conShop24Hrs);

            DataTable dtLine = SelectDataTable($@"SELECT	*
                FROM  	Shop_POSTOLINE   WITH (NOLOCK)   
                where	VOUCHERID = '{docNo}'  AND QTY > 0  ", conShop24Hrs);

            List<String> sql24 = new List<string>
            {
                $@"
                    UPDATE  Shop_POSTOTABLE 
                    SET     Staax = 1
                    WHERE   VOUCHERID = '{docNo}'
                "
            };


            List<string> sqlAX = new List<string> {
                $@"
                INSERT INTO     SPC_POSTOTABLE18 
                                (TRANSFERXLSID,FREIGHTSLIPTYPE, FREIGHTZONEID,
                                REMARKS,CASHIERID,POSGROUP,
                                POSNUMBER,PICKINGROUTEID,ALLPRINTSTICKER,
                                EMPLPICK,EMPLCHECKER,ROUTESHIPMENTID,
                                DLVTERM,DLVMODE,INVENTLOCATIONIDTO,
                                INVENTLOCATIONIDFROM,TRANSFERID,SHIPDATE, 
                                VOUCHERID,DATAAREAID,RECVERSION,
                                RECID,TRANSFERSTATUS )
                VALUES          ('{dtTable.Rows[0]["TRANSFERXLSID"]}','{dtTable.Rows[0]["FREIGHTSLIPTYPE"]}','{dtTable.Rows[0]["FREIGHTZONEID"]}',
                                '{dtTable.Rows[0]["REMARKS"]}','{dtTable.Rows[0]["CASHIERID"]}','{dtTable.Rows[0]["POSGROUP"]}',
                                '{dtTable.Rows[0]["POSNUMBER"]}','{dtTable.Rows[0]["PICKINGROUTEID"]}','{dtTable.Rows[0]["ALLPRINTSTICKER"]}',
                                '{dtTable.Rows[0]["EMPLPICK"]}','{dtTable.Rows[0]["EMPLCHECKER"]}','{dtTable.Rows[0]["ROUTESHIPMENTID"]}',
                                '{dtTable.Rows[0]["DLVTERM"]}','{dtTable.Rows[0]["DLVMODE"]}','{dtTable.Rows[0]["INVENTLOCATIONIDTO"]}',
                                '{dtTable.Rows[0]["INVENTLOCATIONIDFROM"]}','{dtTable.Rows[0]["TRANSFERID"]}','{dtTable.Rows[0]["SDATE"]}',
                                '{dtTable.Rows[0]["VOUCHERID"]}','{dtTable.Rows[0]["DATAAREAID"]}','{dtTable.Rows[0]["RECVERSION"]}',
                                '{dtTable.Rows[0]["RECID"]}','1')
                "
            };

            for (int iL = 0; iL < dtLine.Rows.Count; iL++)
            {
                sqlAX.Add($@"
                INSERT INTO     SPC_POSTOLINE18
                                (VOUCHERID,SHIPDATE,LINENUM,QTY,
                                SALESPRICE,ITEMID,NAME,
                                SALESUNIT,ITEMBARCODE,INVENTSERIALID,
                                INVENTDIMID,INVENTTRANSID,REFBOXTRANS,
                                DATAAREAID,RECVERSION,RECID,INVENTBATCHID ) 
                VALUES          ('{dtLine.Rows[iL]["VOUCHERID"]}','{dtTable.Rows[0]["SDATE"]}','{iL + 1}','{dtLine.Rows[iL]["QTY"]}',
                                '{dtLine.Rows[iL]["SALESPRICE"]}','{dtLine.Rows[iL]["ITEMID"]}','{dtLine.Rows[iL]["NAME"]}',
                                '{dtLine.Rows[iL]["SALESUNIT"]}','{dtLine.Rows[iL]["ITEMBARCODE"]}','{dtLine.Rows[iL]["INVENTSERIALID"]}',
                                '{dtLine.Rows[iL]["INVENTDIMID"]}','{dtLine.Rows[iL]["INVENTTRANSID"]}','{dtLine.Rows[iL]["REFBOXTRANS"]}',
                                'SPC','{dtLine.Rows[iL]["RECVERSION"]}','{dtLine.Rows[iL]["RECID"]}','{dtLine.Rows[iL]["INVENTBATCHID"]}'
                                )
                ");
            }

            for (int iT = 0; iT < dtTag.Rows.Count; iT++)
            {
                sqlAX.Add($@"
                INSERT INTO     SPC_POSTOBOXTRANS18
                                (REFPALLETTRANS,REFBOXTRANS,ITEMBARCODE,
                                LINECAPACITY,LINEWEIGHT,TRANSDATE,
                                BOXQTY,BOXTYPE,NAME,
                                BOXGROUP,VOUCHERID,DATAAREAID,
                                RECVERSION,RECID ) 
                VALUES          ('{dtTag.Rows[iT]["REFPALLETTRANS"]}','{dtTag.Rows[iT]["REFBOXTRANS"]}','{dtTag.Rows[iT]["ITEMBARCODE"]}',
                                '{dtTag.Rows[iT]["LINECAPACITY"]}','{dtTag.Rows[iT]["LINEWEIGHT"]}','{dtTable.Rows[0]["SDATE"]}',
                                '{dtTag.Rows[iT]["BOXQTY"]}','{dtTag.Rows[iT]["BOXTYPE"]}','{dtTag.Rows[iT]["NAME"]}',
                                '{dtTag.Rows[iT]["BOXGROUP"]}','{dtTag.Rows[iT]["VOUCHERID"]}','SPC',
                                '{dtTag.Rows[iT]["RECVERSION"]}','{dtTag.Rows[iT]["RECID"]}'
                                )
                ");
            }

            for (int iB = 0; iB < dtSheet.Rows.Count; iB++)
            {
                sqlAX.Add($@"
                INSERT INTO     SPC_POSTOBOXFACESHEET18
                                (SHIPPINGDATEREQUESTED,REFBOXTRANS,BOXGROUP,
                                QTY,NUMBERLABEL,ROUTENAME,
                                DLVNAME,BOXNAME,BOXTYPE,
                                LOCATIONNAMETO,INVENTLOCATIONIDTO,SHIPDATE,
                                VOUCHERID,DATAAREAID,RECVERSION,RECID
                                ) 
                VALUES          ('{dtTable.Rows[0]["SDATE"]}','{dtSheet.Rows[iB]["REFBOXTRANS"]}','{dtSheet.Rows[iB]["BOXGROUP"]}',      
                                '{dtSheet.Rows[iB]["QTY"]}','{dtSheet.Rows[iB]["NUMBERLABEL"]}','{dtSheet.Rows[iB]["ROUTENAME"]}',
                                '{dtSheet.Rows[iB]["DLVNAME"]}','{dtSheet.Rows[iB]["BOXNAME"]}','{dtSheet.Rows[iB]["BOXTYPE"]}',
                                '{dtSheet.Rows[iB]["LOCATIONNAMETO"]}','{dtSheet.Rows[iB]["INVENTLOCATIONIDTO"]}','{dtTable.Rows[0]["SDATE"]}',
                                '{dtSheet.Rows[iB]["VOUCHERID"]}','SPC','{dtSheet.Rows[iB]["RECVERSION"]}','{dtSheet.Rows[iB]["RECID"]}'
                                )
                ");
            }

            return InsertSqlTwoServer(sql24, conShop24Hrs, sqlAX, con708AX, "MNT_BillApvSendAX");
        }

        //'บิลค้างปล่อยทั้งหมด [Price_FindByType]
        [HttpGet, Route("MNT_Price")]
        public GlobalClass<MNTPRICEData> MNT_Price(string typeBill)
        {
            return SelectDataSend<MNTPRICEData>($@" SupcSupport_MNTF_FindData '4','{typeBill}','','','','' ", conShop24Hrs, "MNT_Price");
        }
    }
}