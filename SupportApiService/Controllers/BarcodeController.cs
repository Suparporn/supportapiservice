﻿using SupportApiService.Models;
using System.Web.Http;
using static SupportApiService.Models.Methods;
using SupportApiService.Models.Barcode;
using SupportApiService.Models.General;
using System.Data;
using System;
using System.Collections.Generic;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/Barcode")]
    public class BarcodeController : ApiController
    {
        //ค้นหาบาร์โค้ด เฉพาะรายละเอียดใน itembarcode ตามระดับราคา [Barcode_FindRecodeByBarcode]
        [HttpGet, Route("BarcodeDetail_ByPriceLevel")]
        public GlobalClass<BarcodeDetailData> BarcodeDetail_ByPriceLevel(string barcode, string priceLevel)
        {
            return SelectDataSend<BarcodeDetailData>(GeneralClass.Barcode_Detail(barcode, priceLevel), Methods.conShop24Hrs, "Barcode_FindDetail");
        }

        //ค้นหาบาร์โค้ดทั้งหมด ตามชุดมิติสินค้า [Barcode_FindAllDim]
        [HttpGet, Route("BarcodeDetailAllDim_ByPriceLevel")]
        public GlobalClass<BarcodeAllDimData> BarcodeDetailAllDim_ByPriceLevel(string productId, string productDim,string priceLevel)
        {
            return SelectDataSend<BarcodeAllDimData>(GeneralClass.BarcodeAllDim_ByPriceLevel(productId, productDim, priceLevel), conShop24Hrs, "BarcodeDetailAllDim_ByPriceLevel");
        }

        //ค้นหาสต็อกทั้งหมด [Stock_FindStockByBarcode]
        [HttpGet, Route("BarcodeFindStock_ByBarcode")]
        public double BarcodeFindStock_ByBarcode(string barcode, string inventId)
        {
            DataTable dtDim = Methods.SelectDataTable($@"[ItembarcodeClass_FindDIM_ByBarcode] '{barcode}'", conShop24Hrs);
            double stock = 0;
            if (dtDim.Rows.Count == 0)
            {
                return stock;
            }
            try
            {
                DataTable dt = Methods.SelectDataTable($@"
                                                    [ItembarcodeClass_FindStockByDIM]
		                                            @pBchID = N'{inventId}',
		                                            @ITEMID = N'{dtDim.Rows[0]["ITEMID"]}',
		                                            @CONFIGID = N'{dtDim.Rows[0]["CONFIGID"] }',
		                                            @INVENTSIZEID = N'{dtDim.Rows[0]["INVENTSIZEID"] }',
		                                            @INVENTCOLORID = N'{dtDim.Rows[0]["INVENTCOLORID"]}',
		                                            @BatchNumGroupId = N'{dtDim.Rows[0]["BatchNumGroupId"]}'", Methods.conShop24Hrs);
                if (dt.Rows.Count > 0)
                {
                    stock = double.Parse(dt.Rows[0]["AVAILPHYSICAL"].ToString()) / double.Parse(dtDim.Rows[0]["QTY"].ToString());
                }
            }
            catch (Exception) { }
            return stock;
        }

        //บันทึกการเปลี่ยนป้าย [CheckPriceD104_InsertChangeSign]
        [HttpPost, Route("BarcodePriceChangeInsert")]
        public ResponseDefault BarcodePriceChangeInsert([FromBody] BarcodeInsertPriceChangeRequest data)
        {
            string sqlIn = $@"
                INSERT INTO [SHOP_CHECKPRICEFOREXPORT] (
                            CheckWho,CheckWhoName,
                            ITEMBARCODE, SPC_ITEMNAME,CheckPrice,UNITID,
                            DptID,DptName,
                            CheckPDA,CheckDepart,CheckDepartName ) 
                VALUES ('{data.CheckWho}','{data.CheckWhoName}',
                            '{data.ITEMBARCODE}','{data.SPC_ITEMNAME}','{data.CheckPrice}','{data.UNITID}',
                            '{data.DptID}','{data.DptName}',
                            '{data.CheckPDA}','{data.CheckDepart}','{data.CheckDepartName}'
                        )
            ";
            return InsertSql(sqlIn, conShop24Hrs, "BarcodeInsertPriceChange");
        }

        //บันทึการพิมพ์บาร์โค้ด [CheckPriceD104_InsertPrtBarcode]
        [HttpPost, Route("BarcodePrintDateInsert")]
        public ResponseDefault BarcodePrintDateInsert([FromBody] BarcodePrintDateSaveHeadRequest data)
        {
            List<string> sql = new List<string>();
            foreach (var item in data.DATA)
            {
                sql.Add($@"
                    INSERT INTO SHOP_ITEMPRTBARCODE (
                                Branch,ItemBarcode,ItemName,ItemPrice,ItemUnit,WhoIns ) 
                    VALUES      ('{data.InventID}','{item.ITEMBARCODE}','{item.ITEMNAME}','{item.ItemPrice}','{item.ItemUnit}','{data.EmpID}')
                ");
            }
            return InsertSql(sql, conShop24Hrs, "BarcodeInsertPrintDate");
        }

        //ค้นหาบาร์โค้ด จากชื่อสินค้า
        [HttpGet, Route("Barcode_FindByName")]
        public GlobalClass<BarcodeName> Barcode_FindByName(string pName)
        {
            return SelectDataSend<BarcodeName>(
                $@" 
                SELECT  INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMNAME AS SPC_ITEMNAME
                FROM SHOP2013TMP.dbo.INVENTITEMBARCODE with (nolock)  
                WHERE INVENTITEMBARCODE.SPC_ITEMACTIVE = '1'  
                  AND INVENTITEMBARCODE.DATAAREAID = 'SPC' 
                  AND SPC_ITEMNAME LIKE '%{pName}%'
                ORDER BY SPC_ITEMNAME", Methods.conShop24Hrs, "Barcode_FindByName");

        }

        //ค้นหาสินค้าในมิติเดียวกัน โดยออกราคาด้วย 
        [HttpGet, Route("Barcode_FindPriceAllDim")]
        public GlobalClass<BarcodeAllDimData> Barcode_FindPriceAllDim(string productId, string productDim)
        {
            return SelectDataSend<BarcodeAllDimData>($@" 
                SELECT QTY,UNITID,SPC_PriceGroup1,SPC_PRICEGROUP3,SPC_PRICEGROUP4,SPC_PRICEGROUP13,SPC_PRICEGROUP14,SPC_PRICEGROUP15,SPC_PriceGroup21,SPC_PRICEGROUP17
                FROM SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK)
                WHERE INVENTITEMBARCODE.DATAAREAID = N'SPC' AND SPC_ITEMACTIVE = '1'
                  AND INVENTITEMBARCODE.ITEMID = '{productId}'  
                  AND INVENTITEMBARCODE.INVENTDIMID = '{productDim}'
                GROUP BY QTY,UNITID,SPC_PriceGroup1,SPC_PRICEGROUP3,SPC_PRICEGROUP4,SPC_PRICEGROUP13,SPC_PRICEGROUP14,SPC_PRICEGROUP15,SPC_PriceGroup21 ,SPC_PRICEGROUP17
                ORDER BY QTY,SPC_PRICEGROUP3
            ", Methods.conShop24Hrs, "Barcode_FindPriceAllDim");

        }
    }
}
