﻿using SupportApiService.Models;
using SupportApiService.Models.MNTO;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;
using static SupportApiService.Models.Methods;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/MNTO")]
    public class MNTOController : ApiController
    {
        //'บิลค้างปล่อยทั้งหมด [MNTO_FindMNTOStaPrcDocAllDpt]
        [HttpGet, Route("MNTO_FindBillNotApv")]
        public GlobalClass<MNTONotApvData> MNTO_FindBillNotApv(string dptID)
        {
            return SelectDataSend<MNTONotApvData>($@" SupcSupport_MNTO '2','{dptID}' ", conShop24Hrs, "MNTO_FindBillNotApv");
        }

        //'ค้นหารายละเอียดบิล [MNTO_FindMNTODetail]
        [HttpGet, Route("MNTO_FindDetail")]
        public GlobalClass<MNTODetailData> MNTO_FindDetail(string docno)
        {
            return SelectDataSend<MNTODetailData>($@" SupcSupport_MNTO '1','{docno}' ", conShop24Hrs, "MNTO_FindDetail");
        }

        //บันทึกเอกสาร HD-DT [MNTO_SaveHD]
        [HttpPost, Route("MNTO_BillSaveHD")]
        public ResponseDefault MNTO_BillSaveHD([FromBody] MNTOSaveRequest data)
        {
            string maxDocno = GetMaxINVOICEIDString("MNTO", "-", "MNTO", "1");
            ResponseDefault valuesDocno = new ResponseDefault();

            List<string> sql = new List<string>() {
                    $@"
                    INSERT INTO SHOP_MNTO_HD 
                                (MNTODocNo,MNTOUserCode,MNTOInventIN,MNTOInventOUT,
                                MNTOSPC,MNTOWhoIn,MNTOInventINName,MNTOWhoDpt)
                    VALUES      ('{maxDocno}','{data.HD.MNTOUserCode}','{data.HD.MNTOInventIN}','{data.HD.MNTOInventOUT}',
                                '{data.HD.MNTOSPC}','{data.HD.MNTOWhoIn}','{data.HD.MNTOInventINName}','{data.HD.MNTOWhoDpt}')
                    ",
                    $@"
                    INSERT INTO SHOP_MNTO_DT
                                (MNTODocNo,MNTOSeqNo,MNTOItemID,MNTODimid,MNTOBarcode,MNTOName,
                                MNTOInvent,MNTOQtyOrder,MNTOUnitID,MNTOFactor,
                                MNTOWhoIn,MNTOUser,INVENTSUB )
                    VALUES      ('{maxDocno}','{data.DT.MNTOSeqNo}','{data.DT.MNTOItemID}','{data.DT.MNTODimid}','{data.DT.MNTOBarcode}','{data.DT.MNTOName.Replace("'"," ")}',
                                '{data.DT.MNTOInvent}','{data.DT.MNTOQtyOrder}','{data.DT.MNTOUnitID}','{data.DT.MNTOFactor}',
                                '{data.HD.MNTOWhoIn}','{data.DT.MNTOUser}','{data.DT.INVENTSUB}')
                    "
            };
            ResponseDefault saveSta = InsertSql(sql, conShop24Hrs, "MNTO_BillSaveHD");
            if (saveSta.CODE == 200)
            {
                valuesDocno.MESSAGE = maxDocno;
                return valuesDocno;
            }
            else
            {
                return saveSta;
            }
        }

        //บันทึกเอกสาร HD-DT [MNTO_SaveDT]
        [HttpPost, Route("MNTO_BillSaveDT")]
        public ResponseDefault MNTO_BillSaveDT(string docNo, string whoIns, [FromBody] MNTOSaveDTRequest data)
        {
            List<string> sql = new List<string>() {
                    $@"
                    INSERT INTO SHOP_MNTO_DT
                                (MNTODocNo,MNTOSeqNo,MNTOItemID,MNTODimid,MNTOBarcode,MNTOName,
                                MNTOInvent,MNTOQtyOrder,MNTOUnitID,MNTOFactor,
                                MNTOWhoIn,MNTOUser,INVENTSUB )
                    VALUES      ('{docNo}','{data.MNTOSeqNo}','{data.MNTOItemID}','{data.MNTODimid}','{data.MNTOBarcode}','{data.MNTOName.Replace("'"," ")}',
                                '{data.MNTOInvent}','{data.MNTOQtyOrder}','{data.MNTOUnitID}','{data.MNTOFactor}',
                                '{whoIns}','{data.MNTOUser}','{data.INVENTSUB}')
                    "
            };
            return InsertSql(sql, conShop24Hrs, "MNTO_BillSaveDT");

        }

        //แก้ไขจำนวน  [MNTO_EditQty] 
        [HttpGet, Route("MNTO_BillEditQty")]
        public ResponseDefault MNTO_BillEditQty(string docNo, int iRows, double qty, string whoIns)
        {
            string SetMNTOStaDt = $@" ,MNTOStaDt = '1' "; //ในกรณีที่ ลบจำนวนหรือจำนวนเท่ากับ0 จะต้องปรับฟิลด์นี้ด้วย
            if (qty == 0)
            {
                SetMNTOStaDt = $@" ,MNTOStaDt = '0' ";
            }

            string sql = $@"
                UPDATE  SHOP_MNTO_DT
                SET     MNTOQtyOrder = '{qty}',
                        MNTODateUp = convert(varchar,getdate(),25),MNTOTimeUp = convert(varchar,getdate(),24),
                        MNTOWhoUp = '{whoIns}'  {SetMNTOStaDt}
                WHERE   MNTODocNo = '{docNo}' AND MNTOSeqNo = '{iRows}'
            ";
            return InsertSql(sql, conShop24Hrs, "MNTO_BillEditQty");
        }

        //ยกเลิกเอกสาร  [MNTO_Cancle]
        [HttpGet, Route("MNTO_BillCancel")]
        public ResponseDefault MNTO_BillCancel(string docNo, string whoIns)
        {
            string sql = $@"
                UPDATE  SHOP_MNTO_HD
                SET     MNTOStaDoc = '3',MNTODateUp = convert(varchar,getdate(),25),MNTOTimeUp = convert(varchar,getdate(),24),
                        MNTOWhoUp = '{whoIns}'
                WHERE   MNTODocNo = '{docNo}'
            ";
            return InsertSql(sql, conShop24Hrs, "MNTO_BillCancel");
        }


        //'ยืนยันเอกสารและส่งข้อมูลเข้า AX [MNTO_SendAXByDOCNO]/[MNTO_BillApvByDocno] ...
        [HttpGet, Route("MNTO_BillApvSendAX")]
        public ResponseDefault MNTO_BillApvSendAX(string docNo, string whoId)
        {
            string typeUpdateAX = "1"; //typeUpdateAX 0 = สร้างแล้ว 1 = จัดส่ง 2 = รับ
            List<string> sql24 = new List<string>
            {
                $@"
                UPDATE  SHOP_MNTO_HD
                SET     MNTOStaPrcDoc = '1',MNTODateUp = convert(varchar,getdate(),25),MNTOTimeUp=convert(varchar,getdate(),24),
                        MNTOWhoUp = '{whoId}'
                WHERE   MNTODocNo  = '{docNo}'
                "
            };

            DataTable data = SelectDataTable($@" SupcSupport_MNTO '0','{docNo}' ", conShop24Hrs);
            List<string> sqlAX = new List<string>
            {
                $@"
                INSERT INTO SPC_POSTOTABLE20 
                            (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                            INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, 
                            SHIPDATE, VOUCHERID,REMARKS) 
                VALUES      ('0','{data.Rows[0]["WHOINS"]}','0','SPC','1','1','{typeUpdateAX}',
                            '{data.Rows[0]["MNTOInventIN"]}','{data.Rows[0]["MNTOInventOUT"]}','{docNo}',
                            '{data.Rows[0]["DATEINS"]}','{docNo}','ส่งสินค้าจาก CN')
            "
            };

            int iRows = 1;
            foreach (DataRow item in data.Rows)
            {

                sqlAX.Add($@"
                        INSERT INTO SPC_POSTOLINE20 
                                    (RECID,REFBOXTRANS,DATAAREAID,VOUCHERID,SHIPDATE,LINENUM,QTY,
                                    ITEMID,NAME,SALESUNIT,ITEMBARCODE,
                                    INVENTDIMID,INVENTTRANSID,RECVERSION)
                        VALUES      ('1','0','SPC','{docNo}','{item["DATEINS"]}','{iRows}','{item["QTY"]}',
                                    '{item["ITEMID"]}','{item["SPC_ITEMNAME"]}','{item["UNITID"]}','{item["ITEMBARCODE"]}',
                                    '{item["INVENTDIM"]}','{docNo + "-" + item["LINENUM"]}','1')
                    ");
                iRows++;

            }

            return InsertSqlTwoServer(sql24, conShop24Hrs, sqlAX, con708AX, "MNTO_BillApvSendAX");
        }

    }
}