﻿using System.Web.Http;
using SupportApiService.Models.Invent;
using static SupportApiService.Models.Methods;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/Invent")]
    public class InventController : ApiController
    {
        //' ค้นหาคลังสินค้าทั้งหมด [Invent_FindAll]
        [HttpGet, Route("Invent_FindAll")]
        public GlobalClass<InventData> Invent_FindAll()
        {
            return SelectDataSend<InventData>($@"  SupcSupport_Invent '0','' ", conShop24Hrs, "Invent_FindAll");
        }

        //' ค้นหาคลังสินค้า ตามคลัง [Invent_FindByID]
        [HttpGet, Route("Invent_FindDetail")]
        public GlobalClass<InventData> Invent_FindDetail(string inventID)
        {
            return SelectDataSend<InventData>($@" SupcSupport_Invent '1','{inventID}' ", conShop24Hrs, "Invent_FindDetail");
        }

        //' ค้นหาเงื่อนไขการขนส่ง หรือว่าล็อกที่ต้องการสินค้า ทั้งหมด [Invent_FindDLVTERM]
        [HttpGet, Route("Invent_FindDLVTERM")]
        public GlobalClass<InventDLVTERMData> Invent_FindDLVTERM()
        {
            return SelectDataSend<InventDLVTERMData>($@"  SupcSupport_Invent '2','' ", conShop24Hrs, "Invent_FindDLVTERM");
        }

        //''ค้นหาเงื่อนไขการขนส่ง หรือว่าล็อกที่ต้องการสินค้า //ตามชื่อของแผนก [Invent_FindDLVTERMCase]
        [HttpGet, Route("Invent_FindDLVTERMID")]
        public GlobalClass<InventDLVTERMData> Invent_FindDLVTERMID(string dptID)
        {
            return SelectDataSend<InventDLVTERMData>($@" SupcSupport_Invent '3','%{dptID}%'  ", conShop24Hrs, "Invent_FindDLVTERMID");
        }

        //''ค้นหาแผนกทั้งหมดที่เป็น SPC ไม่ใช่ MN [MNRL_FindDpt]
        [HttpGet, Route("Invent_DptAllSPC")]
        public GlobalClass<DptData> Invent_DptAllSPC(string dptID)
        {
            return SelectDataSend<DptData>($@" SupcSupport_Invent '6','%{dptID}%'  ", conShop24Hrs, "Invent_DptAllSPC");
        }

        //'ค้นหาสายรถตามแต่ละสาขา  [Branch_ForShip]
        [HttpGet, Route("Branch_ForShip")]
        public GlobalClass<BranchRouteData> Branch_ForShip()
        {
            return SelectDataSend<BranchRouteData>($@" BranchClass_All '1'  ", conShop24Hrs, "Branch_ForShip");
        }

        [HttpGet, Route("Branch_FindAll")]
        public GlobalClass<BranchData> Branch_FindAll()
        {
            return SelectDataSend<BranchData>($@" BranchClass_All '0'  ", conShop24Hrs, "Branch_FindAll");
        }
    }
}