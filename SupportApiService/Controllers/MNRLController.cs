﻿using System.Collections.Generic;
using System.Web.Http;
using SupportApiService.Models;
using SupportApiService.Models.MNRL;
using static SupportApiService.Models.Methods;

namespace SupportApiService.Controllers
{
    [RoutePrefix("api/MNRL")]
    public class MNRLController : ApiController
    {

        //'บิลค้างยืนยัน [MNRL_FindMNTOStaPrcDocAllDpt]
        [HttpGet, Route("MNRL_FindBillNotApv")]
        public GlobalClass<MNRLNotApvData> MNRL_FindBillNotApv(string dptID)
        {
            return SelectDataSend<MNRLNotApvData>($@" SupcSupport_MNRL '0','{dptID}' ", conShop24Hrs, "MNRL_FindBillNotApv");
        }

        //'ค้นหารายละเอียดบิล [MNRL_FindDetail]
        [HttpGet, Route("MNRL_FindDetail")]
        public GlobalClass<MNRLDetailData> MNRL_FindDetail(string docno)
        {
            return SelectDataSend<MNRLDetailData>($@" SupcSupport_MNRL '1','{docno}' ", conShop24Hrs, "MNRL_FindDetail");
        }

        //บันทึกเอกสาร HD-DT [MNRL_SaveHD]
        [HttpPost, Route("MNRL_BillSaveHD")]
        public ResponseDefault MNRL_BillSaveHD([FromBody] MNRLSaveRequest data)
        {
            string maxDocno = GetMaxINVOICEIDString("MNRL", "-", "MNRL", "1");
            ResponseDefault valuesDocno = new ResponseDefault();

            List<string> sql = new List<string>() {
                    $@"
                    INSERT INTO SHOP_MNRL_HD 
                                (DocNo,DeptID,DeptName,UserID,UserNAME)
                    VALUES      ('{maxDocno}','{data.HD.DeptID}','{data.HD.DeptName}','{data.HD.UserID}','{data.HD.UserNAME}')
                    ",
                    $@"
                    INSERT INTO SHOP_MNRL_DT
                                (DocNo,SeqNo,ItemName,Qty,WHOIN,REMARK)
                    VALUES      ('{maxDocno}','{data.DT.SeqNo}','{data.DT.ItemName}','{data.DT.Qty}','{data.HD.UserID}','{data.DT.REMARK}')
                    "
            };

            ResponseDefault saveSta = InsertSql(sql, conShop24Hrs, "MNRL_BillSaveHD");
            if (saveSta.CODE == 200)
            {
                valuesDocno.MESSAGE = maxDocno;
                return valuesDocno;
            }
            else
            {
                return saveSta;
            }
        }

        //บันทึกเอกสาร DT [MNRL_SaveDT]
        [HttpPost, Route("MNRL_BillSaveDT")]
        public ResponseDefault MNRL_BillSaveDT(string docNo, string whoIns, [FromBody] MNRLSaveDTRequest data)
        {
            List<string> sql = new List<string>() {
            $@"
                    INSERT INTO SHOP_MNRL_DT
                                (DocNo,SeqNo,ItemName,Qty,WHOIN,REMARK)
                    VALUES      ('{docNo}','{data.SeqNo}','{data.ItemName}','{data.Qty}','{whoIns}','{data.REMARK}')
            "
            };
            return InsertSql(sql, conShop24Hrs, "MNRL_BillSaveDT");
        }

        //แก้ไขจำนวน  [MNRL_EditQty] 
        [HttpGet, Route("MNRL_BillEditQty")]
        public ResponseDefault MNRL_BillEditQty(string docNo, int iRows, double qty, string whoIns)
        {
            string REMARK = $@""; //ในกรณีที่ ลบจำนวนหรือจำนวนเท่ากับ0 จะต้องปรับฟิลด์นี้ด้วย
            if (qty == 0)
            {
                REMARK = $@" ,REMARK = 'ลบรายการโดย  {whoIns} '";
            }

            string sql = $@"
                UPDATE  SHOP_MNRL_DT
                SET     Qty = '{qty}' {REMARK}
                WHERE   DocNo = '{docNo}' AND SeqNo = '{iRows}'
            ";

            return InsertSql(sql, conShop24Hrs, "MNRL_BillEditQty");
        }

        //ยกเลิกเอกสาร  [MNRL_Cancle]
        [HttpGet, Route("MNRL_BillCancel")]
        public ResponseDefault MNRL_BillCancel(string docNo, string whoIns)
        {
            string sql = $@"
                UPDATE  SHOP_MNRL_HD
                SET     StaDoc = '3',DateUp = convert(varchar,getdate(),25),
                        WhoUp = '{whoIns}'
                WHERE   DocNo = '{docNo}'
            ";
            return InsertSql(sql, conShop24Hrs, "MNRL_BillCancel");
        }

        //ยืนยันเอกสาร  [MNRL_BillApv]
        [HttpGet, Route("MNRL_BillApv")]
        public ResponseDefault MNRL_BillApv(string docNo, string whoIns)
        {
            string sql = $@"
                UPDATE  SHOP_MNRL_HD
                SET     StaDoc='1',StaPrcDoc = '1',DateUp = convert(varchar,getdate(),25),
                        WhoUp = '{whoIns}'
                WHERE   DocNo = '{docNo}'
            ";
            return InsertSql(sql, conShop24Hrs, "MNRL_BillApv");
        }
   
    }
}